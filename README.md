# README #

HaxBall
=======

Academic version of popular game HaxBall (http://www.haxball.com/)

How do I run it up?
-------------------

* Requirements
    * Node.js version >= 4.2.4 (http://nodejs.org)
    
* Setup
    * Download local libraries:

    `npm install`

    * Install gulp globally:

    `npm install -g gulp`

    
* Build
    * Run `gulp build` to build project
    * Run `gulp test` to run unit tests

### Fire it up ###

Run server
----------
After build run:
~~~
npm start
~~~

Now go to http://localhost:5858 to start playing.

Acceptance tests
----------------
* Requirements
    * CodeceptJS (http://codecept.io/)

    `npm install -g codeceptjs`

    * WebdriverIO 

    `npm install -g webdriverio`

    * WebDriver protocol server (one of):
        * Selenium (http://www.seleniumhq.org/download/)
        * PhantomJS (http://phantomjs.org/download.html)

* Running

1.    Start WebDriver protocol server:
    * Selenium server: `java -jar selenium-server-standalone-x.x.x.jar`
    * PhantomJS server: `phantomjs --webdriver=4444`
2.    Fire tests by typing: `codeceptjs run --steps`