Feature('Create room');

Scenario('Creating room only for logged user', (I) => {
   I.waitForAppLoad();
   I.beginRoomCreation();
   I.seeLoginPage();
});

Scenario('Start game after creating room', (I) => {
   I.waitForAppLoad();
   I.beginRoomCreation();
   I.logIn();
   I.beginRoomCreation();
   I.fillField({css: '#roomName'},'Test run');
   I.click('Create');
   I.waitForText("Score", 10);
});