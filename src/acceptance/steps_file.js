'use strict';

module.exports = function() {
  return actor({

      waitForAppLoad: function() {
          this.amOnPage('/');
          this.seeInTitle("HaxBall");
          this.waitForElement("//a[text()='HaxBall']", 10);
          this.waitForElement('//a[text()="Room create"]', 1);
      },
      waitForHomepage: function(){
          this.waitForText("Welcome to HaxBall!", 5)
      },
      logIn: function(){
          this.seeLoginPage();
          this.fillField('#playerName', "TestUser");
          this.click("Start");
          this.see("Select room to join");
      },
      seeLoginPage: function() {
          this.see("Define yourself");
          this.see("Your name");
      },
      beginRoomCreation: function() {
          this.click('#roomCreate');
      },
      createRoom: function() {
        this.amOnPage("/");
        this.click('Room create');
        this.fillField('Room name','Test run');
        this.click('Create');
        this.see("Score");
      }
  });
};
