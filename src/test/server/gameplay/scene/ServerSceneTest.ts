import chai = require("chai");
import bootstrap = require("../../../../main/server/bootstrap");
import sharedBootstrap = require("../../../../main/shared/sharedBootstrap");
import {CONTAINER} from "../../../../main/shared/DI";
import {ClientScene} from "../../../../main/client/gameplay/scene/ClientScene";
import {PhysicsManager} from "../../../../main/shared/gameplay/physics/PhysicsManager";
import {DrawableFactory} from "../../../../main/client/gameplay/drawables/DrawableFactory";
import {SnapshotManager} from "../../../../main/shared/gameplay/snapshots/SnapshotManager";
import {PhysicObject} from "../../../../main/shared/gameplay/physics/PhysicObject";
import {ServerScene} from "../../../../main/server/gameplay/scene/ServerScene";
import {TimeManager} from "../../../../main/shared/utils/TimeManager";
import {PhysicObjectTemplate} from "../../../../main/shared/gameplay/scene/templates/PhysicObjectTemplate";
import {PhysicObjectTemplateBuilder} from "../../../../main/shared/gameplay/scene/builders/PhysicObjectTemplateBuilder";


describe("Server scene", () => {

    var physicsManager = new PhysicsManager([]);
    physicsManager.initWorld();
    var scene:ServerScene = new ServerScene(physicsManager, new SnapshotManager(), new TimeManager());

    it("Should remove added object from scene", () => {
        var objectId = "testId";
        var object:PhysicObject = scene.createObjectFromTemplate(new PhysicObjectTemplateBuilder()
            .setId(objectId)
            .build());

        scene.addObject(object);
        chai.expect(scene.removeObject(objectId)).to.be.true;
        chai.expect(scene.getObject(objectId)).to.be.undefined;
    });

    it("Should get objects by predicate", () => {
        var objectId = "testId";
        var object:PhysicObject = scene.createObjectFromTemplate(new PhysicObjectTemplateBuilder()
            .setId(objectId)
            .build());

        scene.addObject(object);
        scene.addObject(scene.createObjectFromTemplate(new PhysicObjectTemplateBuilder()
            .setId("badId")
            .build()));

        var objectsWithPredicate = scene.getObjectsWithPredicate((object) => object.id == objectId);

        chai.expect(objectsWithPredicate).is.not.empty;
        chai.expect(objectsWithPredicate[0]).is.not.undefined.and.not.null;
        chai.expect(objectsWithPredicate[0].id).is.eq(objectId);
        chai.expect(objectsWithPredicate).has.length(1);
    });
});