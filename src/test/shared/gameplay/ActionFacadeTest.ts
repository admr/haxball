import chai = require("chai");
import {ActionFacade} from "../../../main/shared/gameplay/ActionFacade";
import {AbstractActionHandler} from "../../../main/shared/gameplay/AbstractActionHandler";
import {AbstractAction} from "../../../main/shared/gameplay/actions/AbstractAction";
import {ActionHandlingResult} from "../../../main/shared/gameplay/ActionHandlingResult";

describe("Action facade", () => {

    var incomingActionResolvedCount:number=0;
    var createdActionCount:number=0;

    before(() => {
        incomingActionResolvedCount=0;
        createdActionCount=0;
    });

    class TestAction extends AbstractAction {
        public actionNumber:number;
        public constructor(caller:string,actionNumber:number){
            super("test");
            this.caller = caller;
            this.actionNumber = actionNumber;
        }
    }

    class TestActionHandler extends AbstractActionHandler {
        getSupportedActionType():string {
            return "test";
        }

        handleAction(action:TestAction, isInternal:boolean):ActionHandlingResult {
            incomingActionResolvedCount++;
            return new ActionHandlingResult(new TestAction(action.caller, action.actionNumber));
        }
    }

    var handler:AbstractActionHandler = new TestActionHandler();

    function createAction(caller:string): AbstractAction {
        return new TestAction(caller, ++createdActionCount);
    }

    it("Should replace duplicated actions", () => {
        var actionFacade:ActionFacade = new ActionFacade([handler]);
        actionFacade.addIncomingAction(createAction("caller1"));
        actionFacade.addIncomingAction(createAction("caller2"));
        actionFacade.addIncomingAction(createAction("caller1"));
        actionFacade.addIncomingAction(createAction("caller1"));

        actionFacade.resolveIncomingActions();

        chai.expect(actionFacade.getOutgoingActions()).is.not.null.and.not.empty;
        chai.expect(actionFacade.getOutgoingActions()).has.length(2);
        var outgoingAction:TestAction = <TestAction>actionFacade.getOutgoingActions()[0];
        chai.expect(outgoingAction.actionNumber).is.eq(createdActionCount);
    })
});