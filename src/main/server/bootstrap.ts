import {CONTAINER} from "../shared/DI";
import {ClientsConnectionMap} from "./communication/ClientsConnectionMap";
import {RoomManager} from "./gameplay/RoomManager";
import {ServerLoop} from "./gameplay/ServerLoop";
import Server = SocketIO.Server;
import {GameDefinitionCreator} from "./gameplay/GameDefinitionCreator";
import {MovePlayerActionHandler} from "../shared/gameplay/actionhandlers/MovePlayerActionHandler";
import {TestActionHandler} from "../shared/gameplay/actionhandlers/TestActionHandler";
import {KickBallActionHandler} from "../shared/gameplay/actionhandlers/KickBallActionHandler";
import {PrePlayingPhase} from "./gameplay/gamephases/PrePlayingPhase";
import {PlayingPhase} from "./gameplay/gamephases/PlayingPhase";
import {PostGoalPhase} from "./gameplay/gamephases/PostGoalPhase";
import {ServerScene} from "./gameplay/scene/ServerScene";
import {AuthorizationRequestHandler} from "./communication/socketrequesthandlers/AuthorizationRequestHandler";
import {CreateRoomEventHandler} from "./communication/socketrequesthandlers/CreateRoomEventHandler";
import {GameDefinitionRequestHandler} from "./communication/socketrequesthandlers/GameDefinitionRequestHandler";
import {ActionHandler} from "./communication/socketrequesthandlers/ActionHandler";
import {JoinGameRequestHandler} from "./communication/socketrequesthandlers/JoinGameRequestHandler";
import {ObjectsDefinitionRequestHandler} from "./communication/socketrequesthandlers/ObjectsDefinitionRequestHandler";
import {ClientDisconnectEventHandler} from "./communication/socketrequesthandlers/ClientDisconnectEventHandler";
import {RoomsListRequestHandler} from "./communication/socketrequesthandlers/RoomsListRequestHandler";
import {GoalEventHandler} from "./gameplay/scene/eventhandlers/GoalEventHandler";
import {FirstBallContactHandler} from "./gameplay/scene/eventhandlers/FirstBallContactHandler";
import {ContactEventFacade} from "./gameplay/scene/ContactEventFacade";
import {SocketEventFacade} from "./communication/SocketEventFacade";
import {LeaveRoomEventHandler} from "./communication/socketrequesthandlers/LeaveRoomEventHandler";
import {RoomManagementService} from "./communication/service/RoomManagementService";
import {LoginRequestHandler} from "./communication/socketrequesthandlers/LoginRequestHandler";
import {RoomConfigRequestHandler} from "./communication/socketrequesthandlers/RoomConfigRequestHandler";
import {ServerGamePhaseManager} from "./gameplay/ServerGamePhaseManager";
import {SetRoomAttributeRequestHandler} from "./communication/socketrequesthandlers/SetRoomAttributeRequestHandler";
import {RoomConfigChangeProcessor} from "./gameplay/RoomConfigChangeProcessor";
import {StartMatchEventHandler} from "./communication/socketrequesthandlers/StartMatchEventHandler";
import {DbConnectionManager} from "./database/DbConnectionManager";
import {UsersRepository} from "./database/UsersRepository";

var socketIO = require('socket.io');

var sio = socketIO();

export function createGameplayBootstrap() {
    CONTAINER.bind("serverLoop").to.type(ServerLoop).as.singleton();
    CONTAINER.bind("scene").to.type(ServerScene).as.singleton();
    CONTAINER.bind("actionHandlers").to.type(MovePlayerActionHandler).as.singleton();
    CONTAINER.bind("actionHandlers").to.type(KickBallActionHandler).as.singleton();
    CONTAINER.bind("actionHandlers").to.type(TestActionHandler).as.singleton();
    CONTAINER.bind("gamePhaseManager").to.type(ServerGamePhaseManager).as.singleton();

    CONTAINER.bind("gamePhases").to.type(PrePlayingPhase).as.singleton();
    CONTAINER.bind("gamePhases").to.type(PlayingPhase).as.singleton();
    CONTAINER.bind("gamePhases").to.type(PostGoalPhase).as.singleton();
}

export function deleteGameplayBootstrap() {
    CONTAINER.unbind("serverLoop");
    CONTAINER.unbind("scene");
    CONTAINER.unbind("actionHandlers");
    CONTAINER.unbind("gamePhases");
    CONTAINER.unbind("gamePhaseManager");
}

function createEventHandlers() {
    CONTAINER.bind("eventHandler").to.type(AuthorizationRequestHandler).as.singleton();
    CONTAINER.bind("eventHandler").to.type(LoginRequestHandler).as.singleton();
    CONTAINER.bind("eventHandler").to.type(RoomConfigRequestHandler).as.singleton();
    CONTAINER.bind("eventHandler").to.type(SetRoomAttributeRequestHandler).as.singleton();
    CONTAINER.bind("eventHandler").to.type(CreateRoomEventHandler).as.singleton();
    CONTAINER.bind("eventHandler").to.type(GameDefinitionRequestHandler).as.singleton();
    CONTAINER.bind("eventHandler").to.type(ActionHandler).as.singleton();
    CONTAINER.bind("eventHandler").to.type(JoinGameRequestHandler).as.singleton();
    CONTAINER.bind("eventHandler").to.type(ObjectsDefinitionRequestHandler).as.singleton();
    CONTAINER.bind("eventHandler").to.type(ClientDisconnectEventHandler).as.singleton();
    CONTAINER.bind("eventHandler").to.type(LeaveRoomEventHandler).as.singleton();
    CONTAINER.bind("eventHandler").to.type(RoomsListRequestHandler).as.singleton();
    CONTAINER.bind("eventHandler").to.type(StartMatchEventHandler).as.singleton();
}

function createContactHandlers() {
    CONTAINER.bind("contactEventHandlers").to.type(GoalEventHandler).as.singleton();
    CONTAINER.bind("contactEventHandlers").to.type(FirstBallContactHandler).as.singleton();
}

export function createGlobalBootstrap() {
    CONTAINER.bind("io").as.instance(sio);
    CONTAINER.bind("clientsConnectionMap").to.type(ClientsConnectionMap).as.singleton();
    CONTAINER.bind("socketEventFacade").to.type(SocketEventFacade).as.singleton();
    CONTAINER.bind("roomManager").to.type(RoomManager).as.singleton();
    CONTAINER.bind("gameDefinitionCreator").to.type(GameDefinitionCreator).as.singleton();
    CONTAINER.bind("contactEventFacade").to.type(ContactEventFacade).as.singleton();
    CONTAINER.bind("roomManagementService").to.type(RoomManagementService).as.singleton();
    CONTAINER.bind("roomConfigChangeListener").to.type(RoomConfigChangeProcessor).as.singleton();
    CONTAINER.bind("dbConnectionManager").to.type(DbConnectionManager).as.singleton();
    CONTAINER.bind("usersRepository").to.type(UsersRepository).as.singleton();
    createContactHandlers();
    createEventHandlers();
}