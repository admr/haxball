export class CollisionRule {
    public static get DEFAULT_GROUP():number {
        return 1;
    };

    public static get BOARD_GROUP():number {
        return Math.pow(2, 1)
    };

    public static get BALL_GROUP():number {
        return Math.pow(2, 2)
    };

    public static get PLAYER_GROUP():number {
        return Math.pow(2, 3)
    };

    public static get PISTIL_GROUP():number {
        return Math.pow(2, 4)
    };

    public static BOARD_RULE:CollisionRule = new CollisionRule(CollisionRule.BOARD_GROUP, CollisionRule.BALL_GROUP);
    public static BALL_RULE:CollisionRule = new CollisionRule(CollisionRule.BALL_GROUP, CollisionRule.PLAYER_GROUP |
        CollisionRule.BOARD_GROUP | CollisionRule.PISTIL_GROUP | CollisionRule.DEFAULT_GROUP);
    public static PLAYER_RULE:CollisionRule = new CollisionRule(CollisionRule.PLAYER_GROUP, CollisionRule.BALL_GROUP |
        CollisionRule.PISTIL_GROUP | CollisionRule.PLAYER_GROUP);
    public static PISTIL_RULE:CollisionRule = new CollisionRule(CollisionRule.PISTIL_GROUP, CollisionRule.BALL_GROUP |
        CollisionRule.PLAYER_GROUP);

    private _collisionGroup:number;
    private _collisionMask:number;


    constructor(collisionGroup:number, collisionMask:number) {
        this._collisionGroup = collisionGroup;
        this._collisionMask = collisionMask;
    }

    public get collisionGroup():number {
        return this._collisionGroup;
    }

    public get collisionMask():number {
        return this._collisionMask;
    }
}