import {GamePhaseManager} from "../../shared/gameplay/GamePhaseManager";
import {AbstractGamePhase} from "../../shared/gameplay/AbstractGamePhase";
import {Room} from "./Room";
import {GamePhaseType} from "../../shared/gameplay/GamePhaseType";
import {Messages} from "../../shared/communication/Messages";
import {GamePhaseChanged} from "../../shared/communication/GamePhaseChanged";
export class ServerGamePhaseManager extends GamePhaseManager{

    private io:SocketIO.Server;
    private room:Room;

    public constructor(gamePhases:AbstractGamePhase[],io:SocketIO.Server) {
        super(gamePhases);
        this.io = io;
    }

    public setRoom(room:Room):void {
        this.room = room;
    }

    public setPhase(nextPhase:GamePhaseType, args:any = null):boolean {
        console.log('Begin changing phase to: ' + GamePhaseType[nextPhase]);
        var previousType:GamePhaseType = null;
        if (this._currentPhase != null) {
            if (this._currentPhase.getAllowedNextPhases().indexOf(nextPhase) == -1) {
                console.log('Changing phase to: ' + GamePhaseType[nextPhase] + ' not allowed');
                return false;
            } else {
                previousType = this._currentPhase.getType();
                this._currentPhase.onExit(nextPhase);
            }
        }

        this._currentPhase = this.gamePhases[nextPhase];
        this._currentPhase.onEnter(previousType, args);
        this.io.in(this.room.id).emit(Messages.GAME_PHASE_CHANGE, new GamePhaseChanged(previousType, nextPhase));
        console.log('Changed phase to: ' + GamePhaseType[nextPhase]);
        return true;
    }
}