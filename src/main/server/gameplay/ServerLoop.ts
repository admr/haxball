import {PhysicsManager} from "../../shared/gameplay/physics/PhysicsManager";
import Server = SocketIO.Server;
import {AbstractLoop} from "../../shared/gameplay/AbstractLoop";
import {ServerScene} from "../gameplay/scene/ServerScene";
import {Messages} from "../../shared/communication/Messages";
import {ActionFacade} from "../../shared/gameplay/ActionFacade";
import {GamePhaseManager} from "../../shared/gameplay/GamePhaseManager";
import {TimeManager} from "../../shared/utils/TimeManager";

export class ServerLoop extends AbstractLoop {
    private stateSendInterval = 100;
    private io:Server;
    private _active:Boolean = false;
    private _roomId:string;
    protected scene:ServerScene;
    private lastTimeSended:number;

    constructor(physicsManager:PhysicsManager, io:Server, scene:ServerScene, actionFacade:ActionFacade, gamePhaseManager:GamePhaseManager,
                timeManager:TimeManager) {
        super(physicsManager, actionFacade, scene, gamePhaseManager, timeManager);
        this.io = io;
        this.scene = scene;
        this.lastTimeSended = timeManager.getNowTimestamp();
    }


    public step():void {
        if(this._active){
            super.step();
        }
    }

    protected postStep():void {
        try {
            var currentTime:number = this.timeManager.getNowTimestamp();
            if ((currentTime - this.lastTimeSended) > this.stateSendInterval) {
                this.lastTimeSended = currentTime;
                this.sendGameState();
            }
        } catch (Error) {
            console.log(Error);
            throw Error;
        }
    }

    protected sendGameState() {
        this.gameSnapshot.timeLeft = this.scene.timeLeft;
        this.gameSnapshot.physicObjectsSnapshots = this.scene.getObjectsStates();

        this.io.in(this.roomId).emit(Messages.GAME_STATE, this.gameSnapshot);
    }

    get active():Boolean {
        return this._active;
    }

    set active(value:Boolean) {
        this._active = value;
    }

    get roomId():string {
        return this._roomId;
    }

    set roomId(value:string) {
        this._roomId = value;
    }
}