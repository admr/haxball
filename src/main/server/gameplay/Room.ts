import {ServerLoop} from "./ServerLoop";
import {PhysicsManager} from "../../shared/gameplay/physics/PhysicsManager";
import {GameTemplate} from "../../shared/gameplay/scene/templates/GameTemplate";
import {ServerScene} from "../gameplay/scene/ServerScene";
import {GamePhaseType} from "../../shared/gameplay/GamePhaseType";
import {RoomInfo} from "../../shared/communication/RoomInfo";
import {RoomConfiguration} from "../../shared/communication/RoomConfiguration";
import {ServerGamePhaseManager} from "./ServerGamePhaseManager";

export class Room {
    private _serverLoop:ServerLoop;
    private physicsManager:PhysicsManager;
    private _gameTemplate:GameTemplate;
    private _scene:ServerScene;
    private _gamePhaseManager:ServerGamePhaseManager;
    private _roomInfo:RoomInfo;
    private _isActive:boolean = true;
    private _roomConfiguration:RoomConfiguration = new RoomConfiguration();

    public constructor(id:string, roomName:string, serverLoop:ServerLoop, physicsManager:PhysicsManager, scene:ServerScene,
                       gamePhaseManager:ServerGamePhaseManager) {
        this._serverLoop = serverLoop;
        this.physicsManager = physicsManager;
        this._scene = scene;
        this._gamePhaseManager = gamePhaseManager;
        this._scene.id = id;
        this._roomInfo = new RoomInfo(id, roomName, 0);
    }

    get id():string {
        return this._roomInfo.id;
    }

    get serverLoop():ServerLoop {
        return this._serverLoop;
    }

    public startRoom():void {

    }

    public startPlaying():void {
        this.prepareScene();
    }

    public updateRoom():void {
        if (this._serverLoop.active) {
            this._serverLoop.step();
        }
    }

    protected prepareScene():void {
        this.physicsManager.initWorld();
        this._serverLoop.active = true;
        this._serverLoop.roomId = this.id;
        this.scene.timeLeft = this.roomConfiguration.timeLeft*60;

        this._scene.createGameWorld(this.gameTemplate);

        this._gamePhaseManager.setPhase(GamePhaseType.PRE_PLAYING);
    }

    public notifyPlayerConnected() {
        this._roomInfo.players++;
    }

    public notifyPlayerDisconnected() {
        this._roomInfo.players--;
        if (this._roomInfo.players <= 0) {
            this._isActive = false;
        }
    }

    get roomConfiguration():RoomConfiguration {
        return this._roomConfiguration;
    }

    set roomConfiguration(value:RoomConfiguration) {
        this._roomConfiguration = value;
    }

    get gamePhaseManager():ServerGamePhaseManager {
        return this._gamePhaseManager;
    }

    get scene():ServerScene {
        return this._scene;
    }

    get gameTemplate():GameTemplate {
        return this._gameTemplate;
    }

    get roomName():string {
        return this._roomInfo.roomName;
    }

    get isActive():boolean {
        return this._isActive;
    }

    public get roomInfo():RoomInfo {
        return new RoomInfo(this._roomInfo.id, this._roomInfo.roomName, this._roomInfo.players);
    }

    set gameTemplate(value:GameTemplate) {
        this._gameTemplate = value;
    }
}