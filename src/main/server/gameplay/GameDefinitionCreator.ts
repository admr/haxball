import {GameTemplate} from "../../shared/gameplay/scene/templates/GameTemplate";
import {MockTemplateCreator} from "../mock/MockTemplateCreator";
import {PhysicObjectTemplateBuilder} from "../../shared/gameplay/scene/builders/PhysicObjectTemplateBuilder";
import {ShapeTemplate} from "../../shared/gameplay/scene/templates/ShapeTemplate";
import {p2} from "../../shared/sharedBootstrap";
import {PhysicObjectTemplate} from "../../shared/gameplay/scene/templates/PhysicObjectTemplate";
import {ShapeTemplateBuilder} from "../../shared/gameplay/scene/builders/ShapeTemplateBuilder";
import {CollisionRule} from "./physics/CollisionRule";
import {SensorTemplate} from "../../shared/gameplay/scene/templates/SensorTemplate";
import {TeamTemplate} from "../../shared/gameplay/scene/templates/TeamTemplate";
import {RendererTemplate} from "../../shared/gameplay/scene/templates/RendererTemplate";

export class GameDefinitionCreator {

    public static DEFAULT_BOARD_SIZE:number[] = [600, 300];
    public static DEFAULT_GATEWAY_SIZE:number[] = [50, 100];
    public static DEFAULT_BALL_SIZE:number = 10;

    public boardSize:number[] = GameDefinitionCreator.DEFAULT_BOARD_SIZE;
    public gatewaySize:number[] = GameDefinitionCreator.DEFAULT_GATEWAY_SIZE;
    public ballSize:number = GameDefinitionCreator.DEFAULT_BALL_SIZE;

    public createGameDefinition(name:string):GameTemplate {
        return MockTemplateCreator.createDefaultGameTemplateBuilder()
            .setBoard(MockTemplateCreator.createBoardTemplate(this.boardSize, this.gatewaySize))
            .setTeamTemplates([
                new TeamTemplate(0, new RendererTemplate("CircleRenderer", {lineWidth: 4, lineColor: 0xffff0000}), [-400, -300, 0, 300]),
                new TeamTemplate(1, new RendererTemplate("CircleRenderer", {lineWidth: 4, lineColor: 0xff0000ff}), [0, -300, 400, 300])
            ])
            .addObject(
                new PhysicObjectTemplateBuilder()
                    .setId('ball')
                    .setPosition([0, 0])
                    .setMass(0.01)
                    .setDamping(0.5)
                    .addShapeTemplate(new ShapeTemplateBuilder()
                        .setType(p2.Shape.CIRCLE)
                        .setDimensions([this.ballSize])
                        .setMaterialId('ball')
                        .setCollisionGroup(CollisionRule.BALL_RULE.collisionGroup)
                        .setCollisionMask(CollisionRule.BALL_RULE.collisionMask)
                        .build())
                    .setDrawable(MockTemplateCreator.createDrawableTemplate())
                    .build()
            )
            .build();
    }

    public createSensors(name:string):SensorTemplate[] {
        return MockTemplateCreator.createGoalSensors(this.boardSize, this.gatewaySize, this.ballSize);
    }

    public createPlayer(id:string, teamTemplate:TeamTemplate):PhysicObjectTemplate {
        return MockTemplateCreator.createPlayerTemplate(id, [0, 0], teamTemplate);
    }
}