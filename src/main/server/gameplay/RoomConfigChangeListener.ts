import {Room} from "./Room";

export interface RoomConfigChangeListener {
    onRoomConfigChanged(room:Room):void;
}