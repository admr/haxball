import {AbstractGamePhase} from "../../../shared/gameplay/AbstractGamePhase";
import {GamePhaseManager} from "../../../shared/gameplay/GamePhaseManager";
import {GamePhaseType} from "../../../shared/gameplay/GamePhaseType";
import {ServerScene} from "../../gameplay/scene/ServerScene";
import {ServerLoop} from "../ServerLoop";
import {RoomManager} from "../RoomManager";
import {Room} from "../Room";
import {TeamTemplate} from "../../../shared/gameplay/scene/templates/TeamTemplate";
import {PhysicObject} from "../../../shared/gameplay/physics/PhysicObject";
import {ContactEventFacade} from "../../gameplay/scene/ContactEventFacade";

export class PrePlayingPhase extends AbstractGamePhase {

    private scene:ServerScene;
    private roomManager:RoomManager;
    private contactEventFacade:ContactEventFacade;

    constructor(scene:ServerScene, roomManager:RoomManager, contactEventFacade:ContactEventFacade) {
        super();
        this.scene = scene;
        this.roomManager = roomManager;
        this.contactEventFacade = contactEventFacade;
    }

    update(dt:number, gamePhaseManager:GamePhaseManager):void {
    }

    getType():GamePhaseType {
        return GamePhaseType.PRE_PLAYING;
    }

    onEnter(previousState:GamePhaseType, args:any):void {
        console.log("On enter (PrePlayingPhase)");
        var room:Room = this.roomManager.getRoom(this.scene.id);
        if (room != null) {
            var teamTemplates:TeamTemplate[] = room.gameTemplate.teamTemplates;
            for (var i in teamTemplates) {
                var objects:PhysicObject[] = this.scene.getObjectsWithPredicate(
                    (object:PhysicObject) => object.playerInfo != null && object.playerInfo.team == teamTemplates[i].id
                );

                PrePlayingPhase.arrangePlayersInTeam(objects, teamTemplates[i]);
            }

            this.arrangeBall();
            this.contactEventFacade.registerHandlerForOneObject('firstBallContact', 'ball', this.scene);
        } else {
            throw new Error("No room: " + this.scene.id);
        }
    }

    protected arrangeBall() {
        var ball:PhysicObject = this.scene.getObject('ball');
        if (ball != null) {
            ball.body.position = [0, 0];
            ball.body.velocity = [0, 0];
        }
    }

    public static arrangePlayersInTeam(players:PhysicObject[], teamTemplate:TeamTemplate):void {
        var areaWidth = teamTemplate.startArea[2] - teamTemplate.startArea[0];
        var areaHeight = teamTemplate.startArea[3] - teamTemplate.startArea[1];

        for (let player of players) {
            player.body.position = [teamTemplate.startArea[0] + areaWidth / 2.0, teamTemplate.startArea[1] + areaHeight / 2.0];
        }
    }

    onExit(nextState:GamePhaseType):void {

    }

    getAllowedNextPhases():GamePhaseType[] {
        return [GamePhaseType.PLAYING];
    }

}