import {AbstractGamePhase} from "../../../shared/gameplay/AbstractGamePhase";
import {GamePhaseManager} from "../../../shared/gameplay/GamePhaseManager";
import {GamePhaseType} from "../../../shared/gameplay/GamePhaseType";
import {ServerScene} from "../scene/ServerScene";

export class PlayingPhase extends AbstractGamePhase {

    private scene:ServerScene;

    constructor(scene:ServerScene){
        super();
        this.scene = scene;
    }

    update(dt:number, gamePhaseManager:GamePhaseManager):void {
        this.scene.timeLeft = this.scene.timeLeft - dt;
    }

    getType():GamePhaseType {
        return GamePhaseType.PLAYING;
    }

    onEnter(previousState:GamePhaseType, args:any):void {
    }

    onExit(nextState:GamePhaseType):void {
    }

    getAllowedNextPhases():GamePhaseType[] {
        return [GamePhaseType.POST_GOAL];
    }

}