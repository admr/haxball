import {AbstractGamePhase} from "../../../shared/gameplay/AbstractGamePhase";
import {GamePhaseManager} from "../../../shared/gameplay/GamePhaseManager";
import {GamePhaseType} from "../../../shared/gameplay/GamePhaseType";

export class PostGoalPhase extends AbstractGamePhase {

    private static RESTART_TIME:number = 3;
    private _timeLeftToRestart:number;

    constructor() {
        super();
    }

    update(dt:number, gamePhaseManager:GamePhaseManager):void {
        this._timeLeftToRestart -= dt;
        if (this._timeLeftToRestart < 0) {
            gamePhaseManager.setPhase(GamePhaseType.PRE_PLAYING);
        }
    }

    getType():GamePhaseType {
        return GamePhaseType.POST_GOAL;
    }

    onEnter(previousState:GamePhaseType, args:any):void {
        this._timeLeftToRestart = PostGoalPhase.RESTART_TIME;
    }

    onExit(nextState:GamePhaseType):void {

    }

    getAllowedNextPhases():GamePhaseType[] {
        return [GamePhaseType.PRE_PLAYING];
    }

}