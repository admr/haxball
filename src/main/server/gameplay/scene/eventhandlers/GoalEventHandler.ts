import {ContactHandler} from "../../../../shared/gameplay/scene/ContactHandler";
import {PhysicObject} from "../../../../shared/gameplay/physics/PhysicObject";
import {AbstractScene} from "../../../../shared/gameplay/scene/AbstractScene";
import {RoomManager} from "../../../gameplay/RoomManager";
import {Room} from "../../../gameplay/Room";
import {GamePhaseType} from "../../../../shared/gameplay/GamePhaseType";
import {ContactHandlerType} from "../../../../shared/gameplay/scene/ContactHandlerType";

export class GoalEventHandler implements ContactHandler {

    private roomManager:RoomManager;

    constructor(roomManager:RoomManager) {
        this.roomManager = roomManager;
    }

    onBeginContact(a:PhysicObject, b:PhysicObject, scene:AbstractScene, contactId:string) {
        var room:Room = this.roomManager.getRoom(scene.id);
        if (room != null && room.gamePhaseManager.currentPhase.getType() == GamePhaseType.PLAYING) {
            if (a.eventData != null) {
                room.serverLoop.gameSnapshot.teamScores[<number>a.eventData]++;
                room.gamePhaseManager.setPhase(GamePhaseType.POST_GOAL);
            }
        }
    }

    getEventType():string {
        return ContactHandlerType.GOAL;
    }
}