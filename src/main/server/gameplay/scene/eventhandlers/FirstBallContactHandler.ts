import {ContactHandler} from "../../../../shared/gameplay/scene/ContactHandler";
import {PhysicObject} from "../../../../shared/gameplay/physics/PhysicObject";
import {AbstractScene} from "../../../../shared/gameplay/scene/AbstractScene";
import {GamePhaseType} from "../../../../shared/gameplay/GamePhaseType";
import {RoomManager} from "../../../gameplay/RoomManager";
import {Room} from "../../../gameplay/Room";
import {ContactHandlerType} from "../../../../shared/gameplay/scene/ContactHandlerType";

export class FirstBallContactHandler implements ContactHandler {

    private roomManager:RoomManager;

    constructor(roomManager:RoomManager) {
        this.roomManager = roomManager;
    }

    onBeginContact(a:PhysicObject, b:PhysicObject, scene:AbstractScene, contactId:string) {
        var room:Room = this.roomManager.getRoom(scene.id);
        if (room != null) {
            room.gamePhaseManager.setPhase(GamePhaseType.PLAYING);
            scene.contactEventHandler.removeHandler(contactId);
        }
    }

    getEventType():string {
        return ContactHandlerType.FIRST_BALL_CONTACT;
    }

}