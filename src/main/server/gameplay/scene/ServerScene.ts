import {AbstractScene} from "../../../shared/gameplay/scene/AbstractScene";
import {PhysicObjectSnapshot} from "../../../shared/gameplay/snapshots/PhysicObjectSnapshot";
import {SnapshotManager} from "../../../shared/gameplay/snapshots/SnapshotManager";
import {PhysicsManager} from "../../../shared/gameplay/physics/PhysicsManager";
import {PhysicObjectTemplate} from "../../../shared/gameplay/scene/templates/PhysicObjectTemplate";
import {BoardTemplate} from "../../../shared/gameplay/scene/templates/BoardTemplate";
import {ServerWorldObject} from "../../gameplay/objects/ServerWorldObject";
import {SensorTemplate} from "../../../shared/gameplay/scene/templates/SensorTemplate";
import {Map} from "../../../shared/utils/Map";
import {TimeManager} from "../../../shared/utils/TimeManager";

export class ServerScene extends AbstractScene {

    protected objects:{[id:string] : ServerWorldObject} = {};
    private templates:{[id:string] : PhysicObjectTemplate} = {};

    private timeManager:TimeManager;
    private snapshotManager:SnapshotManager;

    private _timeLeft:number;

    public constructor(physicsManager:PhysicsManager, snapshotManager:SnapshotManager, timeManager:TimeManager) {
        super(physicsManager);
        this.snapshotManager = snapshotManager;
        this.timeManager = timeManager;
    }

    public createPrivateObjectFromTemplate(template:PhysicObjectTemplate):ServerWorldObject {
        var out:ServerWorldObject = new ServerWorldObject();
        this.applyObjectTemplate(out, template);
        out.isPrivate = true;

        return out;
    }

    public createFromSensorTemplate(template:SensorTemplate):ServerWorldObject {
        var out:ServerWorldObject = new ServerWorldObject();
        this.applySensorTemplate(out, template);
        out.isPrivate = true;

        return out;
    }

    public createFromBoardTemplate(template:BoardTemplate):ServerWorldObject {
        var out:ServerWorldObject = new ServerWorldObject();
        super.applyBoardTemplate(out, template);

        return out;
    }

    public createObjectFromTemplate(template:PhysicObjectTemplate):ServerWorldObject {
        var out:ServerWorldObject = new ServerWorldObject();
        this.applyObjectTemplate(out, template);

        if (this.templates[template.id] != null) {
            //TODO: replace to log error...
            throw new Error("Object with id: " + template.id + " has already created template.")
        }

        this.templates[template.id] = template;

        return out;
    }

    public getObjectTemplate(id:string):PhysicObjectTemplate {
        return this.templates[id];
    }

    public removeObject(id:string):boolean {
        var out:boolean = super.removeObject(id);

        if (out) {
            delete this.templates[id];
        }

        return out;
    }

    public getObjectsStates():Map<PhysicObjectSnapshot> {
        var out:Map<PhysicObjectSnapshot> = {};
        var timestamp:number = this.timeManager.getNowTimestamp();
        for (let key in this.objects) {
            if (!this.objects[key].isPrivate) {
                var state:PhysicObjectSnapshot = this.snapshotManager.getStateFromPhysicObject(this.objects[key], timestamp);
                out[key] = state;
            }
        }

        return out;
    }

    get timeLeft():number {
        return this._timeLeft;
    }

    set timeLeft(value:number) {
        this._timeLeft = value;
    }
}