import {ContactHandler} from "../../../shared/gameplay/scene/ContactHandler";
import {AbstractScene} from "../../../shared/gameplay/scene/AbstractScene";
import {isArray} from "util";

export class ContactEventFacade {

    private contactHandlers:{[type:string]: ContactHandler} = {};

    private registerHandlers(contactHandlers:ContactHandler[]) {
        for (let i in contactHandlers) {
            this.contactHandlers[contactHandlers[i].getEventType()] = contactHandlers[i];
        }
    }

    constructor(contactEventHandlers:any) {
        if (isArray(contactEventHandlers)) {
            this.registerHandlers(<ContactHandler[]>contactEventHandlers);
        } else {
            this.registerHandlers([contactEventHandlers]);
        }
    }

    public registerHandlerForPair(type:string, idA:string, idB:string, scene:AbstractScene) {
        var handler:ContactHandler = this.contactHandlers[type];
        if (handler != null) {
            scene.contactEventHandler.addHandlerForObjectsPair(idA, idB, handler);
        } else {
            throw new Error("No contact handler for type: " + type);
        }
    }

    public registerHandlerForOneObject(type:string, id:string, scene:AbstractScene) {
        var handler:ContactHandler = this.contactHandlers[type];
        if (handler != null) {
            scene.contactEventHandler.addHandlerForObject(id, handler);
        } else {
            throw new Error("No contact handler for type: " + type);
        }
    }
}