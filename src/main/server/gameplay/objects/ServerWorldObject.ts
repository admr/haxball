import {PhysicObject} from "../../../shared/gameplay/physics/PhysicObject";

export class ServerWorldObject extends PhysicObject {
    private _isPrivate:boolean = false;

    get isPrivate():boolean {
        return this._isPrivate;
    }

    set isPrivate(value:boolean) {
        this._isPrivate = value;
    }
}