import {RoomConfigChangeListener} from "./RoomConfigChangeListener";
import {Room} from "./Room";
import {RoomManagementService} from "../communication/service/RoomManagementService";
import {Messages} from "../../shared/communication/Messages";
import {RoomConfiguration} from "../../shared/communication/RoomConfiguration";

export class RoomConfigChangeProcessor implements RoomConfigChangeListener {

    private io:SocketIO.Server;
    private roomManagementService:RoomManagementService;

    constructor(io:SocketIO.Server,roomManagementService:RoomManagementService){
        this.io = io;
        this.roomManagementService = roomManagementService;
    }

    onRoomConfigChanged(room:Room):void {
        console.log("onRoomConfigChanged: " + room.id);
        var roomConfiguration:RoomConfiguration = this.roomManagementService.getRoomConfiguration(room);
        console.log("Sending room configuration: "+JSON.stringify(roomConfiguration));
        this.io.in(room.id).emit(Messages.ROOM_CONFIG, roomConfiguration);
    }
}