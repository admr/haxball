import {Room} from "./Room";
import {CONTAINER} from "../../shared/DI";
import bootstrap = require('../bootstrap');
import sharedBootstrap = require('../../shared/sharedBootstrap');
import {ServerLoop} from "./ServerLoop";
import {PhysicsManager} from "../../shared/gameplay/physics/PhysicsManager";
import {ServerScene} from "../gameplay/scene/ServerScene";
import {GamePhaseManager} from "../../shared/gameplay/GamePhaseManager";
import {ServerGamePhaseManager} from "./ServerGamePhaseManager";

export class RoomManager {
    private rooms:{[key: string] : Room} = {};

    public constructor() {

    }

    public createRoom(id:string, name:string):Room {
        sharedBootstrap.createGameplayBootstrap();
        bootstrap.createGameplayBootstrap();

        var loop:ServerLoop = CONTAINER.get<ServerLoop>("serverLoop");
        var physicsManager:PhysicsManager = CONTAINER.get<PhysicsManager>("physicsManager");
        var scene:ServerScene = CONTAINER.get<ServerScene>("scene");
        var gamePhaseManager:ServerGamePhaseManager = CONTAINER.get<ServerGamePhaseManager>("gamePhaseManager");

        var out:Room = new Room(id, name, loop, physicsManager, scene, gamePhaseManager);

        gamePhaseManager.setRoom(out);

        sharedBootstrap.deleteGameplayBootstrap();
        bootstrap.deleteGameplayBootstrap();

        this.rooms[id] = out;

        return out;
    }

    public getRoom(id:string) {
        return this.rooms[id];
    }

    public getAllRoomIds():string[] {
        var out:string[] = [];
        for (let name in this.rooms) {
            out.push(name);
        }
        return out;
    }

    public updateRooms() {
        var roomsToDelete:string[] = [];
        for (var key in this.rooms) {
            var room:Room = this.rooms[key];
            room.updateRoom();

            if (!room.isActive) {
                roomsToDelete.push(room.id);
            }
        }

        for (let roomId of roomsToDelete) {
            console.log('Deleting room: ' + roomId);
            delete this.rooms[roomId];
        }
    }
}