import bootstrap = require("./bootstrap");
import sharedBootstrap = require("../shared/sharedBootstrap");

sharedBootstrap.createGlobalBootstrap();
bootstrap.createGlobalBootstrap();

import {SocketEventFacade} from './communication/SocketEventFacade';
import {CONTAINER} from "../shared/DI";
import {isArray} from "util";
import {ClientsConnectionMap} from "./communication/ClientsConnectionMap";
import {ClientConnectionData} from "./communication/ClientConnectionData";
import Server = SocketIO.Server;
import {RoomManager} from "./gameplay/RoomManager";
import {ServerEventHandler} from "./communication/ServerEventHandler";
import {DbConnectionManager} from "./database/DbConnectionManager";

var nodeStatic = require('node-static');
var http = require('http');


var sio:Server = CONTAINER.get<Server>("io");
var roomManager:RoomManager = CONTAINER.get<RoomManager>("roomManager");
var socketEventFacade:SocketEventFacade = CONTAINER.get<SocketEventFacade>("socketEventFacade");
var eventHandlers:any = CONTAINER.get<ServerEventHandler>("eventHandler");
var clientsConnectionMap:ClientsConnectionMap = CONTAINER.get<ClientsConnectionMap>("clientsConnectionMap");
var dbConnectionManager:DbConnectionManager = CONTAINER.get<DbConnectionManager>("dbConnectionManager");

dbConnectionManager.openDbConnection("mongodb://127.0.0.1:27017/haxBall");

if (eventHandlers != null) {
    if (isArray(eventHandlers)) {
        for (var i = 0; i < eventHandlers.length; ++i) {
            socketEventFacade.addEventHandler(eventHandlers[i]);
        }
    } else {
        socketEventFacade.addEventHandler(eventHandlers);
    }
}


var file = new nodeStatic.Server('build', {
    cache: 0,
    gzip: true
});


var httpServer = http.createServer(function (request, response) {
    request.addListener('end', function () {
        file.serve(request, response);
    }).resume();
}).listen(5858);

sio.serveClient(true);
sio.attach(httpServer);
console.log('Server attached on port ' + 5858);

sio.on('connection', function (socket) {
    console.log('User ' + socket.id + ' connected');
    clientsConnectionMap.addClient(socket.id, new ClientConnectionData(socket.id));

    socketEventFacade.configureSocket(socket, socket.id);

    console.log('Socket configured');
});

function gameLoop() {
    roomManager.updateRooms();
    setTimeout(gameLoop, 1);
}

setTimeout(gameLoop, 1);

process.stdin.resume();//so the program will not close instantly

function exitHandler(options, err) {
    console.log('cleanup');
    dbConnectionManager.closeConnection();
}

//do something when app is closing
process.on('exit', exitHandler.bind(null,{cleanup:true}));
