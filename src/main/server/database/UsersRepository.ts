import {DbConnectionManager} from "./DbConnectionManager";
import {User} from "./User";
import {IConnectionStateListener} from "./IConnectionStateListener";
import Q = require('q');
/**
 * Created by Adrian on 2017-01-03.
 */
export class UsersRepository implements IConnectionStateListener{
    private dbConnectionManager:DbConnectionManager;
    private userCollection:any;

    constructor(dbConnectionManager:DbConnectionManager) {
        this.dbConnectionManager = dbConnectionManager;
        this.dbConnectionManager.addConnectionStateListener(this);
    }

    public getUserById(id:string):Q.Promise<User>{
        var deferred:Q.Deferred<User> = Q.defer<User>();
        var searchObject = {"username": id};
        this.userCollection.findOne(searchObject, (err, document) => {
            if(err != null) {
                console.log(err);
                deferred.reject(new Error());
            }
            deferred.resolve(document);
        });

        return deferred.promise;

    }

    onConnect(dbConnection): void {
        this.userCollection = dbConnection.collection('users');
    }

    onDisconnect(): void {
        this.userCollection = null;
    }
}