import {IConnectionStateListener} from "./IConnectionStateListener";
/**
 * Created by Adrian on 2017-01-03.
 */
var MongoClient = require('mongodb').MongoClient;

export class DbConnectionManager {

    static dbUrl: string = 'mongodb://127.0.0.1:27017/share';

    private connectionStateListeners:IConnectionStateListener[] = [];

    dbConnection: any = null;

    public openDbConnection(url:string): void {
        if(!this.isConnected()) {
            MongoClient.connect(url, (err, db) => {
                if(err != null) {
                    console.log('An error occurred', err);
                }
                this.dbConnection = db;

                this.connectionStateListeners.forEach(listener => listener.onConnect(this.dbConnection));
            })
        }
    }

    public addConnectionStateListener(listener:IConnectionStateListener):void {
        this.connectionStateListeners.push(listener);
    }

    public closeConnection():void {
        if(this.isConnected()) {
            this.dbConnection.close();
            this.connectionStateListeners.forEach(listener => listener.onDisconnect());
            this.dbConnection = null;
        }
    }

    public isConnected():boolean {
        return this.dbConnection != null;
    }

    public getConnection():any {
        return this.dbConnection;
    }
}