/**
 * Created by Adrian on 2017-01-03.
 */
export interface IConnectionStateListener {
    onConnect(dbConnection):void;
    onDisconnect():void;
}