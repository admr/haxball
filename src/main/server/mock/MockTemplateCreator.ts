import {p2} from '../../shared/sharedBootstrap';
import {PlayerInfo} from "../../shared/gameplay/player/PlayerInfo";
import {GameTemplateBuilder} from "../../shared/gameplay/scene/builders/GameTemplateBuilder";
import {MaterialContactTemplateBuilder} from "../../shared/gameplay/scene/builders/MaterialContactTemplateBuilder";
import {TeamTemplate} from "../../shared/gameplay/scene/templates/TeamTemplate";
import {PhysicObjectTemplate} from "../../shared/gameplay/scene/templates/PhysicObjectTemplate";
import {PhysicObjectTemplateBuilder} from "../../shared/gameplay/scene/builders/PhysicObjectTemplateBuilder";
import {ShapeTemplateBuilder} from "../../shared/gameplay/scene/builders/ShapeTemplateBuilder";
import {CollisionRule} from "../gameplay/physics/CollisionRule";
import {DrawableTemplateBuilder} from "../../shared/gameplay/scene/builders/DrawableTemplateBuilder";
import {ShapeTemplate} from "../../shared/gameplay/scene/templates/ShapeTemplate";
import {DrawableTemplate} from "../../shared/gameplay/scene/templates/DrawableTemplate";
import {RendererTemplate} from "../../shared/gameplay/scene/templates/RendererTemplate";
import {SensorTemplate} from "../../shared/gameplay/scene/templates/SensorTemplate";
import {SensorTemplateBuilder} from "../../shared/gameplay/scene/builders/SensorTemplateBuilder";
import {ContactHandlerType} from "../../shared/gameplay/scene/ContactHandlerType";
import {BoardTemplate} from "../../shared/gameplay/scene/templates/BoardTemplate";

export class MockTemplateCreator {

    public static createDefaultGameTemplateBuilder():GameTemplateBuilder {
        return new GameTemplateBuilder()
            .addMaterialContactTemplate(
                new MaterialContactTemplateBuilder('ball', 'board')
                    .setRestitution(1.0)
                    .setStiffness(Number.MAX_VALUE)
                    .setRelaxation(1)
                    .build())
            .addMaterialContactTemplate(
                new MaterialContactTemplateBuilder('ball', 'player')
                    .setRestitution(0)
                    .setStiffness(Number.MAX_VALUE)
                    .build())
            .addMaterialContactTemplate(
                new MaterialContactTemplateBuilder('player', 'pistil')
                    .setRestitution(0)
                    .build()
            )
    }

    public static createPlayerTemplate(id:string, position:number[], teamTemplate:TeamTemplate):PhysicObjectTemplate {
        return new PhysicObjectTemplateBuilder()
            .setId(id)
            .setPosition(position)
            .setBodyDefinition(<p2.BodyOptions>{
                mass: 1
            })
            .setDamping(0.8)
            .setShapeTemplates([
                MockTemplateCreator.createShapeTemplate('player'),
                new ShapeTemplateBuilder()
                    .setType(p2.Shape.CIRCLE)
                    .setDimensions([20])
                    .setIsSensor(true)
                    .setCollisionGroup(CollisionRule.PLAYER_RULE.collisionGroup)
                    .setCollisionMask(CollisionRule.PLAYER_RULE.collisionMask)
                    .build()
            ])
            .setDrawable(new DrawableTemplateBuilder("SimplePhysicObjectDrawable")
                .addRendererTemplate(teamTemplate.rendererTemplate)
                .build()
            )
            .setPlayerInfo(new PlayerInfo('tmp', 150, 200, teamTemplate.id))
            .build();

    }

    public static createShapeTemplate(material:string):ShapeTemplate {
        return new ShapeTemplateBuilder()
            .setType(p2.Shape.CIRCLE)
            .setDimensions([14])
            .setMaterialId(material)
            .setCollisionGroup(CollisionRule.PLAYER_RULE.collisionGroup)
            .setCollisionMask(CollisionRule.PLAYER_RULE.collisionMask)
            .build();
    }

    public static createDrawableTemplate():DrawableTemplate {
        return new DrawableTemplateBuilder("SimplePhysicObjectDrawable")
            .addRendererTemplate(MockTemplateCreator.createRendererTemplate())
            .build();
    }

    public static createRendererTemplate():RendererTemplate {
        return new RendererTemplate("CircleRenderer", {lineWidth: 4, lineColor: 0xffffffff});
    }

    public static createGoalSensors(size:number[], gatewaySize:number[], ballSize:number):SensorTemplate[] {
        var sensorWidth = gatewaySize[0] - ballSize;
        var sensorShapeBuilder:ShapeTemplateBuilder = new ShapeTemplateBuilder()
            .setType(p2.Shape.RECTANGLE)
            .setDimensions([sensorWidth, gatewaySize[1]])
            .setCollisionGroup(CollisionRule.DEFAULT_GROUP)
            .setCollisionMask(CollisionRule.BALL_GROUP)
            .setIsSensor(true);

        var offset:number = size[0] / 2.0 + ballSize + sensorWidth / 2.0;

        return [
            new SensorTemplateBuilder()
                .setId('redGateway')
                .setEventType(ContactHandlerType.GOAL)
                .setEventArguments(0)
                .setShapeTemplates([sensorShapeBuilder.build()])
                .setPosition([offset, 0])
                .build(),
            new SensorTemplateBuilder()
                .setId('blueGateway')
                .setEventType(ContactHandlerType.GOAL)
                .setEventArguments(1)
                .setShapeTemplates([sensorShapeBuilder.build()])
                .setPosition([-offset, 0])
                .build()
        ]
    }

    public static createBoardTemplate(size:number[], gatewaySize:number[]):BoardTemplate {
        var boardTemplate:BoardTemplate = new BoardTemplate();
        boardTemplate.shapeTemplates = [];

        var cornerHeight = (size[1] - gatewaySize[1]) / 2.0;
        var w2 = size[0] / 2.0;
        var h2 = size[1] / 2.0;

        var g2 = cornerHeight / 2.0;

        var verticalBuilder:ShapeTemplateBuilder = new ShapeTemplateBuilder()
            .setAngle(Math.PI / 2)
            .setType(p2.Shape.LINE)
            .setCollisionGroup(CollisionRule.BOARD_RULE.collisionGroup)
            .setCollisionMask(CollisionRule.BOARD_RULE.collisionMask)
            .setMaterialId('board');

        var horizontalBuilder:ShapeTemplateBuilder = new ShapeTemplateBuilder()
            .setAngle(0)
            .setType(p2.Shape.LINE)
            .setCollisionGroup(CollisionRule.BOARD_RULE.collisionGroup)
            .setCollisionMask(CollisionRule.BOARD_RULE.collisionMask)
            .setMaterialId('board')
            .setDimensions([size[0]]);

        boardTemplate.shapeTemplates.push(horizontalBuilder.setOffset([0, h2]).build());
        boardTemplate.shapeTemplates.push(horizontalBuilder.setOffset([0, -h2]).build());

        verticalBuilder.setDimensions([cornerHeight]);
        var cornerPosition:number = h2 - g2;
        boardTemplate.shapeTemplates.push(verticalBuilder.setOffset([-w2, -cornerPosition]).build());
        boardTemplate.shapeTemplates.push(verticalBuilder.setOffset([-w2, cornerPosition]).build());
        boardTemplate.shapeTemplates.push(verticalBuilder.setOffset([w2, -cornerPosition]).build());
        boardTemplate.shapeTemplates.push(verticalBuilder.setOffset([w2, cornerPosition]).build());

        horizontalBuilder.setDimensions([gatewaySize[0]]);
        var gho:number = gatewaySize[0] / 2;
        boardTemplate.shapeTemplates.push(horizontalBuilder.setOffset([-w2 - gho, -g2]).build());
        boardTemplate.shapeTemplates.push(horizontalBuilder.setOffset([-w2 - gho, g2]).build());
        boardTemplate.shapeTemplates.push(horizontalBuilder.setOffset([w2 + gho, -g2]).build());
        boardTemplate.shapeTemplates.push(horizontalBuilder.setOffset([w2 + gho, g2]).build());

        verticalBuilder.setDimensions([gatewaySize[1]]);
        boardTemplate.shapeTemplates.push(verticalBuilder.setOffset([-w2 - gatewaySize[0], 0]).build());
        boardTemplate.shapeTemplates.push(verticalBuilder.setOffset([w2 + gatewaySize[0], 0]).build());

        var pistilBuilder = new ShapeTemplateBuilder()
            .setType(p2.Shape.CIRCLE)
            .setCollisionGroup(CollisionRule.PISTIL_RULE.collisionGroup)
            .setCollisionMask(CollisionRule.PISTIL_RULE.collisionMask)
            .setDimensions([5])
            .setMaterialId('pistil');

        boardTemplate.shapeTemplates.push(pistilBuilder.setOffset([w2, -g2]).build());
        boardTemplate.shapeTemplates.push(pistilBuilder.setOffset([w2, g2]).build());
        boardTemplate.shapeTemplates.push(pistilBuilder.setOffset([-w2, -g2]).build());
        boardTemplate.shapeTemplates.push(pistilBuilder.setOffset([-w2, g2]).build());

        var planeBuilder:ShapeTemplateBuilder = new ShapeTemplateBuilder()
            .setType(p2.Shape.PLANE)
            .setCollisionGroup(CollisionRule.BOARD_RULE.collisionGroup)
            .setCollisionMask(CollisionRule.BOARD_RULE.collisionMask)
            .setDimensions([])
            .setMaterialId('board');

        boardTemplate.shapeTemplates.push(planeBuilder.setOffset([0, h2]).setAngle(Math.PI).build());//TOP
        boardTemplate.shapeTemplates.push(planeBuilder.setOffset([0, -h2]).setAngle(0).build());//BOTTOM
        boardTemplate.shapeTemplates.push(planeBuilder.setOffset([-w2 - gatewaySize[0], 0]).setAngle(-Math.PI / 2).build());//LEFT
        boardTemplate.shapeTemplates.push(planeBuilder.setOffset([w2 + gatewaySize[0], 0]).setAngle(Math.PI / 2).build());//RIGHT


        return boardTemplate;
    }
}