import {ClientConnectionData} from "./ClientConnectionData";

export class ClientsConnectionMap {
    private connectedClients:{[key: string] : ClientConnectionData} = {};

    public addClient(id:string, connectionData:ClientConnectionData):void {
        console.log("Adding client: " + id);
        this.connectedClients[id] = connectionData;
    }

    public getClient(id:string):ClientConnectionData {
        return this.connectedClients[id];
    }

    public removeClient(id:string):void {
        console.log("Removing client: " + id);
        delete this.connectedClients[id];
    }

    public getConnectionsWithRoom(roomId:string):ClientConnectionData[] {
        var out:ClientConnectionData[] = [];

        for(var id in this.connectedClients){
            var connection:ClientConnectionData = this.connectedClients[id];
            if(connection.roomId == roomId) {
                out.push(connection);
            }
        }

        return out;
    }
}