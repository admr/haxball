import {SocketRequestHandler} from "../../shared/communication/SocketRequestHandler";
import Socket = SocketIO.Socket;
import {ClientState} from "./ClientState";

export interface ServerSocketRequestHandler<T> extends SocketRequestHandler<T> {
    isClientStateAcceptable(clientState:ClientState):boolean;
    isAsync():boolean;
    handleMessageAsync(from:any, msg:any, response:any,socket:T):void;
}

export interface ServerEventHandler extends ServerSocketRequestHandler<Socket> {

}