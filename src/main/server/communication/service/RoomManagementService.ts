import {RoomManager} from "../../gameplay/RoomManager";
import {Messages} from "../../../shared/communication/Messages";
import {ClientConnectionData} from "../ClientConnectionData";
import {Room} from "../../gameplay/Room";
import {ClientState} from "../ClientState";
import {PrePlayingPhase} from "../../gameplay/gamephases/PrePlayingPhase";
import {PhysicObject} from "../../../shared/gameplay/physics/PhysicObject";
import {TeamTemplate} from "../../../shared/gameplay/scene/templates/TeamTemplate";
import {GameDefinitionCreator} from "../../gameplay/GameDefinitionCreator";
import {PhysicObjectTemplate} from "../../../shared/gameplay/scene/templates/PhysicObjectTemplate";
import {RoomConfiguration} from "../../../shared/communication/RoomConfiguration";
import {ClientsConnectionMap} from "../ClientsConnectionMap";
import {PlayerAssignmentInfo} from "../../../shared/communication/PlayerAssignmentInfo";
import {GamePhaseType} from "../../../shared/gameplay/GamePhaseType";

export class RoomManagementService {

    private roomManager:RoomManager;
    private io:SocketIO.Server;
    private gameDefinitionCreator:GameDefinitionCreator;
    private clientsConnectionMap:ClientsConnectionMap;

    constructor(roomManager:RoomManager, gameDefinitionCreator:GameDefinitionCreator,io:SocketIO.Server, clientsConnectionMap:ClientsConnectionMap) {
        this.roomManager = roomManager;
        this.gameDefinitionCreator = gameDefinitionCreator;
        this.io = io;
        this.clientsConnectionMap = clientsConnectionMap;
    }

    public removeClientFromRoom(from:any, socket:SocketIO.Socket, connectionData:ClientConnectionData, room?:Room):void {
        if (connectionData.roomId != null) {
            if(room == null){
              room  = this.roomManager.getRoom(connectionData.roomId);
            }
            if (room != null) {
                room.notifyPlayerDisconnected();
                console.log("Removing user [" + socket.id + "] from room: " + connectionData.roomId);
                room.scene.removeObject(socket.id);
                connectionData.clientState = ClientState.LOGGED_IN;
                connectionData.team = -1;
                if(room.gamePhaseManager.currentPhase.getType() != GamePhaseType.DEFINING_MATCH){
                    this.io.in(connectionData.roomId).emit(Messages.REMOVE_OBJECT, from);
                } else {
                    this.io.in(connectionData.roomId).emit(Messages.ROOM_CONFIG, this.getRoomConfiguration(room));
                }
            }
        }
    }

    public getRoomConfiguration(room:Room):RoomConfiguration {
        var roomConfiguration:RoomConfiguration = room.roomConfiguration;
        roomConfiguration.playerAssignments = this.clientsConnectionMap.getConnectionsWithRoom(room.id)
            .map((connection:ClientConnectionData) => this.createPlayerAssignmentInfo(connection, room));

        return roomConfiguration;
    }

    protected createPlayerAssignmentInfo(connection:ClientConnectionData, room:Room): PlayerAssignmentInfo {
        var info:PlayerAssignmentInfo = new PlayerAssignmentInfo();
        info.clientId = connection.clientId;
        info.playerName = connection.playerName;
        var object:PhysicObject = room.scene.getObject(connection.clientId);
        if(object != null) {
            info.team = object.playerInfo.team;
        }else {
            info.team = connection.team;
        }

        return info;
    }

    public addPlayer(room:Room, teamId:number, from:string):PhysicObjectTemplate {
        var teamTemplate:TeamTemplate = room.gameTemplate.teamTemplates[teamId];
        var result:PhysicObjectTemplate = this.gameDefinitionCreator.createPlayer(from, teamTemplate);
        var playerObject:PhysicObject = room.scene.createObjectFromTemplate(result);
        PrePlayingPhase.arrangePlayersInTeam([playerObject], teamTemplate);
        room.scene.addObject(playerObject);
        return result;
    };
}