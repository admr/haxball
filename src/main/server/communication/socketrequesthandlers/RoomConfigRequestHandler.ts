import {AbstractSecuredEventHandler} from "./AbstractSecuredEventHandler";
import {Messages} from "../../../shared/communication/Messages";
import {Room} from "../../gameplay/Room";
import {ClientsConnectionMap} from "../ClientsConnectionMap";
import {RoomManager} from "../../gameplay/RoomManager";
import {ClientConnectionData} from "../ClientConnectionData";
import {PlayerAssignmentInfo} from "../../../shared/communication/PlayerAssignmentInfo";
import {PhysicObject} from "../../../shared/gameplay/physics/PhysicObject";
import {ClientState} from "../ClientState";
import {RoomConfiguration} from "../../../shared/communication/RoomConfiguration";
import {RoomManagementService} from "../service/RoomManagementService";

export class RoomConfigRequestHandler extends AbstractSecuredEventHandler {

    private roomManagementService:RoomManagementService;

    constructor(clientsConnectionMap:ClientsConnectionMap, roomManager:RoomManager,roomManagementService:RoomManagementService) {
        super(clientsConnectionMap, roomManager);
        this.roomManagementService = roomManagementService;
    }

    doHandle(from:any, msg:any, socket:SocketIO.Socket, room:Room):RoomConfiguration {
        return this.roomManagementService.getRoomConfiguration(room);
    }

    public isClientStateAcceptable(clientState:ClientState):boolean {
        return clientState != ClientState.CONNECTED;
    }

    messageType():string {
        return Messages.ROOM_CONFIG;
    }

}
