import {AbstractSecuredEventHandler} from "./AbstractSecuredEventHandler";
import {Room} from "../../gameplay/Room";
import {Messages} from "../../../shared/communication/Messages";
import {AbstractAction} from "../../../shared/gameplay/actions/AbstractAction";
import {ClientsConnectionMap} from "../ClientsConnectionMap";
import {RoomManager} from "../../gameplay/RoomManager";

export class ActionHandler extends AbstractSecuredEventHandler {

    constructor(clientsConnectionMap:ClientsConnectionMap, roomManager:RoomManager) {
        super(clientsConnectionMap, roomManager);
    }

    doHandle(from:any, actions:AbstractAction[], socket:SocketIO.Socket, room:Room) {
        for (let i in actions) {
            actions[i].caller = from;

            room.serverLoop.actionFacade.addIncomingAction(actions[i]);
        }
    }

    messageType():string {
        return Messages.DO_ACTIONS;
    }

}