import {AbstractSecuredEventHandler} from "./AbstractSecuredEventHandler";
import {Room} from "../../gameplay/Room";
import {Messages} from "../../../shared/communication/Messages";
import {PhysicObject} from "../../../shared/gameplay/physics/PhysicObject";
import {PhysicObjectTemplate} from "../../../shared/gameplay/scene/templates/PhysicObjectTemplate";
import {ClientsConnectionMap} from "../ClientsConnectionMap";
import {RoomManager} from "../../gameplay/RoomManager";
import {GameDefinitionCreator} from "../../gameplay/GameDefinitionCreator";
import {PrePlayingPhase} from "../../gameplay/gamephases/PrePlayingPhase";
import {TeamTemplate} from "../../../shared/gameplay/scene/templates/TeamTemplate";
import {RoomManagementService} from "../service/RoomManagementService";
import {ClientConnectionData} from "../ClientConnectionData";

export class JoinGameRequestHandler extends AbstractSecuredEventHandler {

    private static counter:number = 0;

    constructor(clientsConnectionMap:ClientsConnectionMap, roomManager:RoomManager, private roomManagementService:RoomManagementService) {
        super(clientsConnectionMap, roomManager);
    }

    doHandle(from:any, msg:any, socket:SocketIO.Socket, room:Room) {
        var object:PhysicObject = room.scene.getObject(from);
        var result:PhysicObjectTemplate = null;
        if (object == null) {
            var client:ClientConnectionData = this.clientsConnectionMap.getClient(from);
            if(client != null){
                result = this.roomManagementService.addPlayer(room, client.team, from);
            }
        } else {
            console.log("Player already created...");
        }

        return result;
    }

    messageType():string {
        return Messages.JOIN_GAME;
    }

}