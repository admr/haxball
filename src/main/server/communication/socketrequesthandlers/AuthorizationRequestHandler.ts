import {ClientsConnectionMap} from "../ClientsConnectionMap";
import {ClientConnectionData} from "../ClientConnectionData";
import {Messages} from "../../../shared/communication/Messages";
import {ServerEventHandler} from "../ServerEventHandler";
import {RoomManager} from "../../gameplay/RoomManager";
import {Room} from "../../gameplay/Room";
import {AuthorizationResult} from "../../../shared/communication/AuthorizationResult";
import {ClientState} from "../ClientState";
import {RoomConfigChangeListener} from "../../gameplay/RoomConfigChangeListener";
import {GamePhaseChanged} from "../../../shared/communication/GamePhaseChanged";

export class AuthorizationRequestHandler implements ServerEventHandler {

    private clientsConnectionMap:ClientsConnectionMap;
    private roomManager:RoomManager;
    private roomConfigChangeListener:RoomConfigChangeListener;

    public constructor(clientsConnectionMap:ClientsConnectionMap, roomManager:RoomManager, roomConfigChangeListener:RoomConfigChangeListener) {
        this.clientsConnectionMap = clientsConnectionMap;
        this.roomManager = roomManager;
        this.roomConfigChangeListener = roomConfigChangeListener;
    }

    handleMessage(from:any, msg:any, socket:SocketIO.Socket):any {
        var connectionData:ClientConnectionData = this.clientsConnectionMap.getClient(from);
        //TODO: check authorization
        if (connectionData != null) {
            var room:Room = this.roomManager.getRoom(msg);
            if (room != null) {
                return this.acceptAuthorization(room, connectionData, socket);
            }
        }

        return this.denyAuthorization(from, socket);
    }

    protected acceptAuthorization(room:Room, connectionData:ClientConnectionData, socket:SocketIO.Socket):AuthorizationResult {
        connectionData.roomId = room.id;
        connectionData.clientState = ClientState.AUTHENTICATED_IN_ROOM;
        room.notifyPlayerConnected();

        this.roomConfigChangeListener.onRoomConfigChanged(room);

        socket.join(connectionData.roomId);
        socket.emit(Messages.GAME_PHASE_CHANGE, new GamePhaseChanged(null, room.gamePhaseManager.currentPhase.getType()));
        return new AuthorizationResult(true);
    }

    protected denyAuthorization(from:any, socket:SocketIO.Socket):AuthorizationResult {
        return new AuthorizationResult(false);
    }

    isAsync(): boolean {
        return false;
    }

    handleMessageAsync(from: any, msg: any, response: any, socket: SocketIO.Socket): void {

    }

    isClientStateAcceptable(clientState:ClientState):boolean{
        return clientState == ClientState.LOGGED_IN;
    }

    messageType():string {
        return Messages.AUTHORIZATION;
    }

    isLoggable():boolean {
        return true;
    }
}