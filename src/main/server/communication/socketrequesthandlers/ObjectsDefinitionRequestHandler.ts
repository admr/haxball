import {AbstractSecuredEventHandler} from "./AbstractSecuredEventHandler";
import {Messages} from "../../../shared/communication/Messages";
import {Room} from "../../gameplay/Room";
import {PhysicObjectTemplate} from "../../../shared/gameplay/scene/templates/PhysicObjectTemplate";
import {RoomManager} from "../../gameplay/RoomManager";
import {ClientsConnectionMap} from "../ClientsConnectionMap";

export class ObjectsDefinitionRequestHandler extends AbstractSecuredEventHandler {

    constructor(clientsConnectionMap:ClientsConnectionMap, roomManager:RoomManager) {
        super(clientsConnectionMap, roomManager);
    }

    doHandle(from:any, msg:any, socket:SocketIO.Socket, room:Room):any {
        var ids:string[] = msg.split(",");
        var response:PhysicObjectTemplate[] = [];
        for (let i in ids) {
            var template:PhysicObjectTemplate = room.scene.getObjectTemplate(ids[i]);
            if (template != null) {
                response.push(template);
            }
        }

        return response;
    }

    messageType():string {
        return Messages.OBJECTS_DEFINITION;
    }

}
