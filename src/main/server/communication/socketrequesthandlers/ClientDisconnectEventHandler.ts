import {Room} from "../../gameplay/Room";
import {ClientsConnectionMap} from "../ClientsConnectionMap";
import {RoomManager} from "../../gameplay/RoomManager";
import {ServerEventHandler} from "../ServerEventHandler";
import {ClientConnectionData} from "../ClientConnectionData";
import {Messages} from "../../../shared/communication/Messages";
import {RoomManagementService} from "../service/RoomManagementService";
import {ClientState} from "../ClientState";

export class ClientDisconnectEventHandler implements ServerEventHandler {

    private clientsConnectionMap:ClientsConnectionMap;
    private roomManagementService:RoomManagementService;

    constructor(clientsConnectionMap:ClientsConnectionMap, roomManager:RoomManager, io:SocketIO.Server, roomManagementService:RoomManagementService) {
        this.clientsConnectionMap = clientsConnectionMap;
        this.roomManagementService = roomManagementService;
    }

    handleMessage(from:any, msg:any, socket:SocketIO.Socket) {
        console.log('User ' + socket.id + ' disconnected');
        var connectionData:ClientConnectionData = this.clientsConnectionMap.getClient(socket.id);
        if (connectionData != null) {
            this.clientsConnectionMap.removeClient(socket.id);

            this.roomManagementService.removeClientFromRoom(from, socket, connectionData);
        }
    }

    isAsync(): boolean {
        return false;
    }

    handleMessageAsync(from: any, msg: any, response: any, socket: SocketIO.Socket): void {

    }

    isClientStateAcceptable(clientState:ClientState):boolean{
        return true;
    }

    isLoggable():boolean {
        return true;
    }

    messageType():string {
        return 'disconnect';
    }

}