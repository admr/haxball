import {ServerEventHandler} from "../ServerEventHandler";
import {ClientsConnectionMap} from "../ClientsConnectionMap";
import {ClientConnectionData} from "../ClientConnectionData";
import {Room} from "../../gameplay/Room";
import {RoomManager} from "../../gameplay/RoomManager";
import {ClientState} from "../ClientState";

export abstract class AbstractSecuredEventHandler implements ServerEventHandler {

    protected clientsConnectionMap:ClientsConnectionMap;
    protected roomManager:RoomManager;

    constructor(clientsConnectionMap:ClientsConnectionMap, roomManager:RoomManager) {
        this.clientsConnectionMap = clientsConnectionMap;
        this.roomManager = roomManager;
    }

    handleMessage(from:any, msg:any, socket:SocketIO.Socket):any {
        var connectionData:ClientConnectionData = this.clientsConnectionMap.getClient(from);
        if (connectionData != null) {
            //TODO what to do when there is no associated room?
            var associatedRoom:Room = this.roomManager.getRoom(connectionData.roomId);
            return this.doHandle(from, msg, socket, associatedRoom);
        } else {
            console.log("Unauthorized access: " + from);
        }
    }

    isAsync(): boolean {
        return false;
    }

    handleMessageAsync(from: any, msg: any, response: any, socket: SocketIO.Socket): void {

    }

    public isLoggable():boolean {
        return true;
    }

    public isClientStateAcceptable(clientState:ClientState):boolean{
        return clientState == ClientState.AUTHENTICATED_IN_ROOM;
    }

    public abstract doHandle(from:any, msg:any, socket:SocketIO.Socket, room:Room):any;

    public abstract messageType():string;
}