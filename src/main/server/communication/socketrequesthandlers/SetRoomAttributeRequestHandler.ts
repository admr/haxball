import {AbstractSecuredEventHandler} from "./AbstractSecuredEventHandler";
import {Messages} from "../../../shared/communication/Messages";
import {Room} from "../../gameplay/Room";
import {ClientState} from "../ClientState";
import {ClientsConnectionMap} from "../ClientsConnectionMap";
import {RoomManager} from "../../gameplay/RoomManager";
import {PlayerAssignmentInfo} from "../../../shared/communication/PlayerAssignmentInfo";
import {ClientConnectionData} from "../ClientConnectionData";
import {PhysicObject} from "../../../shared/gameplay/physics/PhysicObject";
import {ServerGamePhaseManager} from "../../gameplay/ServerGamePhaseManager";
import {AbstractGamePhase} from "../../../shared/gameplay/AbstractGamePhase";
import {GamePhaseType} from "../../../shared/gameplay/GamePhaseType";
import {RoomManagementService} from "../service/RoomManagementService";
import {RoomConfigChangeListener} from "../../gameplay/RoomConfigChangeListener";
import {RoomAttributeInfo} from "../../../shared/communication/RoomAttributeInfo";
import {RoomAttributeType} from "../../../shared/communication/RoomAttributeInfo";

export class SetRoomAttributeRequestHandler extends AbstractSecuredEventHandler {

    private roomManagementService:RoomManagementService;
    private io:SocketIO.Server;
    private roomConfigChangeListener:RoomConfigChangeListener;

    constructor(clientsConnectionMap:ClientsConnectionMap, roomManager:RoomManager, roomManagementService:RoomManagementService,
                io:SocketIO.Server, roomConfigChangeListener:RoomConfigChangeListener) {
        super(clientsConnectionMap, roomManager);
        this.roomConfigChangeListener = roomConfigChangeListener;
        this.roomManagementService = roomManagementService;
        this.io = io;
    }

    doHandle(from:any, msg:RoomAttributeInfo, socket:SocketIO.Socket, room:Room):any {

        //TODO check if from has access to do this action..
        switch(msg.type){
            case RoomAttributeType.PLAYER_TEAM:
                this.changePlayerAssignment(<PlayerAssignmentInfo>msg.value, room);
                break;
            case RoomAttributeType.MATCH_TIME:
                this.changeMatchTime(<number>msg.value, room);
                break;
        }

        this.roomConfigChangeListener.onRoomConfigChanged(room);
    }

    protected changeMatchTime(newValue:number,room:Room):void {
        room.roomConfiguration.timeLeft = newValue;
    }

    protected changePlayerAssignment(player:PlayerAssignmentInfo, room:Room):void {
        var client:ClientConnectionData = this.clientsConnectionMap.getClient(player.clientId);
        if (client != null) {
            client.team = player.team;
            var currentPhase:AbstractGamePhase = room.gamePhaseManager.currentPhase;
            if (currentPhase != null) {
                if (this.isPlayingPhase(currentPhase.getType())) {
                    this.doChangePlayerTeam(room, player);
                }
            }
        }
    }

    protected isPlayingPhase(type:GamePhaseType):boolean {
        return type != GamePhaseType.DEFINING_MATCH;
    }

    protected doChangePlayerTeam(room:Room, playerAssignment:PlayerAssignmentInfo):void {
        var object:PhysicObject = room.scene.getObject(playerAssignment.clientId);
        if (object != null && object.playerInfo != null && object.playerInfo.team != playerAssignment.team) {
            this.io.in(room.id).emit(Messages.REMOVE_OBJECT, playerAssignment.clientId);
            room.scene.removeObject(playerAssignment.clientId);
        }

        if (playerAssignment.team != -1) {
            this.roomManagementService.addPlayer(room, playerAssignment.team, playerAssignment.clientId);
        }
    }

    messageType():string {
        return Messages.SET_ROOM_ATTRIBUTE;
    }

    public isClientStateAcceptable(clientState:ClientState):boolean {
        return clientState != ClientState.CONNECTED;
    }
}