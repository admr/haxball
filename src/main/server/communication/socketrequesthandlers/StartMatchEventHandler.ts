import {ServerEventHandler} from "../ServerEventHandler";
import {ClientState} from "../ClientState";
import {Messages} from "../../../shared/communication/Messages";
import {ServerWorldObject} from "../../gameplay/objects/ServerWorldObject";
import {SensorTemplate} from "../../../shared/gameplay/scene/templates/SensorTemplate";
import {Room} from "../../gameplay/Room";
import {AbstractSecuredEventHandler} from "./AbstractSecuredEventHandler";
import {ContactEventFacade} from "../../gameplay/scene/ContactEventFacade";
import {GameDefinitionCreator} from "../../gameplay/GameDefinitionCreator";
import {ClientsConnectionMap} from "../ClientsConnectionMap";
import {RoomManager} from "../../gameplay/RoomManager";
import {GamePhaseType} from "../../../shared/gameplay/GamePhaseType";

export class StartMatchEventHandler extends AbstractSecuredEventHandler {

    private gameDefinitionCreator:GameDefinitionCreator;
    private contactEventFacade:ContactEventFacade;
    private io:SocketIO.Server;

    public constructor(roomManager:RoomManager, clientsConnectionMap:ClientsConnectionMap, gameDefinitionCreator:GameDefinitionCreator,
                       contactEventFacade:ContactEventFacade,io:SocketIO.Server){
        super(clientsConnectionMap,roomManager);
        this.gameDefinitionCreator = gameDefinitionCreator;
        this.contactEventFacade = contactEventFacade;
        this.io = io;
    }

    doHandle(from:any, msg:any, socket:SocketIO.Socket, room:Room):any {
        if(room.gamePhaseManager.currentPhase.getType() == GamePhaseType.DEFINING_MATCH){
            room.startPlaying();
            this.registerSensors(room, 'default');
            room.gamePhaseManager.setPhase(GamePhaseType.PRE_PLAYING);
            this.io.in(room.id).emit(Messages.START_MATCH);
        }
    }

    protected registerSensors(room:Room, map:string):void {
        var sensorTemplates:SensorTemplate[] = this.gameDefinitionCreator.createSensors(map);
        for (let i in sensorTemplates) {
            var obj:ServerWorldObject = room.scene.createFromSensorTemplate(sensorTemplates[i]);
            room.scene.addObject(obj);
            this.contactEventFacade.registerHandlerForPair('goal', 'ball', obj.id, room.scene);
        }
    }

    isClientStateAcceptable(clientState:ClientState):boolean {
        return clientState != ClientState.CONNECTED;
    }

    messageType():string {
        return Messages.START_MATCH;
    }

    isLoggable():boolean {
        return true;
    }

}