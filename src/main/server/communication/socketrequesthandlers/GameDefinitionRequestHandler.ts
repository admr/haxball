import {Messages} from "../../../shared/communication/Messages";
import {ClientsConnectionMap} from "../ClientsConnectionMap";
import {RoomManager} from "../../gameplay/RoomManager";
import {AbstractSecuredEventHandler} from "./AbstractSecuredEventHandler";
import {Room} from "../../gameplay/Room";

export class GameDefinitionRequestHandler extends AbstractSecuredEventHandler {

    public constructor(clientsConnectionMap:ClientsConnectionMap, roomManager:RoomManager) {
        super(clientsConnectionMap, roomManager);
    }

    doHandle(from:any, msg:any, socket:SocketIO.Socket, room:Room) {
        return room.gameTemplate;
    }

    messageType():string {
        return Messages.GAME_DEFINITION;
    }

}