import {ServerEventHandler} from "../ServerEventHandler";
import {Messages} from "../../../shared/communication/Messages";
import {LoginRequest} from "../../../shared/communication/LoginRequest";
import {ClientsConnectionMap} from "../ClientsConnectionMap";
import {ClientConnectionData} from "../ClientConnectionData";
import {ClientState} from "../ClientState";
import {LoginResult} from "../../../shared/communication/LoginResult";
import {UsersRepository} from "../../database/UsersRepository";
import {User} from "../../database/User";
export class LoginRequestHandler implements ServerEventHandler {
    private clientsConnectionMap:ClientsConnectionMap;
    private usersRepository:UsersRepository;

    constructor(clientsConnectionMap:ClientsConnectionMap,usersRepository:UsersRepository) {
        this.clientsConnectionMap = clientsConnectionMap;
        this.usersRepository = usersRepository;
    }

    handleMessage(from:any, msg:any, socket:SocketIO.Socket):any {

    }

    isAsync(): boolean {
        return true;
    }

    handleMessageAsync(from: any, msg: LoginRequest, response: any, socket: SocketIO.Socket): void {
        var connection:ClientConnectionData = this.clientsConnectionMap.getClient(from);
        if(connection != null){
            this.usersRepository.getUserById(msg.playerName).then((document:User) => {
                if(document != null) {
                    console.log(document);
                    if(document.password == msg.password) {
                        connection.clientState = ClientState.LOGGED_IN;
                        connection.playerName = msg.playerName;
                        response(new LoginResult(true, null));
                    } else {
                        response(new LoginResult(false, 'Bad username or password'));
                    }
                } else {
                    response(new LoginResult(false, 'Bad username or password'));
                }
            }, (err) => {
                console.log(err);
                response(new LoginResult(false, err));
            });
        } else {
            response(new LoginResult(false, 'General error'));
        }
    }

    isClientStateAcceptable(clientState:ClientState):boolean {
        return clientState == ClientState.CONNECTED;
    }

    messageType():string {
        return Messages.LOGIN;
    }

    isLoggable():boolean {
        return true;
    }
}