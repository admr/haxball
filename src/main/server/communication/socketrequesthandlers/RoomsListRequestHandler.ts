import {ServerEventHandler} from "../ServerEventHandler";
import {Messages} from "../../../shared/communication/Messages";
import {RoomManager} from "../../gameplay/RoomManager";
import {RoomInfo} from "../../../shared/communication/RoomInfo";
import {ClientState} from "../ClientState";

export class RoomsListRequestHandler implements ServerEventHandler {

    private roomManager:RoomManager;

    constructor(roomManager:RoomManager) {
        this.roomManager = roomManager;
    }

    handleMessage(from:any, msg:any, socket:SocketIO.Socket):any {
        var out:RoomInfo[] = [];
        var roomIds:string[] = this.roomManager.getAllRoomIds();

        for (let id of roomIds) {
            out.push(this.roomManager.getRoom(id).roomInfo);
        }

        return out;
    }

    isAsync(): boolean {
        return false;
    }

    handleMessageAsync(from: any, msg: any, response: any, socket: SocketIO.Socket): void {

    }

    isClientStateAcceptable(clientState:ClientState):boolean{
        return clientState == ClientState.LOGGED_IN;
    }

    messageType():string {
        return Messages.ROOMS_LIST;
    }

    isLoggable():boolean {
        return true;
    }

}