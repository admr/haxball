import {ClientDisconnectEventHandler} from "./ClientDisconnectEventHandler";
import {Messages} from "../../../shared/communication/Messages";
import {ClientsConnectionMap} from "../ClientsConnectionMap";
import {RoomManager} from "../../gameplay/RoomManager";
import {AbstractSecuredEventHandler} from "./AbstractSecuredEventHandler";
import {Room} from "../../gameplay/Room";
import {RoomManagementService} from "../service/RoomManagementService";
import {ClientConnectionData} from "../ClientConnectionData";

export class LeaveRoomEventHandler extends AbstractSecuredEventHandler {

    private roomManagementService:RoomManagementService;

    constructor(clientsConnectionMap:ClientsConnectionMap, roomManager:RoomManager, roomManagementService:RoomManagementService) {
        super(clientsConnectionMap, roomManager);
        this.roomManagementService = roomManagementService;
    }

    doHandle(from:any, msg:any, socket:SocketIO.Socket, room:Room):any {
        var connectionData:ClientConnectionData = this.clientsConnectionMap.getClient(socket.id);
        if (connectionData != null) {
            this.roomManagementService.removeClientFromRoom(from, socket, connectionData, room);
        }
    }

    messageType():string {
        return Messages.LEAVE_GAME;
    }
}