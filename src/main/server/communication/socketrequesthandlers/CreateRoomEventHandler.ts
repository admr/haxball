import {Messages} from "../../../shared/communication/Messages";
import {RoomManager} from "../../gameplay/RoomManager";
import {ClientsConnectionMap} from "../ClientsConnectionMap";
import {ClientConnectionData} from "../ClientConnectionData";
import {Room} from "../../gameplay/Room";
import {ServerEventHandler} from "../ServerEventHandler";
import {GameDefinitionCreator} from "../../gameplay/GameDefinitionCreator";
import {PlayerMoveAction} from "../../../shared/gameplay/actions/PlayerMoveAction";
import {SensorTemplate} from "../../../shared/gameplay/scene/templates/SensorTemplate";
import {ServerWorldObject} from "../../gameplay/objects/ServerWorldObject";
import {ContactEventFacade} from "../../gameplay/scene/ContactEventFacade";
import {CreateRoomResult} from "../../../shared/communication/CreateRoomResult";
import {ClientState} from "../ClientState";
import {GamePhaseType} from "../../../shared/gameplay/GamePhaseType";
var uuid = require("node-uuid");

export class CreateRoomEventHandler implements ServerEventHandler {

    private roomManager:RoomManager;
    private clientsConnectionMap:ClientsConnectionMap;
    private gameDefinitionCreator:GameDefinitionCreator;
    private contactEventFacade:ContactEventFacade;

    public constructor(roomManager:RoomManager, clientsConnectionMap:ClientsConnectionMap, gameDefinitionCreator:GameDefinitionCreator,
                       contactEventFacade:ContactEventFacade) {
        this.roomManager = roomManager;
        this.clientsConnectionMap = clientsConnectionMap;
        this.gameDefinitionCreator = gameDefinitionCreator;
        this.contactEventFacade = contactEventFacade;
    }

    handleMessage(from:any, msg:any, socket:SocketIO.Socket):any {
        var clientConnectionData:ClientConnectionData = this.clientsConnectionMap.getClient(from);
        if (clientConnectionData != null) {
            var newRoomId:string = this.createRoomId();
            console.log("Creating room with id: " + newRoomId);

            if (this.roomManager.getRoom(newRoomId)) {
                console.log("Room already created. Only joining player...");
                socket.emit(Messages.ROOM_CREATED, new CreateRoomResult(null, false));
            } else {
                var room:Room = this.createRoom(newRoomId, msg, clientConnectionData);
                socket.join(newRoomId);
                socket.emit(Messages.ROOM_CREATED, new CreateRoomResult(room.roomInfo, true));
            }

        }
    }

    protected createRoom(roomId:string, roomName:string, clientConnectionData:ClientConnectionData):Room {
        var room:Room = this.roomManager.createRoom(roomId, roomName);
        room.gameTemplate = this.gameDefinitionCreator.createGameDefinition('default');

        console.log('Creating game template:');
        console.log(JSON.stringify(room.gameTemplate));

        clientConnectionData.roomId = roomId;
        room.startRoom();
        //TODO: remove when configuration of room completed

        console.log("Room created: " + roomId);
        room.gamePhaseManager.setPhase(GamePhaseType.DEFINING_MATCH);
        return room;
    }

    isAsync(): boolean {
        return false;
    }

    handleMessageAsync(from: any, msg: any, response: any, socket: SocketIO.Socket): void {

    }

    isClientStateAcceptable(clientState:ClientState):boolean{
        return clientState == ClientState.LOGGED_IN;
    }

    isLoggable():boolean {
        return true;
    }

    protected createRoomId():string {
        return uuid.v4().substr(0, 6);
    }

    messageType():string {
        return Messages.CREATE_ROOM;
    }

}