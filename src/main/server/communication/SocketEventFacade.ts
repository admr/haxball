import {AbstractSocketEventFacade} from "../../shared/communication/AbstractSocketEventFacade";
import {SocketRequestHandler} from "../../shared/communication/SocketRequestHandler";
import {ServerSocketRequestDelegationContext} from "./ServerSocketRequestDelegationContext";
import {ClientsConnectionMap} from "./ClientsConnectionMap";
import {ServerSocketRequestHandler} from "./ServerEventHandler";

export class SocketEventFacade extends AbstractSocketEventFacade<SocketIO.Socket> {

    private clientsConnectionMap:ClientsConnectionMap;

    public constructor(clientsConnectionMap:ClientsConnectionMap) {
        super();
        this.clientsConnectionMap = clientsConnectionMap;
    }

    protected createDelegationContext(handler:ServerSocketRequestHandler<SocketIO.Socket>, from:string, socket:SocketIO.Socket):Function {
        return new ServerSocketRequestDelegationContext(handler, from, socket, this.clientsConnectionMap.getClient(from)).delegate;
    }
}