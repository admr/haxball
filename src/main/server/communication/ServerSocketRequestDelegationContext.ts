import {SocketRequestHandler} from "../../shared/communication/SocketRequestHandler";
import {ServerSocketRequestHandler} from "./ServerEventHandler";
import {ClientConnectionData} from "./ClientConnectionData";
//import Q = require('q');

export class ServerSocketRequestDelegationContext<T> {
    private handler:ServerSocketRequestHandler<T>;
    private clientConnectionData:ClientConnectionData;
    private from:string;
    private socket:T;

    constructor(handler:ServerSocketRequestHandler<T>, from:string, socket:T,clientConnectionData:ClientConnectionData) {
        this.handler = handler;
        this.from = from;
        this.socket = socket;
        this.clientConnectionData = clientConnectionData;
    }

    public delegate = (message:any, response:any) => {
        if (this.handler.isLoggable()) {
            console.log("Handling message [" + this.handler.messageType() + "] from: " + this.from + " <" + JSON.stringify(message) + ">");
        }

        var result=null;
        if(this.handler.isClientStateAcceptable(this.clientConnectionData.clientState)){
            if(this.handler.isAsync()){
                this.handler.handleMessageAsync(this.from, message, response, this.socket);
            } else {
                result = this.handler.handleMessage(this.from, message, this.socket);
            }
        } else {
            result = {"errorMessage": "badClientState"};
        }

        if (result != null && response != null) {
            //Call the client to get response.
            console.log('Sending response: ' + JSON.stringify(result));
            response(result);
        }
    }
}