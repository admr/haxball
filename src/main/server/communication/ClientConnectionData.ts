import {ClientState} from "./ClientState";
export class ClientConnectionData {
    public clientId:string;
    public playerName:string;
    public roomId:string;
    public team:number;
    public clientState:ClientState;

    constructor(clientId:string) {
        this.clientId = clientId;
        this.clientState = ClientState.CONNECTED;
        this.team = -1;
    }
}