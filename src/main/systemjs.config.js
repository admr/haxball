(function (global) {

    // map tells the System loader where to look for things
    var map = {
        'ui/app': 'ui/app', // 'dist',
        'client': 'client',
        'shared': 'shared',
        'rxjs': 'lib/rxjs',
        'forge-di': 'lib/forge-di/build',
        'p2': 'lib/p2/build',
        'underscore': 'lib/underscore',
        'assert': 'lib/assert',
        'util': 'lib/util',
        'inherits': 'lib/inherits',
        '@angular': 'lib/@angular'
    };

    // packages tells the System loader how to load when no filename and/or no extension
    var packages = {
        'ui/app': {main: 'main.js', defaultExtension: 'js'},
        'client': {defaultExtension: 'js'},
        'shared': {defaultExtension: 'js'},
        'rxjs': {defaultExtension: 'js'},
        'forge-di': {main: 'Forge.js', defaultExtension: 'js'},
        'underscore': {main: 'underscore.js', defaultExtension: 'js' },
        'assert': {main: 'assert.js', defaultExtension: 'js' },
        'util': {main: 'util.js', defaultExtension: 'js' },
        'inherits': {main: 'inherits_browser.js', defaultExtension: 'js' },
        'p2': {main:'p2.js', defaultExtension: 'js'}
    };

    var packageNames = [
        '@angular/common',
        '@angular/compiler',
        '@angular/core',
        '@angular/http',
        '@angular/platform-browser',
        '@angular/platform-browser-dynamic',
        '@angular/router',
        '@angular/router-deprecated',
        '@angular/testing',
        '@angular/upgrade'
    ];

    // add package entries for angular packages in the form '@angular/common': { main: 'index.js', defaultExtension: 'js' }
    packageNames.forEach(function (pkgName) {
        packages[pkgName] = {main: 'index.js', defaultExtension: 'js'};
    });

    var config = {
        map: map,
        packages: packages
    };

    // filterSystemConfig - index.html's chance to modify config before we register it.
    if (global.filterSystemConfig) {
        global.filterSystemConfig(config);
    }

    System.config(config);

})(this);