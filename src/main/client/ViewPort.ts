import Point = PIXI.Point;

export class ViewPort {
    private _width:number;
    private _height:number;

    constructor(width:number, height:number) {
        this._width = width;
        this._height = height;
    }

    public static updateContainerForRenderer(container:PIXI.Container, renderer:PIXI.SystemRenderer, scale:Point) {
        container.position.x = renderer.width / 2;
        container.position.y = renderer.height / 2;
        container.scale.x = scale.x;
        container.scale.y = -scale.y;
    }

    public getScaleForViewport(viewPortWidth:number, viewPortHeight:number):Point {
        var newScale:number;
        if (viewPortHeight * this._width / viewPortWidth >= this._height) {
            newScale = viewPortWidth / this._width;
        } else {
            newScale = viewPortHeight / this._height;
        }
        if (newScale > 1) newScale = 1;
        return new Point(newScale, newScale);
    }

    public getRendererSizeForScale(scale:Point):number[] {
        return [scale.x * this._width, scale.y * this._height];
    }

    get width():number {
        return this._width;
    }

    get height():number {
        return this._height;
    }
}