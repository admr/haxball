import {AbstractLoop} from "../../shared/gameplay/AbstractLoop";
import {PhysicsManager} from "../../shared/gameplay/physics/PhysicsManager";
import {ActionFacade} from "../../shared/gameplay/ActionFacade";
import {ClientScene} from "../gameplay/scene/ClientScene";
import {SocketClient} from "../communication/SocketClient";
import {Messages} from "../../shared/communication/Messages";
import {AbstractAction} from "../../shared/gameplay/actions/AbstractAction";
import {ClientDebugOptions} from "../ClientDebugOptions";
import {PhysicObjectSnapshot} from "../../shared/gameplay/snapshots/PhysicObjectSnapshot";
import {PhysicObject} from "../../shared/gameplay/physics/PhysicObject";
import {ClientWorldObject} from "./objects/ClientWorldObject";
import {TimeManager} from "../../shared/utils/TimeManager";
import {GameSnapshotListener} from "./GameSnapshotListener";
import {GamePhaseManager} from "../../shared/gameplay/GamePhaseManager";
export class ClientLoop extends AbstractLoop {

    private socketClient:SocketClient;
    private unknownIds:string[];
    private gameSnapshotListener:GameSnapshotListener;

    constructor(physicsManager:PhysicsManager, actionFacade:ActionFacade, scene:ClientScene, socketClient:SocketClient,
                gamePhaseManager:GamePhaseManager, timeManager:TimeManager) {
        super(physicsManager, actionFacade, scene, gamePhaseManager, timeManager);
        this.socketClient = socketClient;
    }

    public step() {
        this.unknownIds = (<ClientScene>this.scene).applyObjectStates(this.gameSnapshot.physicObjectsSnapshots);
        if(this.gameSnapshotListener){
            this.gameSnapshotListener.onGameSnapshotChange(this.gameSnapshot);
        }
        super.step();
    }

    public registerGameSnapshotListener(gameSnapshotListener:GameSnapshotListener){
        this.gameSnapshotListener = gameSnapshotListener;
    }

    protected updateWorld(timeStep:number):void {
        if (ClientDebugOptions.getParameter("notUpdateInternal") != "true") {
            super.updateWorld(timeStep);
        }
    }

    protected postStep():void {
        var actions:AbstractAction[] = this.actionFacade.getOutgoingActions();
        if (actions.length > 0) {
            this.socketClient.emit(Messages.DO_ACTIONS, actions);
            console.log(JSON.stringify(actions));
        }

        if (this.unknownIds != null && this.unknownIds.length > 0) {
            this.socketClient.request(Messages.OBJECTS_DEFINITION, this.unknownIds.join(","));
        }

        this.unknownIds = [];
        this.gameSnapshot.physicObjectsSnapshots = {};
        super.postStep();
    }

}