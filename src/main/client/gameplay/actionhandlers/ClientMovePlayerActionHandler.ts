import {MovePlayerActionHandler} from "../../../shared/gameplay/actionhandlers/MovePlayerActionHandler";
import {AbstractAction} from "../../../shared/gameplay/actions/AbstractAction";
import {ActionHandlingResult} from "../../../shared/gameplay/ActionHandlingResult";
import {ClientScene} from "../../gameplay/scene/ClientScene";

export class ClientMovePlayerActionHandler extends MovePlayerActionHandler {

    constructor(scene:ClientScene) {
        super(scene);
    }

    handleAction(action:AbstractAction, isInternal:boolean):ActionHandlingResult {
        super.handleAction(action, isInternal);
        return new ActionHandlingResult(action);
    }
}