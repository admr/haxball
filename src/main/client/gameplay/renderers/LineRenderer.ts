import {p2} from "../../../shared/sharedBootstrap";
import {AbstractShapeRenderer} from "./AbstractShapeRenderer";

export class LineRenderer extends AbstractShapeRenderer<p2.Line> {
    draw(graphics:PIXI.Graphics, offset:number[], angle:number) {
        super.applyAttributes(graphics);

        var len = this.shape.length;
        var startPoint = p2.vec2.fromValues(-len / 2, 0);
        var endPoint = p2.vec2.fromValues(len / 2, 0);

        p2.vec2.rotate(startPoint, startPoint, angle);
        p2.vec2.rotate(endPoint, endPoint, angle);

        p2.vec2.add(startPoint, startPoint, offset);
        p2.vec2.add(endPoint, endPoint, offset);

        graphics.moveTo(startPoint[0], startPoint[1]);
        graphics.lineTo(endPoint[0], endPoint[1]);
    }

}