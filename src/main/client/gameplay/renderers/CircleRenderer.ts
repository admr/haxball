import {AbstractShapeRenderer} from "./AbstractShapeRenderer";

export class CircleRenderer extends AbstractShapeRenderer<p2.Circle> {

    draw(graphics:PIXI.Graphics, offset:number[], angle:number) {
        super.applyAttributes(graphics);
        graphics.drawCircle(offset[0], offset[1], this.shape.radius);
    }

}