import {ShapeRenderer} from "./ShapeRenderer";
import {ShapeRendererAttributes} from "../../../shared/gameplay/ShapeRendererAttributes";

export abstract class AbstractShapeRenderer<T extends p2.Shape> implements ShapeRenderer {
    protected shape:T;
    protected attributes:ShapeRendererAttributes;

    public setShape(value:p2.Shape) {
        if (value == null) {
            this.shape = null;
        } else {
            var tmp:T = <T>value;
            if (tmp == null) {
                throw new Error("Expected other type of shape");
            }
            this.shape = tmp;
        }
    }

    public setAttributes(attributes:any):void {
        this.attributes = <ShapeRendererAttributes>attributes;
    }

    public getShape():p2.Shape {
        return this.shape;
    }

    public applyAttributes(graphics:PIXI.Graphics):void {
        //TODO: apply attributes like colors, line width, etc...
        if (this.attributes != null) {
            graphics.lineStyle(this.attributes.lineWidth, this.attributes.lineColor);
        }
    }

    public abstract draw(graphics:PIXI.Graphics, offset:number[], angle:number):void;
}