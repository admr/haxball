export interface ShapeRenderer {
    getShape():p2.Shape;

    setShape(value:p2.Shape):void;

    setAttributes(attributes:any):void;

    draw(graphics:PIXI.Graphics, offset:number[], angle:number):void;
}