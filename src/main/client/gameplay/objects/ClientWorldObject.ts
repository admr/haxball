import {PhysicObjectDrawable} from "./../drawables/PhysicObjectDrawable";
import {PhysicObject} from "../../../shared/gameplay/physics/PhysicObject";

export class ClientWorldObject extends PhysicObject {
    private _drawable:PhysicObjectDrawable;
    private _visible:boolean;

    public draw():void {
        if (this._drawable != null) {
            this._drawable.draw();
        }
    }

    get drawable():PhysicObjectDrawable {
        return this._drawable;
    }

    set drawable(value:PhysicObjectDrawable) {
        this._drawable = value;
        this._drawable.setObject(this);
    }

    get visible():boolean {
        return this._visible;
    }

    set visible(value:boolean) {
        this._visible = value;
    }
}