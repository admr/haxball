import {PhysicObjectDrawable} from "./PhysicObjectDrawable";
import {ClientWorldObject} from "../objects/ClientWorldObject";
import {ShapeRenderer} from "./../renderers/ShapeRenderer";
import {p2} from "../../../shared/sharedBootstrap";

export class SimplePhysicObjectDrawable implements PhysicObjectDrawable {

    protected graphics:PIXI.Graphics;
    protected object:ClientWorldObject;
    private _shapeRenderers:ShapeRenderer[] = [];

    constructor(graphics:PIXI.Graphics) {
        this.graphics = graphics;
    }

    setRenderers(renderers:ShapeRenderer[]):void {
        this._shapeRenderers = renderers;
    }

    protected prepareGraphics():void {
        this.graphics.lineStyle(1, 0xff00ff);
    }

    setObject(object:ClientWorldObject) {
        this.object = object;
        for (var i = 0; i < object.body.shapes.length; ++i) {
            if (i < this._shapeRenderers.length) {
                this._shapeRenderers[i].setShape(object.body.shapes[i]);
            }
        }
    }

    draw() {
        this.prepareGraphics();
        for (var i = 0; i < this.object.body.shapes.length; ++i) {
            if (i < this._shapeRenderers.length) {
                var resultPosition:number[] = [0, 0];
                p2.vec2.add(resultPosition, this.object.body.position, this.object.body.shapeOffsets[i]);

                this._shapeRenderers[i].draw(this.graphics, resultPosition, this.object.body.shapeAngles[i]);
            }
        }
    }

    getDisplayObject():PIXI.DisplayObject {
        return null;
    }

}