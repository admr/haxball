import {LineRenderer} from "./../renderers/LineRenderer";
import {SimplePhysicObjectDrawable} from "./SimplePhysicObjectDrawable";
import {CircleRenderer} from "../renderers/CircleRenderer";

export class BoardDrawable extends SimplePhysicObjectDrawable {
    constructor(graphics:PIXI.Graphics) {
        super(graphics);
        //TODO Completly remove this shit...
        this.setRenderers([
            new LineRenderer(), new LineRenderer(), new LineRenderer(), new LineRenderer(),
            new LineRenderer(), new LineRenderer(), new LineRenderer(), new LineRenderer(),
            new LineRenderer(), new LineRenderer(), new LineRenderer(), new LineRenderer(),
            new CircleRenderer(), new CircleRenderer(), new CircleRenderer(), new CircleRenderer()]
        );
    }

    protected prepareGraphics():void {
        this.graphics.lineStyle(3, 0xffffffff);
    }
}