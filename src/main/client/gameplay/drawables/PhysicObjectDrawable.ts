import {Drawable} from "./Drawable";
import {ClientWorldObject} from "./../objects/ClientWorldObject";

export interface PhysicObjectDrawable extends Drawable {
    setObject(object:ClientWorldObject);
    draw();
}