import {CONTAINER} from "../../../shared/DI";
import {BoardDrawable} from "./BoardDrawable";
import {SimplePhysicObjectDrawable} from "./SimplePhysicObjectDrawable";
import {ShapeRenderer} from "../renderers/ShapeRenderer";
import {CircleRenderer} from "../renderers/CircleRenderer";
import {LineRenderer} from "../renderers/LineRenderer";
import {DrawableTemplate} from "../../../shared/gameplay/scene/templates/DrawableTemplate";
import {Drawable} from "./Drawable";
import {RendererTemplate} from "../../../shared/gameplay/scene/templates/RendererTemplate";

export class DrawableFactory {
    public createDrawable<T>(name:string):T {
        return CONTAINER.get<T>(name);
    }

    public createRenderer(name:string):ShapeRenderer {
        return CONTAINER.get<ShapeRenderer>(name);
    }

    public drawableFromTemplate<T extends Drawable>(template:DrawableTemplate):T {
        var drawable:T = this.createDrawable<T>(template.drawableName);
        var renderers:ShapeRenderer[] = [];
        for (let i in template.rendererTemplates) {
            renderers.push(this.rendererFromTemplate(template.rendererTemplates[i]));
        }

        drawable.setRenderers(renderers);

        return drawable;
    }

    public rendererFromTemplate(template:RendererTemplate):ShapeRenderer {
        var renderer:ShapeRenderer = this.createRenderer(template.rendererName);
        renderer.setAttributes(template.rendererAttributes);
        return renderer;
    }
}

export function createDrawableBootstrap() {
    CONTAINER.bind("BoardDrawable").to.type(BoardDrawable).as.transient();
    CONTAINER.bind("SimplePhysicObjectDrawable").to.type(SimplePhysicObjectDrawable).as.transient();

    CONTAINER.bind("CircleRenderer").to.type(CircleRenderer).as.transient();
    CONTAINER.bind("LineRenderer").to.type(LineRenderer).as.transient();
}