import {ShapeRenderer} from "../renderers/ShapeRenderer";

export interface Drawable {
    getDisplayObject():PIXI.DisplayObject;
    setRenderers(renderers:ShapeRenderer[]):void;
}