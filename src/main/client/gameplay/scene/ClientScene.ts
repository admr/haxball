import {PhysicsManager} from "../../../shared/gameplay/physics/PhysicsManager";
import {BoardTemplate} from "./../../../shared/gameplay/scene/templates/BoardTemplate";
import {ClientWorldObject} from "../../gameplay/objects/ClientWorldObject";
import {PhysicObjectTemplate} from "../../../shared/gameplay/scene/templates/PhysicObjectTemplate";
import {DrawableFactory} from "../../gameplay/drawables/DrawableFactory";
import {p2} from '../../../shared/sharedBootstrap';
import {BoardDrawable} from "../../gameplay/drawables/BoardDrawable";
import {AbstractScene} from "../../../shared/gameplay/scene/AbstractScene";
import {PhysicObject} from "../../../shared/gameplay/physics/PhysicObject";
import {GameTemplate} from "../../../shared/gameplay/scene/templates/GameTemplate";
import {PhysicObjectDrawable} from "../../gameplay/drawables/PhysicObjectDrawable";
import {PhysicObjectSnapshot} from "../../../shared/gameplay/snapshots/PhysicObjectSnapshot";
import {SnapshotManager} from "../../../shared/gameplay/snapshots/SnapshotManager";
import {ClientDebugOptions} from "../../ClientDebugOptions";
import {SensorTemplate} from "../../../shared/gameplay/scene/templates/SensorTemplate";
import {Map} from "../../../shared/utils/Map";

export class ClientScene extends AbstractScene {

    protected objects:{[id:string] : ClientWorldObject} = {};
    private drawableFactory:DrawableFactory;
    private mainStage:PIXI.Container;
    private graphics:PIXI.Graphics;
    private snapshotManager:SnapshotManager;

    public constructor(physicsManager:PhysicsManager, drawableFactory:DrawableFactory, mainStage:PIXI.Container, graphics:PIXI.Graphics,
                       snapshotManager:SnapshotManager) {
        super(physicsManager);
        this.drawableFactory = drawableFactory;
        this.mainStage = mainStage;
        this.graphics = graphics;
        this.snapshotManager = snapshotManager;

        this.mainStage.addChild(this.graphics);
    }

    public createGameWorld(template:GameTemplate):void {
        super.createGameWorld(template);
    }

    public applyObjectStates(states:Map<PhysicObjectSnapshot>):string[] {
        var out:string[] = [];
        if (this.gameCreated) {
            for (let objId in states) {
                var clientWorldObject:ClientWorldObject = this.objects[objId];
                if (clientWorldObject != null) {
                    if (ClientDebugOptions.getParameter('acceptServerState') != 'false') {
                        this.snapshotManager.applyStateOnPhysicObject(states[objId], clientWorldObject);
                    }
                } else {
                    out.push(objId);
                }
            }
        }

        return out;
    }

    public createFromBoardTemplate(template:BoardTemplate):ClientWorldObject {
        var out:ClientWorldObject = new ClientWorldObject();
        super.applyBoardTemplate(out, template);
        out.drawable = this.drawableFactory.createDrawable<BoardDrawable>("BoardDrawable");

        return out;
    }

    public createObjectFromTemplate(template:PhysicObjectTemplate):ClientWorldObject {
        var out:ClientWorldObject = new ClientWorldObject();
        super.applyObjectTemplate(out, template);
        out.drawable = this.drawableFactory.drawableFromTemplate<PhysicObjectDrawable>(template.drawable);
        return out;
    }

    public createFromSensorTemplate(template:SensorTemplate):ClientWorldObject {
        var out:ClientWorldObject = new ClientWorldObject();
        super.applySensorTemplate(out, template);
        return out;
    }

    public draw() {
        this.graphics.clear();
        for (let id in this.objects) {
            this.objects[id].draw();
        }
    }

    public addObject(object:PhysicObject):boolean {
        var result:boolean = super.addObject(object);

        if (result && object instanceof ClientWorldObject) {
            if (object.drawable != null) {
                var displayObject = object.drawable.getDisplayObject();
                if (displayObject != null) {
                    this.addDisplayObject(displayObject);
                }
            }
        }

        return result;
    }

    public addDisplayObject(object:PIXI.DisplayObject):void {
        this.mainStage.addChild(object);
    }

    public removeObject(id:string):boolean {
        var obj:ClientWorldObject = <ClientWorldObject>this.getObject(id);
        var out:boolean = super.removeObject(id);

        if (out && obj != null && obj.drawable != null) {
            var displayObject = obj.drawable.getDisplayObject();
            if (displayObject != null) {
                this.mainStage.removeChild(displayObject);
            }
        }

        return out;
    }
}

