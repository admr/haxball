import {GameSnapshot} from "../../shared/gameplay/snapshots/GameSnapshot";
export interface GameSnapshotListener {
    onGameSnapshotChange(gameSnapshot:GameSnapshot);
}