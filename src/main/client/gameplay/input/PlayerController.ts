import {p2} from "../../../shared/sharedBootstrap";
import {ClientWorldObject} from "../objects/ClientWorldObject";
import {ActionFacade} from "../../../shared/gameplay/ActionFacade";
import {PlayerMoveAction} from "../../../shared/gameplay/actions/PlayerMoveAction";
import {SocketClient} from "../../communication/SocketClient";
import {AbstractAction} from "../../../shared/gameplay/actions/AbstractAction";
import {NoArgAction} from "../../../shared/gameplay/actions/NoArgAction";
import {ActionTypes} from "../../../shared/gameplay/actions/ActionTypes";

export class PlayerController {

    public static LEFT_ARROW:number = 37;
    public static UP_ARROW:number = 38;
    public static RIGHT_ARROW:number = 39;
    public static DOWN_ARROW:number = 40;
    public static KICK:number = 88;//x

    public static ESC:number = 27;

    private _playerObject:ClientWorldObject;
    private directionVector:number[] = [0, 0];
    private previousDirection:number[] = [0, 0];
    private actionFacade:ActionFacade;
    private socketClient:SocketClient;

    public constructor(actionFacade:ActionFacade, socketClient:SocketClient) {
        this.actionFacade = actionFacade;
        this.socketClient = socketClient;
    }

    get playerObject():ClientWorldObject {
        return this._playerObject;
    }

    set playerObject(value:ClientWorldObject) {
        this._playerObject = value;
    }

    protected updatePlayerDirection():void {
        if (this.previousDirection[0] != this.directionVector[0] || this.previousDirection[1] != this.directionVector[1]) {
            var moveAction:PlayerMoveAction = new PlayerMoveAction();
            moveAction.caller = this.getCallerId();
            if (this.directionVector[0] == 0 && this.directionVector[1] == 0) {
                moveAction.direction = null;
            } else {
                moveAction.direction = this.directionVector;
            }

            this.actionFacade.addIncomingAction(moveAction);
        }
    }


    protected kickBall():void {
        var kickAction:AbstractAction = new NoArgAction(ActionTypes.KICK_BALL);
        kickAction.caller = this.getCallerId();

        this.actionFacade.addIncomingAction(kickAction);
    }

    public registerEvents(document:Document) {
        document.addEventListener('keydown', (e:KeyboardEvent) => {
            this.previousDirection[0] = this.directionVector[0];
            this.previousDirection[1] = this.directionVector[1];
            console.log('Got keydown event: ' + e.keyCode);
            switch (e.keyCode) {
                case PlayerController.LEFT_ARROW:
                    this.directionVector[0] = -1;
                    break;
                case PlayerController.UP_ARROW:
                    this.directionVector[1] = 1;
                    break;
                case PlayerController.RIGHT_ARROW:
                    this.directionVector[0] = 1;
                    break;
                case PlayerController.DOWN_ARROW:
                    this.directionVector[1] = -1;
                    break;
                case PlayerController.KICK:
                    this.kickBall();
                    break;
            }
            this.updatePlayerDirection();
        });

        document.addEventListener('keyup', (e:KeyboardEvent) => {
            console.log('Got keyup event: ' + e.keyCode);
            this.previousDirection[0] = this.directionVector[0];
            this.previousDirection[1] = this.directionVector[1];
            switch (e.keyCode) {
                case PlayerController.LEFT_ARROW:
                    this.directionVector[0] = 0;
                    break;
                case PlayerController.UP_ARROW:
                    this.directionVector[1] = 0;
                    break;
                case PlayerController.RIGHT_ARROW:
                    this.directionVector[0] = 0;
                    break;
                case PlayerController.DOWN_ARROW:
                    this.directionVector[1] = 0;
                    break;
            }

            this.updatePlayerDirection();
        });
    }

    private getCallerId() {
        return '/#' + this.socketClient.socket.id;
    };
}