import Resource = PIXI.loaders.Resource;
import Texture = PIXI.Texture;
export class ResourceManager {

    public constructor() {

    }

    public loadResources(resources:{[key: string] : string}, fn:Function, context?:any) {
        for (var key in  resources) {
            PIXI.loader.add(key, resources[key]);
        }

        PIXI.loader.once('complete', fn, context);
        PIXI.loader.load();
    }

    public getResource(name:string):Resource {
        return PIXI.loader.resources[name];
    }

    public getTexture(name:string):Texture {
        var resource:Resource = this.getResource(name);
        if (resource != null) {
            return resource.texture;
        }

        return null;
    }
}