import {SocketRequestHandler} from "../../shared/communication/SocketRequestHandler";

export interface ClientSocketRequestHandler extends SocketRequestHandler<SocketIOClient.Socket> {

}