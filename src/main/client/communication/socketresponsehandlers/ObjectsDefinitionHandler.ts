import {ClientScene} from "../../gameplay/scene/ClientScene";
import {PhysicObjectTemplate} from "../../../shared/gameplay/scene/templates/PhysicObjectTemplate";
import {PhysicObject} from "../../../shared/gameplay/physics/PhysicObject";
import {Messages} from "../../../shared/communication/Messages";
import {ClientSocketResponseHandler} from "../ClientSocketResponseHandler";
import {SocketClient} from "../SocketClient";

export class ObjectsDefinitionHandler implements ClientSocketResponseHandler {

    protected scene:ClientScene;

    constructor(scene:ClientScene) {
        this.scene = scene;
    }

    handleResponse(result:any, context:SocketClient):void {
        var definitions:PhysicObjectTemplate[] = <PhysicObjectTemplate[]>result;
        if (definitions != null) {
            for (let i in definitions) {
                var playerObject:PhysicObject = this.scene.createObjectFromTemplate(definitions[i]);
                this.scene.addObject(playerObject);
            }
        }
    }

    getRequestType():string {
        return Messages.OBJECTS_DEFINITION;
    }
}