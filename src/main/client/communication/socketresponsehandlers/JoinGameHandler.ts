import {ClientSocketRequestHandler} from "../ClientSocketRequestHandler";
import {Messages} from "../../../shared/communication/Messages";
import {ClientScene} from "../../gameplay/scene/ClientScene";
import {ClientSocketResponseHandler} from "../ClientSocketResponseHandler";
import {SocketClient} from "../SocketClient";

export class JoinGameHandler implements ClientSocketResponseHandler {

    private scene:ClientScene;

    constructor(scene:ClientScene) {
        this.scene = scene;
    }

    handleResponse(result:any, context:SocketClient):void {
        //TODO: Some kind of message box for client?
    }

    getRequestType():string {
        return Messages.JOIN_GAME;
    }
}