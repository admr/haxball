import {ClientSocketRequestHandler} from "../ClientSocketRequestHandler";
import {Messages} from "../../../shared/communication/Messages";
import {ClientScene} from "../../gameplay/scene/ClientScene";
import {GameTemplate} from "../../../shared/gameplay/scene/templates/GameTemplate";
import {PlayerController} from "../../gameplay/input/PlayerController";
import {ClientDebugOptions} from "../../ClientDebugOptions";
import {SocketResponseHandler} from "../../../shared/communication/SocketResponseHandler";
import {SocketClient} from "../SocketClient";
import {ClientSocketResponseHandler} from "../ClientSocketResponseHandler";

export class GameDefinitionHandler implements ClientSocketResponseHandler {

    private scene:ClientScene;

    constructor(scene:ClientScene) {
        this.scene = scene;
    }

    handleResponse(result:any, socket:SocketClient):void {
        var gameTemplate:GameTemplate = <GameTemplate>result;
        if (gameTemplate != null) {
            this.scene.createGameWorld(gameTemplate);
        }
    }

    getRequestType():string {
        return Messages.GAME_DEFINITION;
    }
}