import {isArray} from "util";
import {SocketResponseHandler} from "../../shared/communication/SocketResponseHandler";
import {SocketResponseDelegationContext} from "../../shared/communication/SocketResponseDelegationContext";
import {ClientSocketResponseHandler} from "./ClientSocketResponseHandler";

export class SocketClient {

    private socketResponseHandlers:{[type:string]: ClientSocketResponseHandler} = {};
    public socket:SocketIOClient.Socket;

    private registerResponseHandlers(responseHandlers:ClientSocketResponseHandler[]) {
        for (let handler of responseHandlers) {
            this.socketResponseHandlers[handler.getRequestType()] = handler;
        }
    }

    constructor(socketResponseHandlers:any) {
        if (isArray(socketResponseHandlers)) {
            this.registerResponseHandlers(socketResponseHandlers);
        } else {
            this.registerResponseHandlers([socketResponseHandlers]);
        }
    }

    public connect(address:string) {
        this.socket = io();
    }

    public emit(event:string, data:any):SocketIOClient.Socket {
        return this.socket.emit(event, data);
    }

    public request(event:string, data:any = null, callbackHandler:Function = null):SocketIOClient.Socket {
        console.log('Sending [' + event + '] <' + data + '>');
        if (callbackHandler != null) {
            return this.requestWithCallbackHandler(event, data, callbackHandler);
        } else {
            return this.requestWithDefaultCallbackHandler(event, data);
        }
    }

    protected requestWithDefaultCallbackHandler(event:string, data:any):SocketIOClient.Socket {
        var handler:ClientSocketResponseHandler = this.socketResponseHandlers[event];
        if (handler != null) {
            return this.socket.emit(event, data, new SocketResponseDelegationContext<SocketClient>(handler, this).delegate);
        } else {
            throw new Error("No response handler for: " + event);
        }
    }

    protected requestWithCallbackHandler(event:string, data:any, callback:Function):SocketIOClient.Socket {
        return this.socket.emit(event, data, callback);
    }
}