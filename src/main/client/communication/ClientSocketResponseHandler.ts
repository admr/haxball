import {SocketResponseHandler} from "../../shared/communication/SocketResponseHandler";
import {SocketClient} from "./SocketClient";

export interface ClientSocketResponseHandler extends SocketResponseHandler<SocketClient> {

}