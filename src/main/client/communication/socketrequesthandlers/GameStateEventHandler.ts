import {ClientSocketRequestHandler} from "../ClientSocketRequestHandler";
import {Messages} from "../../../shared/communication/Messages";
import {ClientScene} from "../../gameplay/scene/ClientScene";
import {GameSnapshot} from "../../../shared/gameplay/snapshots/GameSnapshot";
import {ClientLoop} from "../../gameplay/ClientLoop";

export class GameStateEventHandler implements ClientSocketRequestHandler {

    private clientLoop:ClientLoop;

    public constructor(clientLoop:ClientLoop) {
        this.clientLoop = clientLoop;
    }

    handleMessage(from:any, msg:any, socket:SocketIOClient.Socket) {
        var gameState:GameSnapshot = <GameSnapshot>msg;
        if (gameState != null) {
            this.clientLoop.gameSnapshot = gameState;
        }
    }

    messageType():string {
        return Messages.GAME_STATE;
    }

    isLoggable():boolean {
        return false;
    }
}