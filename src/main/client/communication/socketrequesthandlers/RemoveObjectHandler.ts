import {ClientSocketRequestHandler} from "../ClientSocketRequestHandler";
import {Messages} from "../../../shared/communication/Messages";
import {ClientScene} from "../../gameplay/scene/ClientScene";

export class RemoveObjectHandler implements ClientSocketRequestHandler {

    private scene:ClientScene;

    constructor(scene:ClientScene) {
        this.scene = scene;
    }

    handleMessage(from:any, msg:any, socket:SocketIOClient.Socket) {
        var ids:string[] = msg.split(",");
        for (let i in ids) {
            this.scene.removeObject(ids[i]);
        }
    }

    messageType():string {
        return Messages.REMOVE_OBJECT;
    }

    isLoggable():boolean {
        return true;
    }

}