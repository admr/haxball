import {ClientSocketRequestHandler} from "./ClientSocketRequestHandler";
import {AbstractSocketEventFacade} from "../../shared/communication/AbstractSocketEventFacade";
import {SocketClient} from "./SocketClient";
import {CONTAINER} from "../../shared/DI";

export class SocketEventFacade extends AbstractSocketEventFacade<SocketIOClient.Socket> {

    protected registered:{[key: string] : ClientSocketRequestHandler} = {};

    public constructor(eventHandlers:ClientSocketRequestHandler) {
        super();
        for (var i in eventHandlers) {
            this.addEventHandler(eventHandlers[i]);
        }
    }

    public mockMessage(msg:any, type:string):void {
        var eventHandler:ClientSocketRequestHandler = this.registered[type];
        if (eventHandler != null) {
            var socketClient:SocketClient = CONTAINER.get<SocketClient>("socketClient");
            eventHandler.handleMessage('mock', msg, socketClient.socket);
        }
    }

}