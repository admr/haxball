import {ClientLoop} from "./gameplay/ClientLoop";
import {SocketClient} from "./communication/SocketClient";
import {PhysicsManager} from "../shared/gameplay/physics/PhysicsManager"
import {SocketEventFacade} from "./communication/SocketEventFacade";
import {Messages} from "../shared/communication/Messages";
import {ClientScene} from "./gameplay/scene/ClientScene";
import {ViewPort} from "./ViewPort";
import Point = PIXI.Point;

export class Game {

    private renderer:PIXI.SystemRenderer;
    private mainStage:PIXI.Container;
    private clientLoop:ClientLoop;
    private physicsManager:PhysicsManager;
    private scene:ClientScene;
    private viewPort:ViewPort;

    private playing:boolean = false;

    constructor(renderer:PIXI.SystemRenderer,
                mainStage:PIXI.Container,
                clientLoop:ClientLoop,
                physicsManager:PhysicsManager,
                scene:ClientScene,
                viewPort:ViewPort) {
        this.renderer = renderer;
        this.mainStage = mainStage;
        this.clientLoop = clientLoop;
        this.physicsManager = physicsManager;
        this.scene = scene;
        this.viewPort = viewPort;
    }

    public startGame():void {
        this.physicsManager.initWorld();
        this.playing = true;
        this.startLoop();
    }

    protected startLoop() {
        if(this.playing){
            requestAnimationFrame(() => this.startLoop());
        }
        this.renderer.render(this.mainStage);
        this.clientLoop.step();
        this.scene.draw();
    }

    public stopGame(): void {
        this.playing = false;
        this.scene.clear();
    }

    public updateViewPort(width:number, height:number):void {
        var scale:Point = this.viewPort.getScaleForViewport(width, height);
        var toResize:number[] = this.viewPort.getRendererSizeForScale(scale);
        this.renderer.resize(toResize[0], toResize[1]);
        ViewPort.updateContainerForRenderer(this.mainStage, this.renderer, scale);
    }
}