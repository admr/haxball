import {CONTAINER} from "../shared/DI";
import {ResourceManager} from "./ResourceManager";
import {ClientLoop} from "./gameplay/ClientLoop";
import {Game} from "./Game";
import {createDrawableBootstrap} from "./gameplay/drawables/DrawableFactory";
import {DrawableFactory} from "./gameplay/drawables/DrawableFactory";
import {PlayerController} from "./gameplay/input/PlayerController";
import {TestActionHandler} from "../shared/gameplay/actionhandlers/TestActionHandler";
import {ClientMovePlayerActionHandler} from "./gameplay/actionhandlers/ClientMovePlayerActionHandler";
import {KickBallActionHandler} from "../shared/gameplay/actionhandlers/KickBallActionHandler";
import {ViewPort} from "./ViewPort";
import Point = PIXI.Point;
import {GameStateEventHandler} from "./communication/socketrequesthandlers/GameStateEventHandler";
import {RemoveObjectHandler} from "./communication/socketrequesthandlers/RemoveObjectHandler";
import {GameDefinitionHandler} from "./communication/socketresponsehandlers/GameDefinitionHandler";
import {JoinGameHandler} from "./communication/socketresponsehandlers/JoinGameHandler";
import {ObjectsDefinitionHandler} from "./communication/socketresponsehandlers/ObjectsDefinitionHandler";
import {SocketClient} from "./communication/SocketClient";
import {SocketEventFacade} from "./communication/SocketEventFacade";
import {ClientScene} from "./gameplay/scene/ClientScene";
import {GamePhaseManager} from "../shared/gameplay/GamePhaseManager";

function createEventHandlers() {
    CONTAINER.bind("eventHandlers").to.type(GameStateEventHandler).as.singleton();
    CONTAINER.bind("eventHandlers").to.type(RemoveObjectHandler).as.singleton();
}

function createSocketResponseHandlers() {
    CONTAINER.bind("socketResponseHandlers").to.type(GameDefinitionHandler).as.singleton();
    CONTAINER.bind("socketResponseHandlers").to.type(JoinGameHandler).as.singleton();
    CONTAINER.bind("socketResponseHandlers").to.type(ObjectsDefinitionHandler).as.singleton();
}

function createActionHandlers() {
    CONTAINER.bind("actionHandlers").to.type(ClientMovePlayerActionHandler).as.singleton();
    CONTAINER.bind("actionHandlers").to.type(KickBallActionHandler).as.singleton();
    CONTAINER.bind("actionHandlers").to.type(TestActionHandler).as.singleton();
}

function createPIXIBootstrap(width:number, height:number) {
    var viewPort:ViewPort = new ViewPort(1200, 800);
    var scale:Point = viewPort.getScaleForViewport(width, height);
    var rendererSize:number[] = viewPort.getRendererSizeForScale(scale);
    var renderer:PIXI.SystemRenderer = PIXI.autoDetectRenderer(rendererSize[0], rendererSize[1], {
        backgroundColor: 0xff129E36,
        antialias: true
    });
    var container:PIXI.Container = new PIXI.Container();
    ViewPort.updateContainerForRenderer(container, renderer, scale);

    renderer.view.setAttribute("style", "display: block; margin: 0 auto;");

    CONTAINER.bind("viewPort").as.instance(viewPort);
    CONTAINER.bind("renderer").as.instance(renderer);
    CONTAINER.bind("mainStage").to.type(PIXI.Container).as.instance(container);
    CONTAINER.bind("graphics").to.type(PIXI.Graphics).as.singleton();
}

export function createBootstrap(width:number, height:number) {
    createPIXIBootstrap(width, height);
    CONTAINER.bind("game").to.type(Game).as.singleton();
    CONTAINER.bind("socketClient").to.type(SocketClient).as.singleton();
    CONTAINER.bind("socketEventFacade").to.type(SocketEventFacade).as.singleton();
    CONTAINER.bind("resourceManager").to.type(ResourceManager).as.singleton();
    CONTAINER.bind("scene").to.type(ClientScene).as.singleton();
    CONTAINER.bind("clientLoop").to.type(ClientLoop).as.singleton();
    CONTAINER.bind("drawableFactory").to.type(DrawableFactory).as.singleton();
    CONTAINER.bind("playerController").to.type(PlayerController).as.singleton();
    CONTAINER.bind("gamePhaseManager").to.type(GamePhaseManager);
    createEventHandlers();
    createSocketResponseHandlers();
    createActionHandlers();
    createDrawableBootstrap();
}