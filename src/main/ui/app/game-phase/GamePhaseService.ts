import {Injectable,OnDestroy} from "@angular/core";
import {Subject} from "rxjs/Rx";
import {ClientSocketRequestHandler} from "../../../client/communication/ClientSocketRequestHandler";
import {Messages} from "../../../shared/communication/Messages";
import {CONTAINER} from "../../../shared/DI";
import {SocketEventFacade} from "../../../client/communication/SocketEventFacade";
import {GamePhaseChanged} from "../../../shared/communication/GamePhaseChanged";

@Injectable()
export class GamePhaseService implements ClientSocketRequestHandler, OnDestroy{

    public phaseChange:Subject<GamePhaseChanged> = new Subject<GamePhaseChanged>();

    handleMessage(from:any, msg:any, socket:SocketIOClient.Socket):any {
        this.phaseChange.next(msg);
        console.log("Received GAME_PHASE_CHANGE");
    }

    messageType():string {
        return Messages.GAME_PHASE_CHANGE;
    }

    isLoggable():boolean {
        return true;
    }

    ngOnDestroy():any {
        this.phaseChange.unsubscribe();
    }

    public init():void{
        CONTAINER.get<SocketEventFacade>("socketEventFacade").addEventHandler(this);
    }
}