//FIXME: Note because of Bug in angular this component will not working for now...
import { Router, RouterOutlet, ComponentInstruction } from '@angular/router-deprecated';
import { ViewContainerRef, DynamicComponentLoader, Directive, Attribute,Inject, OnDestroy, ElementRef} from '@angular/core';
import {UserService} from "../components/user/UserService";

@Directive({
    selector: 'router-outlet'
})
export class LoggedOutlet extends RouterOutlet {

    private routerOutlet:RouterOutlet;
    private parentRouter:Router;
    private userService:UserService;

    constructor(@Inject(UserService) userService:UserService, elementRef:ViewContainerRef, @Inject(DynamicComponentLoader)loader:DynamicComponentLoader, @Inject(Router)parentRouter:Router) {
        super(elementRef, loader, parentRouter, null);
        this.userService = userService;
        this.parentRouter = parentRouter;
    }

    activate(instruction:ComponentInstruction) {
        if (this.canActivate(instruction.urlPath)) {
            return this.routerOutlet.activate(instruction);
        }

        this.parentRouter.navigate(['Login']);
    }

    protected canActivate(url):boolean {
        return this.userService.isLoggedIn;
    }
}