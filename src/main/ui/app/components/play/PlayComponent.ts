import {Component,OnInit,OnDestroy,Inject,ViewChild } from "@angular/core";
import {RouteParams, Router} from "@angular/router-deprecated";
import {SocketClient} from "../../../../client/communication/SocketClient";
import {CONTAINER} from "../../../../shared/DI";
import {Messages} from "../../../../shared/communication/Messages";
import {SocketService} from "../socket/SocketService";
import {GameScoreComponent} from "../game-score/GameScoreComponent";
import {AuthorizationResult} from "../../../../shared/communication/AuthorizationResult";
import {RoomConfigComponent} from "../room-config/RoomConfigComponent";
import {SecuredComponent} from "../user/SecuredComponent";
import {UserService} from "../user/UserService";
import {ClientService} from "../../client/ClientService";
import {ClientSocketRequestHandler} from "../../../../client/communication/ClientSocketRequestHandler";
import {GamePhaseService} from "../../game-phase/GamePhaseService";
import {GamePhaseChanged} from "../../../../shared/communication/GamePhaseChanged";

@Component({
    templateUrl: 'ui/app/components/play/play.html',
    directives: [GameScoreComponent, RoomConfigComponent]
})
export class PlayComponent extends SecuredComponent implements OnInit,OnDestroy,ClientSocketRequestHandler  {
    @ViewChild(RoomConfigComponent)
    private teamSelectComponent:RoomConfigComponent;
    private roomId:string;

    constructor(@Inject(RouteParams)params:RouteParams,@Inject(SocketService)private socketService:SocketService,
                @Inject(Router) router:Router, @Inject(UserService) userService:UserService,@Inject(ClientService)private clientService:ClientService,
                @Inject(GamePhaseService)private gamePhaseService:GamePhaseService) {
        super(router, userService);
        this.roomId = params.get('roomId');
    }

    handleMessage(from:any, msg:any, socket:SocketIOClient.Socket):any {
        this.socketService.emit(Messages.JOIN_GAME);
        this.teamSelectComponent.close();
    }

    messageType():string {
        return Messages.START_MATCH;
    }

    isLoggable():boolean {
        return true;
    }

    ngOnInit():any {
        this.clientService.registerRequestHandler(this);
        this.socketService.request(Messages.AUTHORIZATION, this.roomId, (result:AuthorizationResult) => {
            if(result.authorized){
                this.clientService.showClient(document.getElementById("playingScreen"));
                this.socketService.request(Messages.GAME_DEFINITION);
                this.teamSelectComponent.show();
            } else {
                alert('Unauthorized access!');
            }
        });
    }

    ngOnDestroy():any {
        this.clientService.deleteEventHandler(this.messageType());
        this.clientService.cancelGame();
        this.socketService.emit(Messages.LEAVE_GAME);
    }
}