import {Component,OnInit,Inject} from "@angular/core";
import {RouterLink,Router} from "@angular/router-deprecated";
import {RoomInfo} from "../../../../shared/communication/RoomInfo";
import {SocketClient} from "../../../../client/communication/SocketClient";
import {CONTAINER} from "../../../../shared/DI";
import {Messages} from "../../../../shared/communication/Messages";
import {SocketService} from "../socket/SocketService";
import {SecuredComponent} from "../user/SecuredComponent";
import {UserService} from "../user/UserService";

@Component({
    templateUrl: 'ui/app/components/room-select/room-select.html',
    directives: [ RouterLink]
})
export class RoomSelectComponent extends SecuredComponent implements OnInit {
    private rooms:RoomInfo[];
    private socketService:SocketService;

    constructor(@Inject(Router) router:Router, @Inject(SocketService) socketService:SocketService, @Inject(UserService) userService:UserService) {
        super(router, userService);
        this.socketService = socketService;
        this.router = router;
    }

    ngOnInit():any {
        super.ngOnInit();
        this.socketService.request(Messages.ROOMS_LIST, null, (response:RoomInfo[]) => {
            this.rooms = response;
        });
    }

    public connect(index:number):void {
        this.router.navigate(["Play", {roomId: this.rooms[index].id}]);
    }
}