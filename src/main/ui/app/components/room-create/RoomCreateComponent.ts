import {Component,OnInit,Inject} from "@angular/core";
import {Router} from "@angular/router-deprecated";
import {CONTAINER} from "../../../../shared/DI";
import {SocketClient} from "../../../../client/communication/SocketClient";
import {Messages} from "../../../../shared/communication/Messages";
import {ClientSocketRequestHandler} from "../../../../client/communication/ClientSocketRequestHandler";
import {CreateRoomResult} from "../../../../shared/communication/CreateRoomResult";
import {SocketService} from "../socket/SocketService";
import {SecuredComponent} from "../user/SecuredComponent";
import {UserService} from "../user/UserService";
import {ClientService} from "../../client/ClientService";

@Component({
    templateUrl: 'ui/app/components/room-create/room-create.html',
})
export class RoomCreateComponent extends SecuredComponent implements OnInit,ClientSocketRequestHandler {

    private roomName:string;

    constructor(@Inject(Router)router:Router, @Inject(SocketService) private socketService:SocketService,
                @Inject(UserService)userService:UserService, @Inject(ClientService)private clientService:ClientService){
        super(router, userService);
    }

    ngOnInit():any {
        super.ngOnInit();
        //CONTAINER.get<Client>("client").registerRequestHandler(this);
        this.clientService.registerRequestHandler(this);
    }

    public createRoom(): void {
        this.socketService.emit(Messages.CREATE_ROOM, this.roomName);
    }

    handleMessage(from:any, msg:any, socket:SocketIOClient.Socket) {
        //TODO: If false show message to client
        var createRoomResult:CreateRoomResult = <CreateRoomResult>msg;
        if (createRoomResult != null && createRoomResult.success) {
            //this.socketClient.request(Messages.AUTHORIZATION, createRoomResult.roomInfo.id);
            this.router.navigate(['Play',{roomId: createRoomResult.roomInfo.id} ]);
        } else {
            alert('Cannot create room');
        }
    }

    messageType():string {
        return Messages.ROOM_CREATED;
    }

    isLoggable():boolean {
        return true;
    }
}