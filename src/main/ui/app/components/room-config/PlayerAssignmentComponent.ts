import {Component,Input} from "@angular/core";
import {PlayerAssignmentInfo} from "../../../../shared/communication/PlayerAssignmentInfo";

@Component({
    selector: 'player-assignment',
    templateUrl: 'ui/app/components/room-config/player-assignment.html',
})
export class PlayerAssignmentComponent {
    @Input("player")
    playerAssignment:PlayerAssignmentInfo;
    @Input()
    team:number;

    constructor() {
    }
}