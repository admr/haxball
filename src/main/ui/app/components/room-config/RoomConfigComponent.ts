import {Component,OnInit,Inject} from "@angular/core";
import {PlayerInfo} from "../../../../shared/gameplay/player/PlayerInfo";
import {PlayerAssignmentInfo} from "../../../../shared/communication/PlayerAssignmentInfo";
import {ClientSocketRequestHandler} from "../../../../client/communication/ClientSocketRequestHandler";
import {Messages} from "../../../../shared/communication/Messages";
import {SocketService} from "../socket/SocketService";
import {ClientService} from "../../client/ClientService";
import {PlayerAssignmentComponent} from "./PlayerAssignmentComponent";
import {RoomConfiguration} from "../../../../shared/communication/RoomConfiguration";
import {GamePhaseService} from "../../game-phase/GamePhaseService";
import {GamePhaseType} from "../../../../shared/gameplay/GamePhaseType";
import {GamePhaseChanged} from "../../../../shared/communication/GamePhaseChanged";
import {RoomAttributeInfo} from "../../../../shared/communication/RoomAttributeInfo";
import {RoomAttributeType} from "../../../../shared/communication/RoomAttributeInfo";

@Component({
    selector: 'team-select',
    templateUrl: 'ui/app/components/room-config/room-config.html',
    directives: [PlayerAssignmentComponent]
})
export class RoomConfigComponent implements OnInit, ClientSocketRequestHandler {

    private gamePhase:GamePhaseType;
    private teamSelectIsVisible:boolean;
    private roomConfiguration:RoomConfiguration = new RoomConfiguration();

    constructor(@Inject(SocketService)private socketService:SocketService,@Inject(ClientService)private clientService:ClientService,
        @Inject(GamePhaseService)private gamePhaseService:GamePhaseService) {

    }

    ngOnInit():any {
        this.gamePhaseService.phaseChange.subscribe((gamePhaseChanged:GamePhaseChanged) => { this.gamePhase = gamePhaseChanged.nextPhase });
        this.clientService.registerRequestHandler(this);
        this.socketService.request(Messages.ROOM_CONFIG,null,(roomConfiguration:RoomConfiguration) =>
            this.roomConfiguration = roomConfiguration
        );
    }

    handleMessage(from:any, msg:RoomConfiguration, socket:SocketIOClient.Socket):any {
        this.roomConfiguration = msg;
    }

    public changeTeam(player:PlayerAssignmentInfo,target:number):void {
        if(player.team == 0){
            player.team = -1;
        } else if(player.team == 1){
            player.team = -1;
        } else {
            player.team = target;
        }

        this.socketService.emit(Messages.SET_ROOM_ATTRIBUTE,new RoomAttributeInfo(RoomAttributeType.PLAYER_TEAM, player));
    }

    public changeMatchTime(newValue:number):void {
        this.socketService.emit(Messages.SET_ROOM_ATTRIBUTE, new RoomAttributeInfo(RoomAttributeType.MATCH_TIME, newValue));
    }

    public startMatch():void {
        this.socketService.emit(Messages.START_MATCH);
    }

    public startMatchAvailable():boolean {
        return this.gamePhase == GamePhaseType.DEFINING_MATCH;
    }

    messageType():string {
        return Messages.ROOM_CONFIG;
    }

    isLoggable():boolean {
        return true;
    }

    public show(){
        this.teamSelectIsVisible = true;
    }

    public close(){
        this.teamSelectIsVisible = false;
    }
}