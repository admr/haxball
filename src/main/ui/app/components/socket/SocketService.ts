import {Injectable} from "@angular/core";
import {SocketClient} from "../../../../client/communication/SocketClient";
import {CONTAINER} from "../../../../shared/DI";

@Injectable()
export class SocketService {

    private socketClient:SocketClient;

    init():any {
        this.socketClient = CONTAINER.get<SocketClient>("socketClient");
    }

    public emit(event:string, data:any = null):SocketIOClient.Socket {
        return this.socketClient.emit(event, data);
    }

    public request(event:string, data:any = null, callbackHandler:Function = null):SocketIOClient.Socket {
        return this.socketClient.request(event, data, callbackHandler);
    }
}