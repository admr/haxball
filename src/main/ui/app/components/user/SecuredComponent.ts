import {OnInit} from "@angular/core";
import {Router} from "@angular/router-deprecated";
import {UserService} from "./UserService";

export class SecuredComponent implements OnInit {
    protected router:Router;
    protected userService:UserService;

    constructor(router:Router, userService:UserService) {
        this.router = router;
        this.userService = userService;
    }

    ngOnInit():any {
        if(!this.userService.isLoggedIn){
            this.router.navigate(["Login"]);
        }
    }
}