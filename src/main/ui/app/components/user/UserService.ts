import {Injectable} from "@angular/core";

@Injectable()
export class UserService {

    private _isLoggedIn:boolean = false;

    constructor(){
    }

    get isLoggedIn():boolean {
        return this._isLoggedIn;
    }

    set isLoggedIn(value:boolean) {
        this._isLoggedIn = value;
    }
}