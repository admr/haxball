import {OnInit,Injectable} from "@angular/core";
import {GameSnapshotListener} from "../../../../client/gameplay/GameSnapshotListener";
import {GameSnapshot} from "../../../../shared/gameplay/snapshots/GameSnapshot";
import {ClientLoop} from "../../../../client/gameplay/ClientLoop";
import {CONTAINER} from "../../../../shared/DI";

@Injectable()
export class GameSnapshotService{
    private clientLoop:ClientLoop;

    constructor(){
        this.clientLoop = CONTAINER.get<ClientLoop>("clientLoop");
    }

    public registerGameSnapshotListener(listener:GameSnapshotListener):void {
        this.clientLoop.registerGameSnapshotListener(listener);
    }
}