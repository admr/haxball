import {Component,OnInit,Inject} from "@angular/core";
import {GameSnapshot} from "../../../../shared/gameplay/snapshots/GameSnapshot";
import {GameSnapshotListener} from "../../../../client/gameplay/GameSnapshotListener";
import {GameSnapshotService} from "./GameSnapshotService";
@Component({
    selector: 'game-score',
    templateUrl: 'ui/app/components/game-score/game-score.html'
})
export class GameScoreComponent implements GameSnapshotListener{
    private gameSnapshot:GameSnapshot;

    constructor(@Inject(GameSnapshotService)gameSnapshotService:GameSnapshotService) {
        this.gameSnapshot = new GameSnapshot();
        this.gameSnapshot.timeLeft = 0;
        gameSnapshotService.registerGameSnapshotListener(this);
    }

    formatTimeLeft():string {
        var date:Date = new Date(this.gameSnapshot.timeLeft*1000);
        return this.printInTwoPlaces(date.getMinutes()) + ":" + this.printInTwoPlaces(date.getSeconds());
    }

    printInTwoPlaces(i:number):string {
        return ("0" + i).slice(-2);
    }

    onGameSnapshotChange(gameSnapshot:GameSnapshot) {
        this.gameSnapshot = gameSnapshot;
    }
}