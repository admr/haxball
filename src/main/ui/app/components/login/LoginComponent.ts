import {Component,OnInit,Inject} from "@angular/core";
import {Router} from "@angular/router-deprecated";
import {SocketService} from "../socket/SocketService";
import {Messages} from "../../../../shared/communication/Messages";
import {LoginRequest} from "../../../../shared/communication/LoginRequest";
import {LoginResult} from "../../../../shared/communication/LoginResult";
import {UserService} from "../user/UserService";

@Component({
    templateUrl: 'ui/app/components/login/login.html'
})
export class LoginComponent {
    private socketService:SocketService;
    private router:Router;
    private userService:UserService;
    private playerName:string;
    private password:string;
    private message:string;
    constructor(@Inject(SocketService) socketService:SocketService, @Inject(Router)router:Router, @Inject(UserService)userService:UserService){
        this.socketService = socketService;
        this.router = router;
        this.userService = userService;
    }

    public login(){
        this.message = null;
        this.socketService.request(Messages.LOGIN, new LoginRequest(this.playerName, this.password), (result:LoginResult) => {
            if(result.loggedIn){
                this.userService.isLoggedIn = true;
                this.router.navigate(["RoomSelect"]);
            } else {
                this.message = result.message;
            }
        })
    }
}