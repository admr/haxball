///<reference path="../../../../typings/index.d.ts"/>

import {bootstrap} from "@angular/platform-browser-dynamic";
import {provide,enableProdMode} from "@angular/core";
import {LocationStrategy, HashLocationStrategy} from "@angular/common";
import {ROUTER_PROVIDERS} from "@angular/router-deprecated";
import {HTTP_PROVIDERS} from "@angular/http";
import {SocketService} from "./components/socket/SocketService";
import {GameSnapshotService} from "./components/game-score/GameSnapshotService";
import {UserService} from "./components/user/UserService";

import {HaxBallApp} from "./HaxBallApp";
import {ClientService} from "./client/ClientService";
import {GamePhaseService} from "./game-phase/GamePhaseService";

bootstrap(HaxBallApp, [
    HTTP_PROVIDERS,
    ROUTER_PROVIDERS,
    GamePhaseService,
    ClientService,
    UserService,
    SocketService,
    GameSnapshotService,
    provide(LocationStrategy, {useClass: HashLocationStrategy})
]);