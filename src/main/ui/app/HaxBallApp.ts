import {Component, OnInit,Inject} from "@angular/core";
import {RouteConfig, RouterLink, ROUTER_DIRECTIVES} from "@angular/router-deprecated";

import bootstrap = require("../../client/bootstrap");
import sharedBootstrap = require("../../shared/sharedBootstrap");
import {CONTAINER} from "../../shared/DI";
import {Game} from "../../client/Game";
import {ResourceManager} from "../../client/ResourceManager";
import {ALL} from "../../client/ResourceGroups";
import {PlayerController} from "../../client/gameplay/input/PlayerController";
import {RoomSelectComponent} from "./components/room-select/RoomSelectComponent";
import {RoomCreateComponent} from "./components/room-create/RoomCreateComponent";
import {PlayComponent} from "./components/play/PlayComponent";
import {SocketService} from "./components/socket/SocketService";
import {MainPageComponent} from "./components/main-page/MainPageComponent";
import {LoginComponent} from "./components/login/LoginComponent";
import {ClientService} from "./client/ClientService";

@Component({
    selector: "app",
    templateUrl: "./ui/app/hax-ball-app.html",
    directives: [ RouterLink, ROUTER_DIRECTIVES]
})
@RouteConfig([
    {path: '/', component: MainPageComponent, useAsDefault: true},
    {path: '/login', component: LoginComponent, as: 'Login'},
    {path: '/room-select', component: RoomSelectComponent, as:'RoomSelect'},
    {path: '/room-create', component: RoomCreateComponent, as:'RoomCreate'},
    {path: '/play/:roomId', component: PlayComponent, as:'Play'}
])
export class HaxBallApp implements OnInit {


    constructor(@Inject(SocketService)private socketService:SocketService, @Inject(ClientService)private clientService:ClientService) {
    }

    ngOnInit() {
        console.log("Application component initialized ...");


        var screen:HTMLElement = document.getElementById('screen');

        var dbWidth = 800;
        var dbHeight = 600;

        bootstrap.createBootstrap(dbWidth, dbHeight);
        sharedBootstrap.createGlobalBootstrap();
        sharedBootstrap.createGameplayBootstrap();

        //var client:Client = CONTAINER.get<Client>("client");
        //client.initClient();
        this.clientService.initClient();

        this.socketService.init();
    }
}