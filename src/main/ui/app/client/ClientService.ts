import {Injectable,Inject} from "@angular/core";
import {SocketClient} from "../../../client/communication/SocketClient";
import {SocketEventFacade} from "../../../client/communication/SocketEventFacade";
import {Game} from "../../../client/Game";
import {PlayerController} from "../../../client/gameplay/input/PlayerController";
import {CONTAINER} from "../../../shared/DI";
import {ResourceManager} from "../../../client/ResourceManager";
import {ClientSocketRequestHandler} from "../../../client/communication/ClientSocketRequestHandler";
import {ALL} from "../../../client/ResourceGroups";
import {GamePhaseService} from "../game-phase/GamePhaseService";

@Injectable()
export class ClientService {

    private game:Game;
    private socketEventFacade:SocketEventFacade;
    private socketClient:SocketClient;
    public constructor(@Inject(GamePhaseService)private gamePhaseService:GamePhaseService){
    }

    public initClient() {
        this.game = CONTAINER.get<Game>("game");
        this.socketEventFacade = CONTAINER.get<SocketEventFacade>("socketEventFacade");
        this.socketClient = CONTAINER.get<SocketClient>("socketClient");

        this.socketClient.connect("http://localhost:5858");

        this.socketEventFacade.configureSocket(this.socketClient.socket, "server");

        var playerController:PlayerController = CONTAINER.get<PlayerController>("playerController");
        playerController.registerEvents(document);
        this.registerRequestHandler(this.gamePhaseService);
        //this.gamePhaseService.init();
    }

    public showClient(element: HTMLElement):void {
        var renderer:PIXI.SystemRenderer = CONTAINER.get<PIXI.SystemRenderer>("renderer");
        element.appendChild(renderer.view);

        var resourceManager:ResourceManager = CONTAINER.get<ResourceManager>("resourceManager");
        resourceManager.loadResources(ALL, () => {
            console.log("LOADED");
        });

        this.game.startGame();
        this.game.updateViewPort(element.clientWidth, element.clientHeight);

        $(window).resize(() => {
            this.game.updateViewPort(element.clientWidth, element.clientHeight);
        });
    }

    public cancelGame():void{
        this.game.stopGame();
    }

    public registerRequestHandler(handler:ClientSocketRequestHandler):void {
        this.socketEventFacade.addAndRegisterEventHandler(handler, this.socketClient.socket, "server");
    }

    public deleteEventHandler(id:string):void {
        this.socketEventFacade.deleteEventHandler(id);
    }
}