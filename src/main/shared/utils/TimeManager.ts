export class TimeManager {
    public getNowTimestamp():number {
        return new Date().getTime();
    }
}