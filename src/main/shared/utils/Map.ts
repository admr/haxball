export interface Map<T> {
    [id:string]: T;
}