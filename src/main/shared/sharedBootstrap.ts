import {CONTAINER} from "../shared/DI";
import {PhysicsManager} from "./gameplay/physics/PhysicsManager";
import {CircleCreator} from "./gameplay/scene/creators/CircleCreator";
import {RectangleCreator} from "./gameplay/scene/creators/RectangleCreator";
import {SnapshotManager} from "./gameplay/snapshots/SnapshotManager";
import {ActionFacade} from "./gameplay/ActionFacade";
import {LineCreator} from "./gameplay/scene/creators/LineCreator";
import {PlaneCreator} from "./gameplay/scene/creators/PlaneCreator";
import {GamePhaseManager} from "./gameplay/GamePhaseManager";
import {DefiningMatchPhase} from "./gameplay/gamephases/DefiningMatchPhase";
import {TestGamePhase} from "./gameplay/gamephases/TestGamePhase";
import {TimeManager} from "./utils/TimeManager";

export var p2 = require("p2");

export function createGameplayBootstrap() {
    CONTAINER.bind("physicsManager").to.type(PhysicsManager).as.singleton();
    CONTAINER.bind("snapshotManager").to.type(SnapshotManager).as.singleton();
    CONTAINER.bind("actionFacade").to.type(ActionFacade).as.singleton();

    CONTAINER.bind("gamePhases").to.type(DefiningMatchPhase).as.singleton();
    CONTAINER.bind("gamePhases").to.type(TestGamePhase).as.singleton();
}

export function deleteGameplayBootstrap() {
    CONTAINER.unbind("snapshotManager");
    CONTAINER.unbind("physicsManager");
    CONTAINER.unbind("actionFacade");
    CONTAINER.unbind("gamePhases");
}

export function createGlobalBootstrap() {
    //CONTAINER.bind("p2").to.instance(p2);
    CONTAINER.bind("timeManager").to.type(TimeManager).as.singleton();
    CONTAINER.bind("shapeCreators").to.type(CircleCreator).as.singleton();
    CONTAINER.bind("shapeCreators").to.type(RectangleCreator).as.singleton();
    CONTAINER.bind("shapeCreators").to.type(LineCreator).as.singleton();
    CONTAINER.bind("shapeCreators").to.type(PlaneCreator).as.singleton();
}