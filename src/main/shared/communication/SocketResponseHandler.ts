export interface SocketResponseHandler<T> {
    handleResponse(result:any, context:T):void;
    getRequestType():string;
}