import {GamePhaseType} from "../gameplay/GamePhaseType";
export class GamePhaseChanged {
    public previousPhase:GamePhaseType;
    public nextPhase:GamePhaseType;

    constructor(previousPhase:GamePhaseType, nextPhase:GamePhaseType) {
        this.previousPhase = previousPhase;
        this.nextPhase = nextPhase;
    }
}