import {SocketRequestHandler} from "./SocketRequestHandler";
import {SocketRequestDelegationContext} from "./SocketRequestDelegationContext";

interface EventBinder {
    on(event:string, listener:Function): any;
}

export abstract class AbstractSocketEventFacade<T extends EventBinder> {
    protected registered:{[key: string] : SocketRequestHandler<T>} = {};

    public constructor() {
    }

    public configureSocket(socket:T, socketId:string) {
        for (var key in this.registered) {
            //socket.on(key, this.createDelegationContext(this.registered[key], socketId, socket));
            this.registerHandler(socket, socketId, this.registered[key]);
        }
    }

    protected registerHandler(socket:T, socketId:string, requestHandler:SocketRequestHandler<T>) {
        socket.on(requestHandler.messageType(), this.createDelegationContext(requestHandler, socketId, socket));
    }

    protected createDelegationContext(handler:SocketRequestHandler<T>, from:string, socket:T):Function {
        return new SocketRequestDelegationContext(handler, from, socket).delegate;
    }

    public addEventHandler(handler:SocketRequestHandler<T>) {
        this.registered[handler.messageType()] = handler;
    }

    public addAndRegisterEventHandler(handler:SocketRequestHandler<T>,socket:T, socketId:string){
        this.addEventHandler(handler);
        this.registerHandler(socket, socketId, handler);
    }

    public deleteEventHandler(id:string):void {
        delete this.registered[id];
    }
}