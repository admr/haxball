import {SocketRequestHandler} from "./SocketRequestHandler";

export class SocketRequestDelegationContext<T> {
    private handler:SocketRequestHandler<T>;
    private from:string;
    private socket:T;

    constructor(handler:SocketRequestHandler<T>, from:string, socket:T) {
        this.handler = handler;
        this.from = from;
        this.socket = socket;
    }

    public delegate = (message:any) => {
        if (this.handler.isLoggable()) {
            console.log("Handling message [" + this.handler.messageType() + "] <" + JSON.stringify(message) + ">");
        }
        this.handler.handleMessage(this.from, message, this.socket);
    }
}