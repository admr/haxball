export class LoginRequest {
    public playerName:string;
    public password:string;

    constructor(playerName:string,password:string) {
        this.playerName = playerName;
        this.password = password;
    }
}