import {RoomInfo} from "./RoomInfo";
export class CreateRoomResult {
    public roomInfo:RoomInfo;
    public success:boolean;

    constructor(roomInfo:RoomInfo, success:boolean) {
        this.roomInfo = roomInfo;
        this.success = success;
    }
}