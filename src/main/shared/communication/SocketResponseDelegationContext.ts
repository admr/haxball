import {SocketResponseHandler} from "./SocketResponseHandler";

export class SocketResponseDelegationContext<T> {

    private handler:SocketResponseHandler<T>;
    private context:T;

    constructor(handler:SocketResponseHandler<T>, context:T) {
        this.handler = handler;
        this.context = context;
    }

    public delegate = (response) => {
        this.handler.handleResponse(response, this.context);
    }
}