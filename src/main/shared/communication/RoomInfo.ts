export class RoomInfo {
    public id:string;
    public roomName:string;
    public players:number;

    constructor(id:string, roomName:string, players:number) {
        this.id = id;
        this.roomName = roomName;
        this.players = players;
    }
}