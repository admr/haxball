export class LoginResult {
    public loggedIn:boolean;
    public message:string;

    constructor(loggedIn:boolean, message:string) {
        this.loggedIn = loggedIn;
        this.message = message;
    }
}