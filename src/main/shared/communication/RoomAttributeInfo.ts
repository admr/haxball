export enum RoomAttributeType {
    PLAYER_TEAM,
    MATCH_TIME
}
export class RoomAttributeInfo {
    public type:RoomAttributeType;
    public value:any;

    constructor(type:RoomAttributeType, value:any) {
        this.type = type;
        this.value = value;
    }
}