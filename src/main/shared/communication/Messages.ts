export class Messages {
    public static get LOGIN():string {
        return "LOGIN"
    };

    public static get ROOMS_LIST():string {
        return "ROOMS_LIST"
    };

    public static get AUTHORIZATION():string {
        return "AUTHORIZATION"
    };

    public static get CREATE_ROOM():string {
        return "CREATE_ROOM"
    };

    public static get ROOM_CREATED():string {
        return "ROOM_CREATED"
    };

    public static get GAME_PHASE_CHANGE():string {
        return "GAME_PHASE_CHANGE";
    };

    public static get GAME_DEFINITION():string {
        return "GAME_DEFINITION"
    };

    public static get GAME_STATE():string {
        return "GAME_STATE"
    };

    public static get DO_ACTIONS():string {
        return "DO_ACTIONS"
    };

    public static get JOIN_GAME():string {
        return "JOIN_GAME"
    };

    public static get LEAVE_GAME():string {
        return "LEAVE_GAME"
    };

    public static get OBJECTS_DEFINITION():string {
        return "OBJECTS_DEFINITION"
    };

    public static get REMOVE_OBJECT():string {
        return "REMOVE_OBJECT"
    };

    public static get ROOM_CONFIG():string {
        return "ROOM_CONFIG"
    };

    public static get SET_ROOM_ATTRIBUTE():string {
        return "SET_ROOM_ATTRIBUTE";
    };

    public static get START_MATCH():string {
        return "START_MATCH";
    };
}