import {PlayerAssignmentInfo} from "./PlayerAssignmentInfo";
export class RoomConfiguration {
    public maxPlayers:number;
    public timeLeft:number;
    public playerAssignments:PlayerAssignmentInfo[];
}