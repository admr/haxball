export class PlayerAssignmentInfo {
    public playerName:string;
    public clientId:string;
    public team:number;
}