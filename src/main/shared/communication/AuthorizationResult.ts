export class AuthorizationResult {
    public authorized:boolean;

    constructor(authorized:boolean) {
        this.authorized = authorized;
    }
}