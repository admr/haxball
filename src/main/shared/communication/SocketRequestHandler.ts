export interface SocketRequestHandler<T> {
    handleMessage(from:any, msg:any, socket:T):any;
    messageType():string;
    isLoggable():boolean;
}