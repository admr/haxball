import {AbstractAction} from "./AbstractAction";

export class NoArgAction extends AbstractAction {

    constructor(type:string) {
        super(type);
    }
}