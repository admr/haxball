export class ActionTypes {
    public static get PLAYER_MOVE():string {
        return "PLAYER_MOVE"
    };

    public static get KICK_BALL():string {
        return "KICK_BALL"
    };
}