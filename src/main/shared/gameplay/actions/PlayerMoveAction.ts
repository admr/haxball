import {AbstractAction} from "./AbstractAction";
import {ActionTypes} from "./ActionTypes";

export class PlayerMoveAction extends AbstractAction {
    public direction:number[];

    constructor() {
        super(ActionTypes.PLAYER_MOVE);
    }
}