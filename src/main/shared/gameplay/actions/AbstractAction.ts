export abstract class AbstractAction {
    public type:string;
    public caller:string;

    constructor(type:string) {
        this.type = type;
    }
}