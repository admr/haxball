import {PhysicsManager} from "./../gameplay/physics/PhysicsManager";
import {ActionFacade} from "./ActionFacade";
import {AbstractScene} from "../gameplay/scene/AbstractScene";
import {GameSnapshot} from "./snapshots/GameSnapshot";
import {GamePhaseManager} from "./GamePhaseManager";
import {TimeManager} from "../utils/TimeManager";

export abstract class AbstractLoop {

    protected physicsManager:PhysicsManager;
    protected previousUpdateTime:number;
    protected scene:AbstractScene;
    protected gamePhaseManager:GamePhaseManager;
    protected timeManager:TimeManager;
    private _actionFacade:ActionFacade;
    private _gameSnapshot:GameSnapshot;

    protected delta:number = 0.0;
    protected _timeStep:number = 1.0 / 60.0;

    constructor(physicsManager:PhysicsManager, actionFacade:ActionFacade, scene:AbstractScene, gamePhaseManager:GamePhaseManager,
                timeManager:TimeManager) {
        this.physicsManager = physicsManager;
        this._actionFacade = actionFacade;
        this.scene = scene;
        this.gamePhaseManager = gamePhaseManager;
        this.timeManager = timeManager;
        this.gameSnapshot = new GameSnapshot();
    }

    public step():void {
        if (this.previousUpdateTime == null) {
            this.previousUpdateTime = this.timeManager.getNowTimestamp();
        }
        this._actionFacade.resolveIncomingActions();

        var currentTime:number = this.timeManager.getNowTimestamp();
        var dt = (currentTime - this.previousUpdateTime) / 1000.0;
        var numUpdates = 0;
        this.delta += dt;

        while (this.delta >= this.timeStep) {
            if (this.gamePhaseManager.currentPhase != null) {
                this.gamePhaseManager.currentPhase.update(this.timeStep, this.gamePhaseManager);
            }
            this.updateWorld(this.timeStep);

            this.delta -= this.timeStep;

            if (++numUpdates > 20) {
                console.log("ERROR too much loop updates");
                break;
            }
        }


        this.previousUpdateTime = currentTime;

        this.postStep();
    }

    protected updateProffiling() {

    }

    get actionFacade():ActionFacade {
        return this._actionFacade;
    }

    protected updateWorld(timeStep:number):void {
        this.scene.update(timeStep);
        this.physicsManager.step(timeStep);
    }

    protected postStep():void {
        this._actionFacade.clearOutgoingActions();
    }

    get gameSnapshot():GameSnapshot {
        return this._gameSnapshot;
    }

    set gameSnapshot(value:GameSnapshot) {
        this._gameSnapshot = value;
    }

    get timeStep():number {
        return this._timeStep;
    }

    set timeStep(value:number) {
        this._timeStep = value;
    }
}