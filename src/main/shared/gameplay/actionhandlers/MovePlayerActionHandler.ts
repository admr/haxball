import {AbstractActionHandler} from "../AbstractActionHandler";
import {ActionTypes} from "../actions/ActionTypes";
import {AbstractAction} from "../actions/AbstractAction";
import {PhysicObject} from "../physics/PhysicObject";
import {ActionHandlingResult} from "../ActionHandlingResult";
import {AbstractScene} from "../../gameplay/scene/AbstractScene";
import {PlayerMoveAction} from "../actions/PlayerMoveAction";
import {p2} from "../../../shared/sharedBootstrap";

export class MovePlayerActionHandler extends AbstractActionHandler {

    private scene:AbstractScene;

    public constructor(scene:AbstractScene) {
        super();
        this.scene = scene;
    }

    getSupportedActionType():string {
        return ActionTypes.PLAYER_MOVE;
    }

    handleAction(action:AbstractAction, isInternal):ActionHandlingResult {
        var physicalObject:PhysicObject = this.scene.getObject(action.caller);
        var playerMoveAction:PlayerMoveAction = <PlayerMoveAction>action;
        if (physicalObject != null && playerMoveAction != null) {
            if (physicalObject.playerInfo != null) {
                var direction:number[] = null;
                if (playerMoveAction.direction != null) {
                    direction = [0, 0];
                    p2.vec2.normalize(direction, playerMoveAction.direction);

                    p2.vec2.scale(direction, direction, physicalObject.playerInfo.movingSpeed);

                }
                physicalObject.direction = direction;
            }
        }

        return ActionHandlingResult.EMPTY_RESULT;
    }

}