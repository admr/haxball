import {AbstractActionHandler} from "../AbstractActionHandler";
import {AbstractAction} from "../actions/AbstractAction";
import {ActionHandlingResult} from "../ActionHandlingResult";

export class TestActionHandler extends AbstractActionHandler {
    getSupportedActionType():string {
        return "TEST";
    }

    handleAction(action:AbstractAction, isInternal:boolean):ActionHandlingResult {
        return ActionHandlingResult.EMPTY_RESULT;
    }

}