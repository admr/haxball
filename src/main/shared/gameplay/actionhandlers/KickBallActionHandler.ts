import {AbstractActionHandler} from "../AbstractActionHandler";
import {AbstractAction} from "../actions/AbstractAction";
import {ActionHandlingResult} from "../ActionHandlingResult";
import {ActionTypes} from "../actions/ActionTypes";
import {AbstractScene} from "../../gameplay/scene/AbstractScene";
import {PhysicObject} from "../physics/PhysicObject";
import {p2} from "../../../shared/sharedBootstrap";

export class KickBallActionHandler extends AbstractActionHandler {

    protected scene:AbstractScene;

    constructor(scene:AbstractScene) {
        super();
        this.scene = scene;
    }

    getSupportedActionType():string {
        return ActionTypes.KICK_BALL;
    }

    handleAction(action:AbstractAction, isInternal:boolean):ActionHandlingResult {
        var ball:PhysicObject = this.scene.getObject('ball');
        var physicalObject:PhysicObject = this.scene.getObject(action.caller.toString());
        if (physicalObject != null && ball != null) {
            if (physicalObject.playerInfo != null) {
                if (ball.body.overlaps(physicalObject.body)) {
                    var force = [0, 0];
                    p2.vec2.sub(force, ball.body.position, physicalObject.body.position);
                    p2.vec2.normalize(force, force);
                    p2.vec2.scale(force, force, physicalObject.playerInfo.kickStrength);
                    ball.body.applyForce(force, ball.body.position);
                }
                return new ActionHandlingResult(action);
            }
        }

        return ActionHandlingResult.EMPTY_RESULT;
    }

}