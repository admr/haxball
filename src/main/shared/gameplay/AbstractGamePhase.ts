import {GamePhaseType} from "./GamePhaseType";
import {GamePhaseManager} from "./GamePhaseManager";

export abstract class AbstractGamePhase {
    public abstract update(dt:number, gamePhaseManager:GamePhaseManager):void;

    public abstract getType():GamePhaseType;

    public abstract onEnter(previousState:GamePhaseType, args:any):void;

    public abstract onExit(nextState:GamePhaseType):void;

    public abstract getAllowedNextPhases():GamePhaseType[];
}