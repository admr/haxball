import {PhysicObjectSnapshot} from "./PhysicObjectSnapshot";
import {Map} from "../../utils/Map";

export class GameSnapshot {
    public timeLeft:number;
    public teamScores:number[] = [0, 0];
    public physicObjectsSnapshots:Map<PhysicObjectSnapshot>;
}