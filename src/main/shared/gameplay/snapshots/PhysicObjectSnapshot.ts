export class PhysicObjectSnapshot {
    public timestamp:number;
    public direction:number[];
    public fixedRotation:number;
    public position:number[];
    public interpolatedPosition:number[];
    public interpolatedAngle:number;
    public previousPosition:number[];
    public previousAngle:number;
    public velocity:number[];
    public vlambda:number[];
    public wlambda:number[];
    public angle:number;
    public angularVelocity:number;
    public force:number[];
    public angularForce:number;
}