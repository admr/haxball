import {PhysicObjectSnapshot} from "./PhysicObjectSnapshot";
import {PhysicObject} from "../physics/PhysicObject";

export class SnapshotManager {

    public applyStateOnPhysicObject(state:PhysicObjectSnapshot, object:PhysicObject):void {
        var bo:p2.Body = object.body;

        object.direction = state.direction;
        bo.angle = state.angle;
        bo.position = state.position;
        bo.angularForce = state.angularForce;
        bo.interpolatedAngle = state.interpolatedAngle;
        bo.interpolatedPosition = state.interpolatedPosition;
        bo.force = state.force;
        bo.velocity = state.velocity;
        bo.vlambda = state.vlambda;
        bo.wlambda = state.wlambda;
        bo.previousPosition = state.previousPosition;
        bo.previousAngle = state.previousAngle;
        bo.angularVelocity = state.angularVelocity;
    }

    public getStateFromPhysicObject(object:PhysicObject, timestamp:number):PhysicObjectSnapshot {
        var b:p2.Body = object.body;
        var out:PhysicObjectSnapshot = new PhysicObjectSnapshot();

        out.direction = object.direction;
        out.timestamp = timestamp;
        out.angle = b.angle;
        out.position = b.position;
        out.angularForce = b.angularForce;
        out.interpolatedAngle = b.interpolatedAngle;
        out.interpolatedPosition = b.interpolatedPosition;
        out.force = b.force;
        out.velocity = b.velocity;
        out.vlambda = b.vlambda;
        out.wlambda = b.wlambda;
        out.previousPosition = b.previousPosition;
        out.previousAngle = b.previousAngle;
        out.angularVelocity = b.angularVelocity;

        return out;
    }
}