import {AbstractActionHandler} from "./AbstractActionHandler";
import {AbstractAction} from "./actions/AbstractAction";
import {ActionHandlingResult} from "./ActionHandlingResult";

export class ActionFacade {

    private handlers:{[type:string] : AbstractActionHandler} = {};
    private incomingActions:AbstractAction[] = [];
    private outgoingActions:AbstractAction[] = [];

    constructor(actionHandlers:AbstractActionHandler[]) {
        for (let i in actionHandlers) {
            this.handlers[actionHandlers[i].getSupportedActionType()] = actionHandlers[i];
        }
    }

    public addIncomingAction(action:AbstractAction) {
        var index:number = this.findIndexOfAction(action);
        if(index != -1){
            this.incomingActions[index] = action;
        } else {
            this.incomingActions.push(action);
        }
    }

    protected findIndexOfAction(action:AbstractAction):number {
        for(var i = 0;i < this.incomingActions.length; ++i) {
            var incomingAction = this.incomingActions[i];
            if(incomingAction.caller == action.caller && incomingAction.type == action.type){
                return i;
            }
        }

        return -1;
    }

    public resolveIncomingActions(howMany:number = 100) {
        for (var i = 0; i < this.incomingActions.length && i < howMany; ++i) {
            this.handleAction(this.incomingActions[i], true);
        }

        this.incomingActions.splice(0, Math.min(i, howMany));
    }

    public getOutgoingActions():AbstractAction[] {
        return this.outgoingActions;
    }

    public clearOutgoingActions() {
        this.outgoingActions = [];
    }

    public handleAction(action:AbstractAction, isIncoming:boolean) {
        var handler:AbstractActionHandler = this.handlers[action.type];
        if (handler != null) {
            var result:ActionHandlingResult = handler.handleAction(action, isIncoming);
            if (result.outgoingAction != null) {
                this.outgoingActions.push(result.outgoingAction);
            }
        }
    }
}