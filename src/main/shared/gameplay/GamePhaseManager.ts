import {GamePhaseType} from "./GamePhaseType";
import {AbstractGamePhase} from "./AbstractGamePhase";

export class GamePhaseManager {
    protected gamePhases:{[id:number] : AbstractGamePhase} = {};
    protected _currentPhase:AbstractGamePhase;

    public constructor(gamePhases:AbstractGamePhase[]) {
        for (let phase of gamePhases) {
            this.gamePhases[phase.getType()] = phase;
        }
    }

    public setPhase(nextPhase:GamePhaseType, args:any = null):boolean {
        console.log('Begin changing phase to: ' + GamePhaseType[nextPhase]);
        var previousType:GamePhaseType = null;
        if (this._currentPhase != null) {
            if (this._currentPhase.getAllowedNextPhases().indexOf(nextPhase) == -1) {
                return false;
            } else {
                previousType = this._currentPhase.getType();
                this._currentPhase.onExit(nextPhase);
            }
        }

        this._currentPhase = this.gamePhases[nextPhase];
        this._currentPhase.onEnter(previousType, args);
        return true;
    }


    get currentPhase():AbstractGamePhase {
        return this._currentPhase;
    }
}