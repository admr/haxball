import {AbstractAction} from "./actions/AbstractAction";

export class ActionHandlingResult {
    public outgoingAction:AbstractAction;

    public static EMPTY_RESULT:ActionHandlingResult = new ActionHandlingResult(null);

    constructor(outgoingAction:AbstractAction) {
        this.outgoingAction = outgoingAction;
    }
}