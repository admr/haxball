export interface ShapeRendererAttributes {
    fillAlpha?: number;
    lineWidth?: number;
    lineColor?: number;
    blendMode?: number;
}