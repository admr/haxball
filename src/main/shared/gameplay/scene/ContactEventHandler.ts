import {ContactHandler} from "./ContactHandler";
import {AbstractScene} from "./AbstractScene";
import {PhysicObject} from "../physics/PhysicObject";

export class ContactEventHandler {

    private handlers:{[id:string] : ContactHandler } = {};
    private scene:AbstractScene;

    public constructor(scene:AbstractScene) {
        this.scene = scene;
    }

    public addHandlerForObjectsPair(idA:string, idB:string, contactHandler:ContactHandler):string {
        var objA:PhysicObject = this.scene.getObject(idA);
        var objB:PhysicObject = this.scene.getObject(idB);

        if (objA != null && objB != null) {
            return this.addHandlerForBodiesPair(objA.body.id, objB.body.id, contactHandler);
        } else {
            throw new Error("Cannot find one of object [" + idA + "," + idB + "] in scene.")
        }
    }

    public addHandlerForBodiesPair(idA:number, idB:number, contactHandler:ContactHandler):string {
        var id:string = this.getPairId(idA, idB);
        this.handlers[id] = contactHandler;
        return id;
    }

    public addHandlerForObject(objectId:string, contactHandler:ContactHandler):string {
        var object:PhysicObject = this.scene.getObject(objectId);
        var result:string = null;
        if (object != null) {
            result = object.body.id.toString();
            this.handlers[result] = contactHandler;
        }

        return result;
    }

    public removeHandler(id:string):void {
        delete this.handlers[id];
    }

    public getPairId(a:number, b:number):string {
        return Math.min(a, b) + "," + Math.max(a, b);
    }

    public delegate = (event) => {
        this.runHandlerIfExists(this.getPairId(event.bodyA.id, event.bodyB.id), event.bodyA.id, event.bodyB.id);
        this.runHandlerIfExists(event.bodyA.id.toString(), event.bodyA.id, event.bodyB.id);
        this.runHandlerIfExists(event.bodyB.id.toString(), event.bodyB.id, event.bodyA.id);
    };

    protected runHandlerIfExists(id:string, bodyIdA:number, bodyIdB:number):void {
        var handler:ContactHandler = this.handlers[id];
        if (handler != null) {
            handler.onBeginContact(
                this.scene.getObjectByBodyId(bodyIdA),
                this.scene.getObjectByBodyId(bodyIdB),
                this.scene,
                id
            );
        }
    }
}