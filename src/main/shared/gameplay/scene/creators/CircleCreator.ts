import {ShapeCreator} from "../ShapeCreator";
import {p2} from '../../../sharedBootstrap';

export class CircleCreator implements ShapeCreator {
    getSupportedType():number {
        return p2.Shape.CIRCLE;
    }

    getRequiredDimensions():number {
        return 1;
    }

    createShape(dimensions:number[]):p2.Shape {
        return new p2.Circle(dimensions[0]);
    }

}