import {ShapeCreator} from "../ShapeCreator";
import {p2} from '../../../sharedBootstrap';

export class PlaneCreator implements ShapeCreator {
    getSupportedType():number {
        return p2.Shape.PLANE;
    }

    getRequiredDimensions():number {
        return 0;
    }

    createShape(dimensions:number[]):p2.Shape {
        return new p2.Plane(p2.Shape.PLANE);
    }

}