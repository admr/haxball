import {ShapeCreator} from "../ShapeCreator";
import {p2} from '../../../sharedBootstrap';

export class RectangleCreator implements ShapeCreator {
    getSupportedType():number {
        return p2.Shape.RECTANGLE;
    }

    getRequiredDimensions():number {
        return 2;
    }

    createShape(dimensions:number[]) {
        return new p2.Rectangle(dimensions[0], dimensions[1]);
    }

}