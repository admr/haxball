import {ShapeCreator} from "../ShapeCreator";
import {p2} from '../../../sharedBootstrap';

export class LineCreator implements ShapeCreator {
    getSupportedType():number {
        return p2.Shape.LINE;
    }

    getRequiredDimensions():number {
        return 1;
    }

    createShape(dimensions:number[]):p2.Shape {
        return new p2.Line(dimensions[0]);
    }

}