import {ShapeTemplate} from "../templates/ShapeTemplate";

export class ShapeTemplateBuilder {
    private type:number;
    private dimensions:number[];
    private materialId:string;
    private collisionGroup:number;
    private collisionMask:number;
    private offset:number[];
    private angle:number;
    private isSensor:boolean = false;

    constructor() {

    }

    public build():ShapeTemplate {
        return new ShapeTemplate(this.type, this.dimensions, this.materialId, this.collisionGroup, this.collisionMask, this.offset, this.angle, this.isSensor);
    }

    public setType(val:number):ShapeTemplateBuilder {
        this.type = val;
        return this;
    }

    public setDimensions(val:number[]):ShapeTemplateBuilder {
        this.dimensions = val;
        return this;
    }

    public setMaterialId(val:string):ShapeTemplateBuilder {
        this.materialId = val;
        return this;
    }

    public setCollisionGroup(val:number):ShapeTemplateBuilder {
        this.collisionGroup = val;
        return this;
    }

    public setCollisionMask(val:number):ShapeTemplateBuilder {
        this.collisionMask = val;
        return this;
    }

    public setOffset(val:number[]):ShapeTemplateBuilder {
        this.offset = val;
        return this;
    }

    public setAngle(val:number):ShapeTemplateBuilder {
        this.angle = val;
        return this;
    }

    public setIsSensor(val:boolean):ShapeTemplateBuilder {
        this.isSensor = val;
        return this;
    }
}