import {ShapeTemplate} from "../templates/ShapeTemplate";
import {SensorTemplate} from "../templates/SensorTemplate";

export class SensorTemplateBuilder {
    public id:string;
    public position:number[];
    public eventType:string;
    public eventArguments:any;
    public shapteTemplates:ShapeTemplate[];

    public build():SensorTemplate {
        return new SensorTemplate(this.id, this.position, this.eventType, this.eventArguments, this.shapteTemplates);
    }

    public setId(val:string):SensorTemplateBuilder {
        this.id = val;
        return this;
    }

    public setPosition(val:number[]):SensorTemplateBuilder {
        this.position = val;
        return this;
    }

    public setEventType(val:string):SensorTemplateBuilder {
        this.eventType = val;
        return this;
    }

    public setEventArguments(val:any):SensorTemplateBuilder {
        this.eventArguments = val;
        return this;
    }

    public setShapeTemplates(val:ShapeTemplate[]):SensorTemplateBuilder {
        this.shapteTemplates = val;
        return this;
    }
}