import {PhysicObjectTemplate} from "../templates/PhysicObjectTemplate";
import BodyOptions = p2.BodyOptions;
import {ShapeTemplate} from "../templates/ShapeTemplate";
import {DrawableTemplate} from "../templates/DrawableTemplate";
import {PlayerInfo} from "../../../gameplay/player/PlayerInfo";

export class PhysicObjectTemplateBuilder {
    public id:string;
    public bodyDefinition:BodyOptions = <BodyOptions>{};
    public shapeTemplates:ShapeTemplate[] = [];
    public x:number;
    public y:number;
    public orientation:number;
    public drawable:DrawableTemplate;
    public damping:number;
    public angularDamping:number;
    public playerInfo:PlayerInfo;

    public constructor() {
    }

    public build():PhysicObjectTemplate {
        return new PhysicObjectTemplate(
            this.id,
            this.x,
            this.y,
            this.orientation,
            this.drawable,
            this.bodyDefinition,
            this.shapeTemplates,
            this.damping,
            this.angularDamping,
            this.playerInfo);
    }

    public setId(value:string):PhysicObjectTemplateBuilder {
        this.id = value;
        return this;
    }

    public setBodyDefinition(value:p2.BodyOptions):PhysicObjectTemplateBuilder {
        this.bodyDefinition = value;
        return this;
    }

    public setShapeTemplates(value:ShapeTemplate[]):PhysicObjectTemplateBuilder {
        this.shapeTemplates = value;
        return this;
    }

    public addShapeTemplate(val:ShapeTemplate):PhysicObjectTemplateBuilder {
        this.shapeTemplates.push(val);
        return this;
    }

    public setPosition(value:number[]):PhysicObjectTemplateBuilder {
        this.x = value[0];
        this.y = value[1];
        this.bodyDefinition.position = value;
        return this;
    }

    public setOrientation(value:number):PhysicObjectTemplateBuilder {
        this.orientation = value;
        return this;
    }

    public setDrawable(value:DrawableTemplate):PhysicObjectTemplateBuilder {
        this.drawable = value;
        return this;
    }

    public setMass(val:number):PhysicObjectTemplateBuilder {
        this.bodyDefinition.mass = val;
        return this;
    }

    public setVelocity(val:number[]):PhysicObjectTemplateBuilder {
        this.bodyDefinition.velocity = val;
        return this;
    }

    public setAngle(val:number):PhysicObjectTemplateBuilder {
        this.bodyDefinition.angle = val;
        return this;
    }

    public setAngularVelocity(val:number):PhysicObjectTemplateBuilder {
        this.bodyDefinition.angularVelocity = val;
        return this;
    }

    public setForce(val:number[]):PhysicObjectTemplateBuilder {
        this.bodyDefinition.force = val;
        return this;
    }

    public setAngularForce(val:number):PhysicObjectTemplateBuilder {
        this.bodyDefinition.angularForce = val;
        return this;
    }

    public setFixedRotation(val:number):PhysicObjectTemplateBuilder {
        this.bodyDefinition.fixedRotation = val;
        return this;
    }

    public setDamping(val:number):PhysicObjectTemplateBuilder {
        this.damping = val;
        return this;
    }

    public setAngularDamping(val:number):PhysicObjectTemplateBuilder {
        this.angularDamping = val;
        return this;
    }

    public setPlayerInfo(val:PlayerInfo):PhysicObjectTemplateBuilder {
        this.playerInfo = val;
        return this;
    }
}