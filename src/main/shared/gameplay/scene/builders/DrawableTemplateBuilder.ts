import {RendererTemplate} from "../templates/RendererTemplate";
import {DrawableTemplate} from "../templates/DrawableTemplate";

export class DrawableTemplateBuilder {
    private drawableName:string;
    private rendererTemplates:RendererTemplate[] = [];


    constructor(drawableName:string) {
        this.drawableName = drawableName;
    }

    public build():DrawableTemplate {
        return new DrawableTemplate(this.drawableName, this.rendererTemplates);
    }

    public addRendererTemplate(val:RendererTemplate):DrawableTemplateBuilder {
        this.rendererTemplates.push(val);
        return this;
    }

    public setDrawableName(val:string):DrawableTemplateBuilder {
        this.drawableName = val;
        return this;
    }
}