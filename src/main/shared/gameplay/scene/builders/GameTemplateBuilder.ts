import {GameTemplate} from "../templates/GameTemplate";
import {BoardTemplate} from "../templates/BoardTemplate";
import {MaterialContactTemplate} from "../templates/MaterialContactTemplate";
import {PhysicObjectTemplate} from "../templates/PhysicObjectTemplate";
import {TeamTemplate} from "../templates/TeamTemplate";

export class GameTemplateBuilder {

    private materialContactTemplates:MaterialContactTemplate[] = [];
    private objects:PhysicObjectTemplate[] = [];
    private boardTemplate:BoardTemplate;
    private teamTemplates:TeamTemplate[];

    public constructor() {

    }

    public build():GameTemplate {
        return new GameTemplate(this.boardTemplate, this.materialContactTemplates, this.objects, this.teamTemplates);
    }

    public setBoard(boardTemplate:BoardTemplate):GameTemplateBuilder {
        this.boardTemplate = boardTemplate;
        return this;
    }

    public addMaterialContactTemplate(template:MaterialContactTemplate):GameTemplateBuilder {
        this.materialContactTemplates.push(template);
        return this;
    }

    public addObject(template:PhysicObjectTemplate):GameTemplateBuilder {
        this.objects.push(template);
        return this;
    }

    public setTeamTemplates(teamTemplates:TeamTemplate[]):GameTemplateBuilder {
        this.teamTemplates = teamTemplates;
        return this;
    }
}