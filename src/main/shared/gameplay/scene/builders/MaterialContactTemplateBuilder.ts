import {p2} from '../../../sharedBootstrap';
import {MaterialContactTemplate} from "../templates/MaterialContactTemplate";

export class MaterialContactTemplateBuilder {
    private materialA:string;
    private materialB:string;
    private options:p2.ContactMaterialOptions;

    public build():MaterialContactTemplate {
        return new MaterialContactTemplate(this.options, this.materialA, this.materialB);
    }

    public constructor(materialA:string, materialB:string) {
        this.materialA = materialA;
        this.materialB = materialB;
        this.options = <p2.ContactMaterialOptions>{};
    }

    public setFriction(val:number):MaterialContactTemplateBuilder {
        this.options.friction = val;
        return this;
    }

    public setRestitution(val:number):MaterialContactTemplateBuilder {
        this.options.restitution = val;
        return this;
    }

    public setStiffness(val:number):MaterialContactTemplateBuilder {
        this.options.stiffness = val;
        return this;
    }

    public setRelaxation(val:number):MaterialContactTemplateBuilder {
        this.options.relaxation = val;
        return this;
    }

    public setFrictionStiffness(val:number):MaterialContactTemplateBuilder {
        this.options.frictionStiffness = val;
        return this;
    }

    public setFrictionRelaxation(val:number):MaterialContactTemplateBuilder {
        this.options.frictionRelaxation = val;
        return this;
    }

    public setSurfaceVelocity(val:number):MaterialContactTemplateBuilder {
        this.options.surfaceVelocity = val;
        return this;
    }
}