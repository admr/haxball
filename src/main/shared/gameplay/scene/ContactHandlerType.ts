export class ContactHandlerType {
    public static get GOAL():string {
        return "goal"
    };

    public static get FIRST_BALL_CONTACT():string {
        return "firstBallContact"
    };
}