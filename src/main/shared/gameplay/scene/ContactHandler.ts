import {PhysicObject} from "../../gameplay/physics/PhysicObject";
import {AbstractScene} from "./AbstractScene";

export interface ContactHandler {
    onBeginContact(a:PhysicObject, b:PhysicObject, scene:AbstractScene, contactId:string);
    getEventType():string;
}