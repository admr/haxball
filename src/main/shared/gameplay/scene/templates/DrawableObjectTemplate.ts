import {DrawableTemplate} from "./DrawableTemplate";

export class DrawableObjectTemplate {
    public id:string;
    public x:number;
    public y:number;
    public orientation:number;
    public drawable:DrawableTemplate;

    constructor(id:string, x:number, y:number, orientation:number, drawable:DrawableTemplate) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.orientation = orientation;
        this.drawable = drawable;
    }
}