export class ShapeTemplate {
    public type:number;
    public dimensions:number[];
    public materialId:string;
    public collisionGroup:number;
    public collisionMask:number;
    public offset:number[];
    public angle:number;
    public isSensor:boolean;

    constructor(type:number, dimensions:number[], materialId:string, collisionGroup:number, collisionMask:number, offset:number[],
                angle:number, isSensor:boolean) {
        this.type = type;
        this.dimensions = dimensions;
        this.materialId = materialId;
        this.collisionGroup = collisionGroup;
        this.collisionMask = collisionMask;
        this.offset = offset;
        this.angle = angle;
        this.isSensor = isSensor;
    }
}