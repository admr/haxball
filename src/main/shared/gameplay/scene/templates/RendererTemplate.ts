export class RendererTemplate {
    public rendererName:string;
    public rendererAttributes:any;


    constructor(rendererName:string, rendererAttributes:any) {
        this.rendererName = rendererName;
        this.rendererAttributes = rendererAttributes;
    }
}