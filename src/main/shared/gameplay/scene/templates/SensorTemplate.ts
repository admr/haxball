import {ShapeTemplate} from "./ShapeTemplate";

export class SensorTemplate {
    public id:string;
    public position:number[];
    public eventType:string;
    public eventArguments:any;
    public shapeTemplates:ShapeTemplate[];

    constructor(id:string, position:number[], eventType:string, eventArguments:any, shapteTemplates:ShapeTemplate[]) {
        this.id = id;
        this.position = position;
        this.eventType = eventType;
        this.eventArguments = eventArguments;
        this.shapeTemplates = shapteTemplates;
    }
}