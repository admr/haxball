import {DrawableTemplate} from "./DrawableTemplate";
import {ShapeTemplate} from "./ShapeTemplate";

export class BoardTemplate {
    public width:number;
    public height:number;
    public shapeTemplates:ShapeTemplate[];
    public drawable:DrawableTemplate;
}