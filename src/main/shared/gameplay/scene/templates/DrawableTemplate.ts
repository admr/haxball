import {RendererTemplate} from "./RendererTemplate";

export class DrawableTemplate {
    public drawableName:string;
    public rendererTemplates:RendererTemplate[];


    constructor(drawableName:string, rendererTemplates:RendererTemplate[]) {
        this.drawableName = drawableName;
        this.rendererTemplates = rendererTemplates;
    }
}