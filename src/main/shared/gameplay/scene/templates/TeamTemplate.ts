import {RendererTemplate} from "./RendererTemplate";
export class TeamTemplate {
    public id:number;
    public rendererTemplate:RendererTemplate;
    public startArea:number[];

    constructor(id:number, rendererTemplate:RendererTemplate, startArea:number[]) {
        this.rendererTemplate = rendererTemplate;
        this.startArea = startArea;
        this.id = id;
    }
}