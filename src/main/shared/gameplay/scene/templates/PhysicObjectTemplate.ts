import BodyOptions = p2.BodyOptions;
import {DrawableObjectTemplate} from "./DrawableObjectTemplate";
import {ShapeTemplate} from "./ShapeTemplate";
import {DrawableTemplate} from "./DrawableTemplate";
import {PlayerInfo} from "../../../gameplay/player/PlayerInfo";

export class PhysicObjectTemplate extends DrawableObjectTemplate {
    public bodyDefinition:BodyOptions;
    public shapeTemplates:ShapeTemplate[];
    public damping:number;
    public angularDamping:number;
    public playerInfo:PlayerInfo;

    constructor(id:string, x:number, y:number, orientation:number, drawable:DrawableTemplate, bodyDefinition:p2.BodyOptions,
                shapeTemplates:ShapeTemplate[], damping:number, angularDamping:number, playerInfo:PlayerInfo) {
        super(id, x, y, orientation, drawable);
        this.bodyDefinition = bodyDefinition;
        this.shapeTemplates = shapeTemplates;
        this.damping = damping;
        this.angularDamping = angularDamping;
        this.playerInfo = playerInfo;
    }
}