import {p2} from "../../../sharedBootstrap";
import {BoardTemplate} from "./BoardTemplate";
import {PhysicObjectTemplate} from "./PhysicObjectTemplate";
import {MaterialContactTemplate} from "./MaterialContactTemplate";
import {TeamTemplate} from "./TeamTemplate";

export class GameTemplate {
    public boardTemplate:BoardTemplate;
    public materialContactTemplates:MaterialContactTemplate[];
    public objects:PhysicObjectTemplate[];
    public teamTemplates:TeamTemplate[];

    constructor(boardTemplate:BoardTemplate, materialContactTemplates:MaterialContactTemplate[], objects:PhysicObjectTemplate[],
                teamTemplates:TeamTemplate[]) {
        this.boardTemplate = boardTemplate;
        this.materialContactTemplates = materialContactTemplates;
        this.objects = objects;
        this.teamTemplates = teamTemplates;
    }
}