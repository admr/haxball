export class MaterialContactTemplate {
    public options:p2.ContactMaterialOptions;
    public materialA:string;
    public materialB:string;


    constructor(options:p2.ContactMaterialOptions, materialA:string, materialB:string) {
        this.options = options;
        this.materialA = materialA;
        this.materialB = materialB;
    }
}