import {PhysicObject} from "../../gameplay/physics/PhysicObject";
import {PhysicsManager} from "../physics/PhysicsManager";
import {GameTemplate} from "./templates/GameTemplate";
import {BoardTemplate} from "./templates/BoardTemplate";
import {PhysicObjectTemplate} from "./templates/PhysicObjectTemplate";
import {ContactEventHandler} from "./ContactEventHandler";
import {SensorTemplate} from "./templates/SensorTemplate";

export abstract class AbstractScene {

    protected physicsManager:PhysicsManager;
    protected objects:{[id:string] : PhysicObject} = {};
    protected bodyIdToObjectMap:{[id:number] : PhysicObject} = {};
    protected gameCreated:boolean = false;
    private _contactEventHandler:ContactEventHandler;
    private _id:string;

    public constructor(physicsManager:PhysicsManager) {
        this.physicsManager = physicsManager;
        this._contactEventHandler = new ContactEventHandler(this);
    }

    public update(step:number):void {
        for (let id in this.objects) {
            this.objects[id].update(step);
        }
    }

    public addObject(object:PhysicObject):boolean {

        if (object.id == null) {
            throw Error("Object must have an id.");
        }

        if (this.objects[object.id] != null) {
            return false;
        }

        this.objects[object.id] = object;

        if (object.body != null) {
            this.physicsManager.addBody(object.body);
            this.bodyIdToObjectMap[object.body.id] = object;
        }

        return true;
    }

    public removeObject(id:string):boolean {
        var obj:PhysicObject = this.objects[id];
        if (obj == null) {
            return false;
        }

        if (this.bodyIdToObjectMap[obj.body.id] != null) {
            delete this.bodyIdToObjectMap[obj.body.id];
        }
        this.physicsManager.removeBody(obj.body);
        delete this.objects[id];
        return true;
    }

    public createGameWorld(template:GameTemplate):void {
        for (let i in template.materialContactTemplates) {
            this.physicsManager.createContactMaterial(template.materialContactTemplates[i]);
        }

        var board:PhysicObject = this.createFromBoardTemplate(template.boardTemplate);
        this.addObject(board);

        for (let i in template.objects) {
            var object:PhysicObject = this.createObjectFromTemplate(template.objects[i]);
            this.addObject(object);
        }

        this.gameCreated = true;

        this.physicsManager.getWorld().on("beginContact", this._contactEventHandler.delegate, null);
    }

    public abstract createObjectFromTemplate(template:PhysicObjectTemplate):PhysicObject;

    public abstract createFromBoardTemplate(template:BoardTemplate):PhysicObject;

    public abstract createFromSensorTemplate(template:SensorTemplate):PhysicObject;

    protected applySensorTemplate(obj:PhysicObject, template:SensorTemplate):void {
        obj.body = this.physicsManager.createBodyFromSensorTemplte(template);
        obj.id = template.id;
        obj.eventData = template.eventArguments;
    }

    protected applyBoardTemplate(obj:PhysicObject, template:BoardTemplate):void {
        obj.body = this.physicsManager.createBodyFromBoardTemplate(template);
        obj.id = 'board';
    }

    get contactEventHandler():ContactEventHandler {
        return this._contactEventHandler;
    }

    protected applyObjectTemplate(obj:PhysicObject, template:PhysicObjectTemplate):void {
        obj.id = template.id;
        obj.body = this.physicsManager.createBodyFromPhysicTemplate(template);
        obj.playerInfo = template.playerInfo;
    }

    public clear():void {
        this.objects = {};
    }

    public getObject(id:string):PhysicObject {
        return this.objects[id];
    }

    public getObjectByBodyId(bodyId:number):PhysicObject {
        return this.bodyIdToObjectMap[bodyId];
    }


    get id():string {
        return this._id;
    }

    set id(value:string) {
        this._id = value;
    }

    public getObjectsWithPredicate(predicate:Function):PhysicObject[] {
        var out:PhysicObject[] = [];

        for (let id in this.objects) {
            if (predicate.call(null, this.objects[id])) {
                out.push(this.objects[id]);
            }
        }

        return out;
    }
}