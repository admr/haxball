export interface ShapeCreator {
    getSupportedType():number;
    getRequiredDimensions():number;
    createShape(dimensions:number[]):p2.Shape;
}