export enum GamePhaseType {
    //CREATING_ROOM,
    DEFINING_MATCH,
    PRE_PLAYING,
    PLAYING,
    POST_GOAL,
}