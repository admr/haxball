import {AbstractAction} from "./actions/AbstractAction";
import {ActionHandlingResult} from "./ActionHandlingResult";

export abstract class AbstractActionHandler {
    public abstract getSupportedActionType():string;

    public abstract handleAction(action:AbstractAction, isInternal:boolean):ActionHandlingResult;
}