import {AbstractGamePhase} from "../AbstractGamePhase";
import {GamePhaseType} from "../GamePhaseType";
import {GamePhaseManager} from "../GamePhaseManager";

export class DefiningMatchPhase extends AbstractGamePhase {

    constructor() {
        super();
    }

    update(dt:number, gamePhaseManager:GamePhaseManager):void {

    }

    getType():GamePhaseType {
        return GamePhaseType.DEFINING_MATCH;
    }

    onEnter(previousState:GamePhaseType, args:any):void {

    }

    onExit(nextState:GamePhaseType):void {

    }

    getAllowedNextPhases():GamePhaseType[] {
        return [GamePhaseType.PRE_PLAYING];
    }

}