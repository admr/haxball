import {AbstractGamePhase} from "../AbstractGamePhase";
import {GamePhaseType} from "../GamePhaseType";
import {GamePhaseManager} from "../GamePhaseManager";

export class TestGamePhase extends AbstractGamePhase {
    update(dt:number, gamePhaseManager:GamePhaseManager):void {
    }

    getType():GamePhaseType {
        return undefined;
    }

    onEnter(previousState:GamePhaseType, args:any):void {
    }

    onExit(nextState:GamePhaseType):void {
    }

    getAllowedNextPhases():GamePhaseType[] {
        return undefined;
    }


}