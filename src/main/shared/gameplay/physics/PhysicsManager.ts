import {p2} from '../../sharedBootstrap';
import {PhysicObjectTemplate} from "../scene/templates/PhysicObjectTemplate";
import {ShapeTemplate} from "../scene/templates/ShapeTemplate";
import {ShapeCreator} from "../scene/ShapeCreator";
import {MaterialContactTemplate} from "../scene/templates/MaterialContactTemplate";
import {BoardTemplate} from "../scene/templates/BoardTemplate";
import {SensorTemplate} from "../scene/templates/SensorTemplate";

export class PhysicsManager {

    private world:p2.World;
    private shapeCreators:ShapeCreator[];
    private materials:{[id:string]: p2.Material} = {};

    constructor(shapeCreators:ShapeCreator[]) {
        this.shapeCreators = shapeCreators;
    }

    public initWorld():void {
        if(this.world == null){
            this.world = new p2.World();
            this.world.gravity = [0, 0];
        }
    }

    public getWorld():p2.World {
        return this.world;
    }

    public addBody(body:p2.Body) {
        this.world.addBody(body);
    }

    public removeBody(body:p2.Body) {
        this.world.removeBody(body);
    }

    public step(timeStep:number):void {
        this.world.step(timeStep);
    }

    public createBodyFromBoardTemplate(template:BoardTemplate):p2.Body {
        //TODO: change template to list of shapes...
        var body:p2.Body = new p2.Body();
        body.type = p2.Body.STATIC;
        body.mass = 0;

        for (let i in template.shapeTemplates) {
            this.createAndAddShapeFromTemplate(body, template.shapeTemplates[i]);
        }

        return body;
    }

    public createBodyFromSensorTemplte(template:SensorTemplate):p2.Body {
        var body:p2.Body = new p2.Body();
        body.position = template.position;
        for (let i in template.shapeTemplates) {
            this.createAndAddShapeFromTemplate(body, template.shapeTemplates[i]);
        }

        return body;
    }

    public createBodyFromPhysicTemplate(template:PhysicObjectTemplate):p2.Body {
        var out:p2.Body = new p2.Body(template.bodyDefinition);

        for (var i in template.shapeTemplates) {
            this.createAndAddShapeFromTemplate(out, template.shapeTemplates[i]);
        }

        if (template.angularDamping != null) {
            out.angularDamping = template.angularDamping;
        }

        if (template.damping != null) {
            out.damping = template.damping;
        }

        return out;
    }

    public createAndAddShapeFromTemplate(body:p2.Body, template:ShapeTemplate):void {
        var shape:p2.Shape = this.createShapeFromTemplate(template);
        body.addShape(shape, template.offset, template.angle);
    }

    public createShapeFromTemplate(template:ShapeTemplate):p2.Shape {
        var creator = this.getShapeCreator(template.type);
        if (creator != null && creator.getRequiredDimensions() <= template.dimensions.length) {
            var shape:p2.Shape = creator.createShape(template.dimensions);
            if (template.materialId != null) {
                this.applyMaterial(shape, template.materialId);
            }

            shape.collisionGroup = template.collisionGroup;
            shape.collisionMask = template.collisionMask;
            shape.sensor = template.isSensor;

            return shape;
        }

        return null;
    }

    public createContactMaterial(template:MaterialContactTemplate):p2.ContactMaterial {
        var contactMaterial:p2.ContactMaterial = new p2.ContactMaterial(
            this.getOrCreateMaterial(template.materialA),
            this.getOrCreateMaterial(template.materialB),
            template.options
        );

        this.world.addContactMaterial(contactMaterial);
        return contactMaterial;
    }

    public applyMaterial(shape:p2.Shape, materialId:string) {
        shape.material = this.getMaterial(materialId);
    }

    public getMaterial(materialId:string):p2.Material {
        if (this.materials[materialId] == null) {
            throw new Error("No material : " + materialId);
        }

        return this.materials[materialId];
    }

    public getOrCreateMaterial(materialId:string):p2.Material {
        if (this.materials[materialId] == null) {
            this.materials[materialId] = new p2.Material(p2.Material.idCounter++);
        }

        return this.materials[materialId];
    }

    public getShapeCreator(type:number):ShapeCreator {
        for (var i in this.shapeCreators) {
            if (this.shapeCreators[i].getSupportedType() == type) {
                return this.shapeCreators[i];
            }
        }

        return null;
    }


}