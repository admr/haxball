import {PlayerInfo} from "../player/PlayerInfo";

export class PhysicObject {
    private _id:string;
    private _body:p2.Body;
    private _direction:number[];
    private _eventData:any;
    private _playerInfo:PlayerInfo;

    public update(step:number) {
        if (this._direction != null) {
            this._body.force = [this._direction[0], this._direction[1]];
        }
    }

    get direction():number[] {
        return this._direction;
    }

    set direction(value:number[]) {
        this._direction = value;
    }

    get id():string {
        return this._id;
    }

    set id(value:string) {
        this._id = value;
    }

    get body():p2.Body {
        return this._body;
    }

    set body(value:p2.Body) {
        this._body = value;
    }

    get eventData():any {
        return this._eventData;
    }

    set eventData(value:any) {
        this._eventData = value;
    }

    get playerInfo():PlayerInfo {
        return this._playerInfo;
    }

    set playerInfo(value:PlayerInfo) {
        this._playerInfo = value;
    }
}