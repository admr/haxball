import {PlayerInfo} from "./PlayerInfo";

export class PlayerInfoBuilder {
    private playerName:string;
    private kickStrength:number;
    private movingSpeed:number;
    private team:number;

    public build():PlayerInfo {
        return new PlayerInfo(this.playerName, this.kickStrength, this.movingSpeed, this.team);
    }

    public setPlayerName(val:string):PlayerInfoBuilder {
        this.playerName = val;
        return this;
    }

    public setKickStrength(val:number):PlayerInfoBuilder {
        this.kickStrength = val;
        return this;
    }

    public setMovingSpeed(val:number):PlayerInfoBuilder {
        this.movingSpeed = val;
        return this;
    }

    public setTeam(val:number):PlayerInfoBuilder {
        this.team = val;
        return this;
    }
}