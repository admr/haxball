export class PlayerInfo {
    public playerName:string;
    public kickStrength:number;
    public movingSpeed:number;
    public team:number;

    constructor(playerName:string, kickStrength:number, movingSpeed:number, team:number) {
        this.playerName = playerName;
        this.kickStrength = kickStrength;
        this.movingSpeed = movingSpeed;
        this.team = team;
    }
}