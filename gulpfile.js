var gulp = require('gulp');
var concat = require('gulp-concat');
var clean = require('gulp-clean');
var	typescript = require('gulp-typescript');
var browserify = require('gulp-browserify');
var mocha = require('gulp-mocha');
var runSequence = require('run-sequence');

var paths = {
    tsDefinitions: 'tsDefinitions/*.ts',
    sources : {
        all: 'src/**/*.ts',
        client: 'src/main/client/**/*.ts',
        shared: 'src/main/shared/**/*.ts',
        server: 'src/main/server/**/*.ts',
        tests: 'src/test/**/*.ts'
    }
};

function errorLog (error) {
  console.error.bind(error);
  this.emit('end');
}

var project = typescript.createProject("tsconfig.json");

gulp.task('compile-sources',['clean-sources'], function(){
    return gulp.src([paths.sources.all,paths.tsDefinitions])
        .pipe(typescript(project))
        .on('error', errorLog)
        .pipe(gulp.dest('out'));
});

gulp.task('pack-client', function(){
    return gulp.src(['out/main/**/*.js', "!out/main/server/**/*.js"])
        .pipe(gulp.dest('build'));
});

gulp.task('clean-sources', function(){
    return gulp.src('out/main', {read: false})
        .pipe(clean());
});

gulp.task('clean-tests', function(){
    return gulp.src('out/test', {read: false})
        .pipe(clean());
});

gulp.task('compile-tests', ['clean-tests'], function(){
    return gulp.src([paths.sources.tests, paths.tsDefinitions])
        .pipe(typescript(project))
        .on('error', errorLog)
        .pipe(gulp.dest('./out'));
});

gulp.task('test', ['compile-tests'], function(){
   return gulp.src(['out/test/**/*.js'], {read: false})
       .pipe(mocha({reporter: 'list'}))
       .on('error', errorLog);
});

gulp.task("libs", function(){
    return gulp.src([
            'es6-shim/es6-shim.min.js',
            'systemjs/dist/system-polyfills.js',
            'systemjs/dist/system.src.js',
            'reflect-metadata/Reflect.js',
            'rxjs/**',
            'zone.js/dist/**',
            '@angular/**',
            'forge-di/build/**/*.js',
            'pixi.js/bin/pixi.js',
            'jquery/dist/jquery.min.js',
            'underscore/underscore.js',
            'util/**',
            'assert/assert.js',
            'inherits/**',
            'forge-di/build/**/*.js',
            'p2/build/p2.js',
            'bootstrap/dist/js/bootstrap.js',
            'jquery/dist/jquery.min.js',
        ], {cwd: "node_modules/**"}) /* Glob required here. */
        .pipe(gulp.dest("build/lib"));
});

gulp.task("fast-build",function(){
   return runSequence("compile-sources","pack-client","resources");
});

gulp.task("resources", function(){
    return gulp.src(["!src/main/server/**/*", "!src/main/server", "src/main/**/*", "!**/*.ts" ])
        .pipe(gulp.dest("build/"));
});

gulp.task('build',function(){
    return runSequence("compile-sources", "libs", "pack-client", "resources");
});